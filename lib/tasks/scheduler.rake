task :reminder_activity => :environment do
  current_time = Time.now
  current_date = Date.today
  # current_date.saturday? ||
  unless current_date.sunday?
    User.all.map do |user|
      logs = Log.where(user_id: user.id)
      if logs.count > 0
        log_last_created = logs.last.created_at
      else
        log_last_created = nil
      end

      if log_last_created.nil? || log_last_created < current_time - 2.days
        reminder_notifications = user.notifications.where(category: :user_reminder)
        if reminder_notifications.count > 0
          reminder_last_created = reminder_notifications.last.created_at
        else
          reminder_last_created = nil
        end

        if reminder_last_created.nil?# || reminder_last_created < current_time - 2.days
          tokens = Phone.where(user_id: user.id).where.not(device_token: nil).pluck(:device_token)
          if tokens.size > 0
            Notification.create(target: user, object: user, category: :user_reminder, type: 'reminder')
          end
        end
      end
    end
  end
end
