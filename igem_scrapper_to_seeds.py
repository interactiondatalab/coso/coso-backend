import pandas as pd

print("Make sure to run this script from the Results Folder of the igem-scrapper python package")


teams_df = pd.read_table('./team_meta_db.tsv')
users_df = pd.read_table('./teams_info_members_db.tsv')

teams_df = teams_df[teams_df['Year'] == 2020]

users_df = users_df[users_df['Year'] == 2020]

def make_team_seed(team):
    return """
    team = Team.find_by(igem_team_id: {TeamID})
    if team.nil?
      team = Team.create(name: '{Team}', year: {Year}, country: '{Country}', igem_team_id: {TeamID})
    else
      team.update_attributes(name: '{Team}', year: {Year}, country: '{Country}', igem_team_id: {TeamID})
    end
    if '{Status}' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save""".format(**team)

def make_user_seed(user):
    return """
    igemuser = IgemUser.find_or_create_by(username: '{UserID}')
    team = Team.find_by(igem_team_id: {TeamID})
    igemuser.team = team
    if igemuser.full_name.nil?
      igemuser.full_name = \"{UserName}\"
    end
    igemuser.save""".format(**user)

f = open('teams.seeds.rb', 'w')
for index, row in teams_df.iterrows():
    f.write(make_team_seed(row))
f.close()
print("teams.seeds.rb saved")

f = open('users.seeds.rb', 'w')
for index, row in users_df.iterrows():
    f.write(make_user_seed(row))
f.close()
print("users.seeds.rb saved")

print("please copy the *.seeds.rb files generated to the koso/db/seeds folder and run rails db:seed:teams followed by  rails db:seed:users")
