module FirebaseCloudMessaging
  class UserNotificationSender
    # Firebase works with up to 1000 device_ids per call
    MAX_USER_IDS_PER_CALL = 1000

    def initialize(user, user_device_ids, title, message)
      # @ttl = 0
      # unless user.last_sign_in_ip.nil?
      #   ip = user.last_sign_in_ip.to_string
      #   country_code = Geocoder.search(ip).first.country
      #   localtime = ActiveSupport::TimeZone.country_zones(country_code).first.now
      #   servertime = DateTime.now
      #
      #   if localtime.ctime > servertime.ctime
      #     new_localtime = localtime.change({hour: 8, min: 0, sec: 0})
      #     if localtime.mday == servertime.mday
      #       new_localtime = localtime.next_day
      #     end
      #     @ttl = new_localtime.to_i - servertime.to_i
      #   else
      #     @ttl = servertime.to_i - localtime.time.to_i
      #   end
      # end

      @user_device_ids = user_device_ids
      @title = title
      @message = message
    end

    def send
      @user_device_ids.each_slice(MAX_USER_IDS_PER_CALL) do |device_ids|
        fcm_client.send(device_ids, options)
      end
    end

    private

    def options
      {
        priority: 'high',
        notification: {
          title: @title,
          body: @message,
          sound: 'default',
          image: 'https://igem-ties.info/pets.png'
        }
        # ,
        # android: {
        #     ttl: '#{@ttl}s'
        # }
        # apns: {
        #   headers: {
        #     "apns-expiration":"1604750400"
        #   }
      }
    end

    # data: {
    #   type: @type_notification
    # }

    def fcm_client
      @fcm_client ||= FCM.new(ENV['FIREBASE_SECRET'])
    end
  end
end
