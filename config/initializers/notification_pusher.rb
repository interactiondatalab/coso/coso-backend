# frozen_string_literal: true

module NotificationPusher
  module DeliveryMethod
    class FireBasePusher < NotificationPusher::DeliveryMethod::Base
      def call
        tokens = notification.target.phones.map do |phone|
          phone.device_token
        end

        fcm = FirebaseCloudMessaging::UserNotificationSender.new(notification.target, tokens, notification.title, notification.content)

        fcm.send
      end
    end
  end
end

NotificationPusher.configure do |config|
  # A delivery method handles the process of sending your notifications to
  # various services for you.
  # Learn more: https://github.com/jonhue/notifications-rails/tree/master/notification-pusher#delivery-methods
  # config.register_delivery_method :email, :ActionMailer, email: 'my@email.com'
    config.register_delivery_method :firebase, :FireBasePusher
end
