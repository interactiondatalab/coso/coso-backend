require 'sidekiq/web'

Rails.application.routes.draw do

  # mount Sidekiq::Web => '/sidekiq'

  devise_for :users,
           defaults: { format: :json },
           path: '/api',
           path_names: {
             sign_in: 'login',
             sign_out: 'logout',
             registration: 'signup'
           },
           controllers: {
             sessions: 'sessions',
             registrations: 'registrations',
             passwords: 'passwords',
             confirmations: 'confirmations'
           }

  scope :api do
    get '/auth/slack/callback', to: 'slack_auth#create'
    get '/auth/slack', to: 'slack_auth#get_auth_url'

    namespace :slack do
      get '/exists' => :is_linked
      get '/users' => :get_users
      get '/user_mentions' => :get_mentions
      get '/get_reactions_count' => :get_reactions_count
      get '/get_daily_message' => :get_daily_message
      get '/get_user_reaction_matrix' => :get_user_reaction_matrix
      get '/get_users_reaction_count' => :get_users_reaction_count
    end

    namespace :igem do
      post '/members' => :update_members
      post '/pages' => :update_pages
      post '/edits' => :update_edits
      get '/team' => :get_team
    end

    resources :users do
      collection do
        get '/self' => :get_self
        get '/exist' => :exist
        get '/is_admin' => :check_admin_status
      end
    end

    resources :news

    resources :surveys do
      collection do
        post '/:id/answer' => :answer
        post '/:id/answer_no_auth' => :answer_no_auth
        get '/:id/is_completed' => :is_completed
        get '/questionnaires' => :questionnaires
      end
    end

    resources :logs
    resources :tasks
    resources :app_activities
    resources :pings
    resources :phones
    resources :relation_categories

    resources :igem_users do
      collection do
        get '/exist' => :exist
        post '/add' => :add
      end
    end

    resources :teams do
      collection do
        get '/exist' => :exist
        get '/list' => :list
        get '/:id/users' => :members
        get '/:id/igem_users' => :members_igem
      end
    end

    namespace :stats do
      get '/streak' => :streak
      get '/tasks' => :tasks
      get '/collaborators' => :collaborators
      get '/global' => :global
      get '/logsperday' => :team_task_nb_per_day
      get '/teamlogs' => :team_logs
      get '/logs_frequency' => :logs_frequency
      get '/network' => :network
    end
  end

  root to: "home#index"

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
