# README

* Ruby version

2.7.1

* System dependencies

Install required gems with `bundle install` and then libpq5 with `sudo apt install libpq5-dev`

* Configuration

Configure the DATABASE_URL so that it points to a PostgreSQL DB.

* Database creation

Create the database with `rails db:create`

* Database initialization

First run Migrations: `rails db:migrate` then populate the teams and users `rails db:seed:teams db:seed:users`. Finally add the tasks `rails db:seed:tasks`

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

# Configuration

This project needs the following ENV variables:


BACKEND_URL: The URL of the backend
DATABASE_URL: The url of the PostgreSQL DB as a postgres:// formated string
DEVISE_JWT_SECRET_KEY: A large secret
EMAIL: The email from which emails are sent
FIREBASE_SECRET: The API token secret for FireBase Notification push
JWT_SECRET_KEY: A large secret
LANG: en_US.UTF-8
RACK_ENV: development or production (defaults to development, so only needed for production)
RAILS_ENV: development or production (defaults to development, so only needed for production)
RAILS_LOG_TO_STDOUT: enabled (should be disabled in production)
RAILS_SERVE_STATIC_FILES: enabled
SECRET_KEY_BASE: A large number
SMTP_HOST: SMTP host for email sending
SMTP_PASSWORD: SMTP password
SMTP_PORT: SMTP port
SMTP_USER: SMTP username
