import json
import requests

class Api:
    def __init__(self, username, password, backend_url):
        self.username = username
        self.password = password
        self.backend_url = backend_url 
        if not self.login():
            raise Exception("Cannot Log In to API")
        
    def make_url(self, endpoint):
        return requests.compat.urljoin(self.backend_url, endpoint)
        
    def login(self):
        params = {
            "user": {
                "email": self.username,
                "password": self.password
            }
        }
        handle = requests.post(self.make_url('api/login'), json=params)
        if handle.status_code == 200:
            self.jwt_token = handle.headers['Authorization']
            self.headers = {"Authorization": self.jwt_token}
            print("Success!")
            return True
        else:
            return False
        
    def get_team(self, igem_team_id):
        params = {"igem_team_id":igem_team_id}
        handle = requests.get(self.make_url('api/igem/team'), params=params, headers=self.headers)
        if handle.status_code == 200:
            team = json.loads(handle.content.decode())
            return team
        elif handle.status_code == 404:
            print("Igem ID does not exist")
            return False
        else:
            print("Error getting team ID")
            return False
        
    def save_members(self, members, team_id):
        params = {
            "team_id": team_id,
            "members": members
        }
        handle = requests.post(self.make_url('api/igem/members'), json=params, headers=self.headers)
        if handle.status_code == 200:
            return True
        else:
            print("Error saving members")
            return False
        
    def save_pages(self, pages, team_id):
        params = {
            "team_id": team_id,
            "pages": pages
        }
        handle = requests.post(self.make_url('api/igem/pages'), json=params, headers=self.headers)
        if handle.status_code == 200:
            return True
        else:
            print("Error saving pages")
            return False
        
    def save_edits(self, edits, team_id):
        params = {
            "team_id": team_id,
            "edits": edits
        }
        handle = requests.post(self.make_url('api/igem/edits'), json=params, headers=self.headers)
        if handle.status_code == 200:
            return True
        else:
            print("Error saving edits")
            return False