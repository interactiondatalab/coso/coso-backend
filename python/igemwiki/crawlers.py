import wiki_parser as wp
from bs4 import BeautifulSoup
import requests
from datetime import datetime
import re
import progressbar

class Crawler:
    def __init__(self, y, team, highschool=False):
        self.year = y
        self.team = team
        if highschool:
            extra = 'hs'
        else:
            extra = ''
        self.base_url = "http://{}{}.igem.org".format(y, extra)
        self.results = []

    def get_results(self):
        return self.results

    def get_page(self, url, counter=0):
        handle = requests.get(url)
        if handle.status_code == 200:
            return handle.content
        else:
            if counter > 10:
                print("Failed to read the page history at {}".format(url))
                return ""
            counter += 1
            get_page(url, counter)

class PageList(Crawler):
    def __init__(self, y, team, highschool=False):
        super().__init__(y, team, highschool=False)

    def crawl(self):
        url = self.base_url + "/wiki/index.php?title=Special%3APrefixIndex&prefix=Team%3A{}/&namespace=0".format(self.team)
        content = self.get_page(url)
        soup = BeautifulSoup(content, 'html.parser')
        if self.year >= 2015:
            table = soup.find("table", {"class": "mw-prefixindex-list-table"})
        else:
            table = soup.find("table", {"id": "mw-prefixindex-list-table"})
        pages = table.find_all('a')
        for page in pages:
            name = page.string
            display_name = name.replace('Team:{}'.format(self.team), '')
            self.results.append([name, display_name])
        self.results.append(["Team:{}".format(self.team), "Home"])

class PageHistory(Crawler):
    def __init__(self, y, team, pages=None, highschool=False):
        super().__init__(y, team, highschool=False)
        if not pages:
            pl = PageList(y, team, highschool)
            pl.crawl()
            pages = pl.get_results()
        self.pages = pages

    def parse_contrib(self, page_link, contrib):
        edit_time = datetime.strptime(contrib.find("a", {"class": "mw-changeslist-date"}).string, '%H:%M, %d %B %Y')
        username = contrib.find("a", {"class": "mw-userlink"}).string.replace(" ", "_")
        page_size = contrib.find("span", {"class": "history-size"}).string.replace('(','').replace(')','').replace(',','').replace("bytes", '').replace('byte','')
        if page_size == "empty":
            page_size = 0
        else:
            page_size = int(page_size)
        regex = re.compile('mw-plusminus-.*')
        delta = contrib.find("span", {"class": regex})
        if not delta:
            delta = contrib.find("strong",{"class": regex})
        contrib_delta = int(delta.string.replace('(','').replace(')','').replace('+','').replace(',',''))
        res = [page_link, username, edit_time.timestamp(), page_size, contrib_delta]
        self.results.append(res)
        return res

    def parse_contribs(self, page_link, contribs):
        results = []
        for contrib in contribs:
            results.append(self.parse_contrib(page_link, contrib))
        return results

    def get_contribs(self, soup):
        contribs = soup.find(id="pagehistory").find_all('li')
        return contribs

    def crawl(self):
        results = []
        for page_link, page_name in progressbar.progressbar(self.pages):
            url = "{}/wiki/index.php?title={}&action=history".format(self.base_url, page_link)
            content = self.get_page(url)
            soup = BeautifulSoup(content, 'html.parser')
            contribs = self.get_contribs(soup)
            results += self.parse_contribs(page_link, contribs)
            next_link = soup.find('a', {"class": "mw-nextlink"})
            while next_link:
                url = "{}{}".format(self.base_url, next_link["href"])
                content = self.get_page(url)
                soup = BeautifulSoup(content, 'html.parser')
                contribs = self.get_contribs(soup)
                results += self.parse_contribs(page_link, contribs)
                next_link = soup.find('a', {"class": "mw-nextlink"})
        return results

class ParseTeamMembers(Crawler):
    def __init__(self, year, team, team_id):
        super().__init__(year, team, highschool=False)
        self.team_id = team_id
        self.parser = wp.TeamInfo(year, team, team_id)

    def crawl(self):
        results = self.parser.parse_team(self.team_id)
        members = self.parser.make_member_table(results)
        self.results = members
        return members

class ParseTeamInfo(Crawler):
    def __init__(self, year, team, team_id):
        super().__init__(year, team, highschool=False)
        self.team_id = team_id
        self.parser = wp.TeamInfo(year, team, team_id)

    def crawl(self):
        results = self.parser.parse_team(self.team_id)
        meta = self.parser.make_team_table(results)
        self.results = meta
        return meta
