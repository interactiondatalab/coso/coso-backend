from crawlers import ParseTeamMembers, PageList, PageHistory
from api import Api
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--igem_team_id', help='The Igem Team ID')
args = parser.parse_args()

print("Logging in the backend")
api = Api(os.environ['API_USERNAME'], os.environ['API_PASSWORD'], os.environ['BACKEND_URL'])

print("Getting team id")
team = api.get_team(args.igem_team_id)

print("Crawling Team members from IGEM website")
ptm = ParseTeamMembers(team["year"], team["team_name"], args.igem_team_id)
ptm.crawl()

print("Crawling Pages from IGEM website")
pl = PageList(team["year"], team["team_name"])
pl.crawl()

print("Crawling Contributions from IGEM website")
ph = PageHistory(team["year"], team["team_name"], pages=pl.results)
ph.crawl()

print("Saving team members back to the API")
api.save_members(ptm.results, team["team_id"])

print("Saving pages back to the API")
api.save_pages(pl.results, team["team_id"])

print("Saving edits back to the API")
api.save_edits(ph.results, team["team_id"])