# Sonar-IgemWiki

Igem Wiki worker for Sonar RAILS backend

# build with 
`docker build . -t registry.heroku.com/coso-backend/igemwiki` 

# deploy with

`docker login --username=_ --password=$(heroku auth:token) registry.heroku.com`

`docker push registry.heroku.com/coso-backend/igemwiki`

`heroku container:release igemwiki -a coso-backend`
