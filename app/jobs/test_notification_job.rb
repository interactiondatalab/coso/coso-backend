class TestNotificationJob < ApplicationJob
  queue_as :default

  def perform(*args)
    idlab = Team.find(301)
    idlab.users.each do |user|
      Notification.create(target: user, object: user, category: :user_reminder, type: 'reminder')
    end
  end
end
