class ActivityReminderJob < ApplicationJob
  queue_as :default

  def perform(*args)
    current_time = Time.now
    current_date = Date.today
    unless current_date.sunday?
      User.all.map do |user|
        log_trigger = false
        last_log = Log.where(user_id: user.id).last
        if last_log.nil? or (Time.now.to_date - last_log.created_at.to_date).to_i > 6
          last_notification = user.notifications.where(category: :user_reminder).last
          if last_notification.nil? or (Time.now.to_date - last_notification.created_at.to_date).to_i > 6
            Notification.create(target: user, object: user, category: :user_reminder, type: 'reminder')
          end
        end
      end
    end
  end
end
