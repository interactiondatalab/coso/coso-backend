require 'platform-api'

class IgemWikiDownloadJob < ApplicationJob
  queue_as :default

  def perform
    Team.all.each do |team|
      download(team.igem_team_id)
    end
  end

  def download(igem_team_id)
    heroku = PlatformAPI.connect_oauth(ENV['HEROKU_API_TOKEN'])
    body={"command": "python ./igem_wiki.py --igem_team_id #{igem_team_id}",
          "type": "igemwiki",
          "time_to_live": 1800}
    dyno = heroku.dyno.create(ENV['HEROKU_APP'], body=body)
    has_run = false

    begin
      while true
        infos = heroku.dyno.info(ENV['HEROKU_APP'], dyno['name'])
        if infos['state'] == "up"
          has_run = true
        end
      end
    rescue Excon::Error::NotFound
      if has_run
        return true
      end
    end
  end
end
