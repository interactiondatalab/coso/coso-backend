class SlackApiDownloadJob < ApplicationJob
  queue_as :default

  def perform
    SlackAuth.pluck(:team_id).uniq.each do |team_id|
      verify_auth team_id
      download team_id
    end
  end

  def verify_auth(team_id)
    SlackAuth.where(team_id: team_id).each do |slack|
      if slack.client.auth_test.ok == false
        slack.destroy
      end
    end
  end

  def download(team_id)
    if SlackAuth.where(team_id: team_id).last.client.api_test.ok
      slack = SlackAuth.where(team_id: team_id).last
      unless slack.nil?
        slack.save_from_api
      end
    end
  end
end
