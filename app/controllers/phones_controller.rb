class PhonesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_phone, only: [:show, :update, :destroy]
  before_action :is_admin, only: [:destroy]

  include Admin

  # GET /phones
  def index
    @phones = Phone.where(user: current_user).all
    render json: @phones
  end

  # GET /phones/1
  def show
    render json: @phone
  end

  # POST /phones
  def create
    @phone = Phone.new(phone_params)
    @phone.user = current_user

    if @phone.save
      render json: @phone, status: :created, location: @phone
    else
      render json: @phone.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /phones/1
  def update
    if @phone.update(phone_params)
      render json: @phone
    else
      render json: @phone.errors, status: :unprocessable_entity
    end
  end

  # DELETE /phones/1
  def destroy
    if @phone.destroy
      render status: :ok
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_phone
      if Phone.exists?(params[:id])
        @phone = Phone.find(params[:id])
        if @phone.user.id != current_user.id
          render json: {"error": "You can only access your phones"}, status: :forbidden and return
        end
      else
        render json: {"error": "This phone does not exist"}, status: :unprocessable_entity
      end
    end

    # Only allow a trusted parameter "white list" through.
    def phone_params
      params.require(:phone).permit(:imei, :brand, :model, :os, :device_token)
    end
end
