# frozen_string_literal: true

module Admin
  extend ActiveSupport::Concern

  # included do
  #   before_action :something
  # end
  #
  # Those helpers are user for finding followers and following of any given object
  # the serializer return a list of users.
  def is_admin
    unless current_user.has_role? :admin
      render json: {"error": "Only an admin can do this action"}, status: :forbidden
    end
  end
end
