require 'net/http'

class SlackAuthController < ApplicationController
  before_action :authenticate_user!, only: [:get_auth_url]
  before_action :parse_state, only: [:create]
  before_action :get_oauth, only: [:create]

  def create
    if @infos["ok"]
      slack_auth = SlackAuth.new
      slack_auth.token = @infos["access_token"]
      slack_auth.scope = @infos["scope"]
      slack_auth.slack_user_id = @infos["user_id"]
      slack_auth.slack_team_id = @infos["team_id"]
      slack_auth.slack_team_name = @infos["team_name"]
      slack_auth.slack_enterprise_id = @infos["enterprise_id"]
      slack_auth.team_id = @team_id
      slack_auth.user_id = @user_id
      slack_auth.save!
    end
    redirect_to ENV['FRONTEND_URL'] + '/team'
  end

  def get_auth_url
    uri = URI('https://slack.com/oauth/authorize')
    p = {
      client_id: ENV["SLACK_CLIENT_ID"],
      scope: "channels:read groups:read im:read mpim:read usergroups:read channels:history groups:history im:history mpim:history reactions:read team:read users.profile:read users:read users:read.email",
      redirect_uri: ENV['BACKEND_URL'] + "/api/auth/slack/callback",
      state: current_user.team.id.to_s + ',' + current_user.id.to_s
    }
    uri.query = URI.encode_www_form(p)
    render json: {url: uri.to_s}, status: :ok
  end

  def parse_state
    if params[:state] and params[:state].split(',').count == 2
      @team_id, @user_id = params[:state].split(',')
      @team_id = @team_id.to_i
      @user_id = @user_id.to_i
    else
      @team_id = -1
      @user_id = -1
    end
  end

  def get_oauth
    uri = URI('https://slack.com/api/oauth.access')
    p = {
      client_id: ENV['SLACK_CLIENT_ID'],
      client_secret: ENV['SLACK_CLIENT_SECRET'],
      code: params[:code],
      redirect_uri: ENV['BACKEND_URL'] + "/api/auth/slack/callback"
    }
    uri.query = URI.encode_www_form(p)
    res = Net::HTTP.get_response(uri)
    @infos = JSON.parse(res.body)
  end
end
