class LogsController < ApplicationController
  before_action :authenticate_user!
  #before_action :is_admin, only: [:destroy]
  before_action :set_log, only: [:show, :update, :destroy]

  include Admin

  # GET /logs
  def index
    @date = Date.parse(params[:current_date]) rescue Date.today
    @logs = Log.where(user: current_user).where("task_done_at <= ?", @date + 1).where("task_finished_at >= ?", @date)
    render json: @logs
  end

  # GET /logs/1
  def show
    render json: @log
  end

  # POST /logs
  def create
    @log = Log.new(log_params)
    @log.user = current_user
    if @log.task_finished_at.nil?
      @log.task_finished_at = @log.task_done_at
    end
    if @log.save
      unless params[:log][:users].nil?
        @log.set_users(params[:log][:users])
      else
        @log.set_igem_users(params[:log][:igem_users])
      end
      @log.set_relation_categories(params[:log][:relation_categories])
      render json: @log, status: :created, location: @log
    else
      render json: @log.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /logs/1
  def update
    if @log.update(log_params)
      @log.users = []
      @log.relation_categories = []
      unless params[:log][:users].nil?
        @log.set_users(params[:log][:users])
      else
        @log.set_igem_users(params[:log][:igem_users])
      end
      @log.set_relation_categories(params[:log][:relation_categories])
      render json: @log
    else
      render json: @log.errors, status: :unprocessable_entity
    end
  end

  # DELETE /logs/1
  def destroy
    if @log.destroy
      render status: :ok
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_log
      if Log.exists?(params[:id])
        @log = Log.find(params[:id])
        if @log.user.id != current_user.id
          render json: {"error": "You can only access your logs"}, status: :forbidden and return
        end
      else
        render json: {"error": "This log does not exist"}, status: :unprocessable_entity
      end
    end


    # if @log.create_at > Time.now - 7.days
    # else
    #   render json: {"error": "Not allowed to delete this task"}, status: :forbidden
    # end

    # Only allow a trusted parameter "white list" through.
    def log_params
      params.require(:log).permit(:user_id, :task_id, :team_id, :task_done_at, :task_finished_at)
    end
end
