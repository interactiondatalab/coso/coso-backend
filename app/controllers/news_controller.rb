class NewsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_news, only: [:show, :update, :destroy]

  # GET /news
  def index
    if current_user.has_role? :admin
      @news = News.all
    else
      @news = News.where(team:nil).or(News.where(team: current_user.team)).with_read_marks_for(current_user)
    end
    render json: @news
  end

  # GET /news/1
  def show
    @news.mark_as_read! for: current_user
    render json: @news
  end

  # POST /news
  def create
    @news = News.new(news_params)

    if @news.save
      render json: @news, status: :created, location: @news
    else
      render json: @news.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /news/1
  def update
    if @news.update(news_params)
      render json: @news
    else
      render json: @news.errors, status: :unprocessable_entity
    end
  end

  # DELETE /news/1
  def destroy
    @news.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_news
      @news = News.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def news_params
      params.require(:news).permit(:team_id,:content,:image,:title,:action_text,:action_url)
    end
end
