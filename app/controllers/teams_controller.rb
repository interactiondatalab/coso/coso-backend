class TeamsController < ApplicationController
  #before_action :authenticate_user!, except: [:exist]
  before_action :is_admin, only: [:update, :destroy]
  before_action :set_team, only: [:show, :update, :destroy, :members, :members_igem]
  before_action :is_my_team, only: [:show]

  include Admin

  # GET /teams
  def index
    @pagy, @teams = pagy(Team.includes([:users, :igem_users]).all)
    render json: @teams
  end

  # GET /teams
  def list
    @teams = Team.pluck(:id, :name)
    render json: @teams
  end

  # GET /teams/1
  def show
    render json: @team
  end

  def exist
    @team = Team.find_by(["lower(name) = ?", params[:name].downcase])
    unless @team.nil?
      render json: {"team_id": @team.id}, status: :ok
    else
      render status: :not_found
    end
  end

  # POST /teams
  def create
    @team = Team.new(team_params)

    if @team.save
      render json: @team, status: :created, location: @team
    else
      render json: @team.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /teams/1
  def update
    if @team.update(team_params)
      render json: @team
    else
      render json: @team.errors, status: :unprocessable_entity
    end
  end

  def members
    render json: @team.users, each_serializer: UserSerializer, status: :ok
  end

  def members_igem
    render json: @team.igem_users, include: ['user.phones'], each_serializer: IgemUserSerializer, status: :ok
  end

  # DELETE /teams/1
  def destroy
    if @team.destroy
      render status: :ok
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_team
      if Team.exists?(params[:id])
        @team = Team.includes([:users, :igem_users]).find(params[:id])
      else
        render json: {"error": "This team does not exist"}, status: :unprocessable_entity
      end
    end

    def is_my_team
      render json: {"error": "You can only access your team"}, status: :forbidden and return if current_user.team.id != params[:id].to_i
    end

    # Only allow a trusted parameter "white list" through.
    def team_params
      params.require(:team).permit(:name, :year, :country, :registered, :url, :igem_team_id)
    end
end
