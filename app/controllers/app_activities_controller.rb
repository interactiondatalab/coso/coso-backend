class AppActivitiesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_app_activity, only: [:show, :update, :destroy]
  before_action :is_admin, expect: [:create]


  include Admin

  # GET /app_activities
  def index
    @pagy, @app_activities = pagy(AppActivity.all)
    render json: @app_activities
  end

  # GET /app_activities/1
  def show
    render json: @app_activity
  end

  # POST /app_activities
  def create
    @app_activity = AppActivity.new(app_activity_params)
    @app_activity.user = current_user

    if @app_activity.save
      render json: @app_activity, status: :created, location: @app_activity
    else
      render json: @app_activity.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /app_activities/1
  def update
    if @app_activity.update(app_activity_params)
      render json: @app_activity
    else
      render json: @app_activity.errors, status: :unprocessable_entity
    end
  end

  # DELETE /app_activities/1
  def destroy
    if @app_activity.destroy
      render status: :ok
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_app_activity
      if AppActivity.exists?(params[:id])
        @app_activity = AppActivity.find(params[:id])
      else
        render json: {"error": "This app activity does not exist"}, status: :unprocessable_entity
      end
    end

    # Only allow a trusted parameter "white list" through.
    def app_activity_params
      params.require(:app_activity).permit(:phone_id, :start_at, :end_at)
    end
end
