class SurveysController < ApplicationController
  before_action :authenticate_user!, except: [:questionnaires, :answer_no_auth]
  before_action :set_survey, only: [:show, :update, :destroy, :answer, :is_completed]
  before_action :is_admin, only: [:create, :update]

  include Admin

  # GET /surveys
  def index
    @surveys = Survey.where(team_id: current_user.team.id).or(Survey.where(team_id: nil)).all
    render json: @surveys
  end

  def questionnaires
    @surveys = Survey.where(team_id: params["team_id"]).all
    render json: @surveys
  end

  # GET /surveys/1
  def show
    render json: @survey
  end

  # POST /surveys
  def create
    @survey = Survey.new(survey_params)
    if @survey.save
      params[:survey][:survey_fields].each do |field|
        @survey_field = SurveyField.new()
        @survey_field.survey = @survey
        @survey_field.category = field["category"]
        @survey_field.name = field["name"]
        @survey_field.required = field["required"]
        @survey_field.content = field["content"]
        @survey_field.save
      end
      render json: @survey, status: :created, location: @survey
    else
      render json: @survey.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /surveys/1
  def update
    if @survey.update(survey_params)
      render json: @survey
    else
      render json: @survey.errors, status: :unprocessable_entity
    end
  end

  # DELETE /surveys/1
  def destroy
    if @survey.destroy
      render status: :ok
    end
  end

  # GET /surveys/user
  def is_completed
    @nb_fields_completed = SurveyDatum.where(survey_id: @survey.id, user_id: current_user.id.to_i).count
    render json: @nb_fields_completed
  end

  def answer
    params[:data].each do |data|
      datum = SurveyDatum.new()
      datum.user = current_user
      datum.survey = @survey
      field = SurveyField.find(data["survey_field_id"])
      datum.survey_field = field
      datum.content = data["content"]
      unless datum.save
        render json: datum.errors, status: :unprocessable_entity and return
      end
    end
    render status: :created
  end

  def answer_no_auth
    @survey = Survey.find(params[:id])

    params[:data].each do |data|
      datum = SurveyDataNoAuth.new()
      datum.igem_user_id = params[:igem_user_id]
      datum.survey = @survey
      field = SurveyField.find(data["survey_field_id"])
      datum.survey_field = field
      datum.content = data["content"]
      unless datum.save
        render json: datum.errors, status: :unprocessable_entity and return
      end
    end

    render status: :created
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_survey
      if Survey.exists?(params[:id])
        @survey = Survey.find(params[:id])
        unless @survey.team.nil?
          if @survey.team.id != current_user.team.id
            render json: {"error": "You cannot look at other teams survey"}, status: :forbidden
          end
        end
      else
        render json: {"error": "This survey does not exist"}, status: :unprocessable_entity
      end
    end

    # Only allow a trusted parameter "white list" through.
    def survey_params
      params.require(:survey).permit(:team_id, :name, :description, :time)
    end
end
