class UsersController < ApplicationController
  before_action :authenticate_user!, except: [:exist, :recover_password]
  before_action :is_self, only: [:update, :delete]
  before_action :set_user, only: [:show, :update, :destroy]
  before_action :is_admin, only: [:index]

  include Admin

  # GET /users
  def index
    if current_user.has_role? :admin
      @pagy, @users = pagy(User.includes([:phones]).all)
      render json: @users
    else
      unless params[:team_id]
        render json: {error: "Missing team_id"}, status: :unprocessable_entity
      else
        @igem_users = IgemUser.where(team_id: params[:team_id]).all
        render json: @igem_users
      end
    end
  end

  # GET /users/1
  def show
    render json: @user
  end

  # GET /users/1
  def get_self
    render json: current_user
  end

  def exist
    if IgemUser.exists?(["lower(username) = ?", params[:username].downcase])
      render status: :ok
    else
      render status: :not_found
    end
  end

  def check_admin_status
    if current_user.has_role? :admin
      render status: :ok
    else
      render status: :forbidden
    end
  end

  # # POST /users
  # Is handled by the registration_controller.rb#create

  # PATCH/PUT /users/1
  def update
    puts user_params
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  def destroy
    igem_user = IgemUser.find_by(user_id: @user.id)
    if @user.destroy
      igem_user.user_id = nil
      igem_user.registered = false
      igem_user.save

      render status: :ok
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      if User.exists?(params[:id])
        @user = User.includes([:phones]).find(params[:id])
      else
        render json: {"error": "This user does not exist"}, status: :unprocessable_entity
      end
    end

    def is_self
      render json: {"error": "You can only modify yourself !"}, status: :forbidden and return if current_user.id != params[:id].to_i
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation, :username)
    end
end
