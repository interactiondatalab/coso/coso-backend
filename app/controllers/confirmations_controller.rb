class ConfirmationsController < Devise::ConfirmationsController
  def generate_url(url, params = {})
    uri = URI(url)
    uri.query = params.to_query
    uri.to_s
  end

  def show
    self.resource = resource_class.confirm_by_token(params[:confirmation_token])
    yield resource if block_given?

    redirect_to generate_url("cosoapp://")
  end

end
