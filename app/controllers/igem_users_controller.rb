class IgemUsersController < ApplicationController
  before_action :authenticate_user!, except: [:exist, :index]
  before_action :is_admin, only: [:create, :update_members]
  before_action :set_igem_user, only: [:show, :update, :destroy]
  before_action :is_team_member, only: [:create, :update, :destroy]

  include Admin

  # GET /igem_users
  def index
    unless params[:team_id]
      render json: {error: "Missing team_id"}, status: :unprocessable_entity
    else
      @igem_users = IgemUser.where(team_id: params[:team_id]).all
      render json: @igem_users
    end
  end

  # GET /igem_users/1
  def show
    render json: @igem_user
  end

  def exist
    if IgemUser.exists?(["lower(username) = ?", params[:username].downcase])
      render status: :ok
    else
      render status: :not_found
    end
  end

  # POST /igem_users
  def create
    @igem_user = IgemUser.new(igem_user_params)
    if @igem_user.save
      render json: @igem_user, status: :created, location: @igem_user
    else
      render json: @igem_user.errors, status: :unprocessable_entity
    end
  end

  def add
    @igem_user = IgemUser.new(igem_user_params)
    @igem_user.team = current_user.team
    @igem_user.custom = true
    @igem_user.registered = false

    if @igem_user.full_name.nil?
      @igem_user.full_name = @igem_user.username
    end

    if @igem_user.save
      render json: @igem_user, status: :created, location: @igem_user
    else
      render json: @igem_user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /igem_users/1
  def update
    if @igem_user.update(igem_user_params_update)
      render json: @igem_user
    else
      render json: @igem_user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /igem_users/1
  def destroy
    if @igem_user.destroy
      render status: :ok
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_igem_user
      if IgemUser.exists?(params[:id])
        @igem_user = IgemUser.find(params[:id])
      else
        render json: {"error": "This igem user does not exist"}, status: :unprocessable_entity
      end
    end

    def is_team_member
      unless current_user.team.id == @igem_user.team.id
        render json: {"error": "You can't create users for other teams"}, status: :forbidden and return
      end
    end

    def is_self
      unless current_user.igem_user.id == params[:id].to_i or current_user.has_role? :admin
        render json: {"error": "You can't update someone else information"}, status: :forbidden and return
      end
    end

    # Only allow a trusted parameter "white list" through.
    def igem_user_params
      params.require(:igem_user).permit(:username, :full_name, :registered, :team_id, :user_id, :custom)
    end

    def igem_user_params_update
      params.require(:igem_user).permit(:full_name, :user_id)
    end
end
