class RelationCategoriesController < ApplicationController
  before_action :authenticate_user!
  before_action :is_admin, only: [:update, :destroy]
  before_action :set_relation_category, only: [:show, :update, :destroy]

  include Admin

  # GET /logs
  def index
    @relation_categories = RelationCategory.where(team: current_user.team).or(RelationCategory.where(team:nil)).all
    render json: @relation_categories
  end

  # GET /logs/1
  def show
    render json: @relation_category
  end

  # POST /logs
  def create
    @relation_category = RelationCategory.new(relation_category_params)
    @relation_category.team = current_user.team

    if @relation_category.save
      render json: @relation_category, status: :created, location: @relation_category
    else
      render json: @relation_category.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /logs/1
  def update
    if @relation_category.update(relation_category_params)
      render json: @relation_category
    else
      render json: @relation_category.errors, status: :unprocessable_entity
    end
  end

  # DELETE /logs/1
  def destroy
    if @relation_category.destroy
      render status: :ok
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_relation_category
      if RelationCategory.exists?(params[:id])
        @relation_category = RelationCategory.find(params[:id])
      else
        render json: {"error": "This relation category does not exist"}, status: :unprocessable_entity
      end
    end

    # Only allow a trusted parameter "white list" through.
    def relation_category_params
      params.require(:relation_categories).permit(:name)
    end
end
