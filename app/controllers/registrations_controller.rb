class RegistrationsController < Devise::RegistrationsController
  respond_to :json

  before_action :username_exists?, only: [:create]
  before_action :email_exists?, only: [:create]
  before_action :set_team, only: [:create]
  before_action :set_igem_user, only: [:create]

  def create
    user = User.new(register_params)
    user.team = @team
    user.igem_user = @igem_user
    user.save
    user.igem_user_id = @igem_user.id
    puts igem_user_params[:full_name]
    if user.save!
      @igem_user.registered = true
      @igem_user.full_name = igem_user_params[:full_name]
      @igem_user.save
      render json: user, status: :created
    else
      render json: {"error": "Somethings went wrong with the parameters update"}, status: :unprocessable_entity
    end
  end

  def username_exists?
    user = User.find_by(["lower(username) = ?", register_params[:username].downcase])
    render json: {"error": "Username already registered !"}, status: :bad_request and return unless user.nil?
  end

  def email_exists?
    user = User.find_by(["lower(email) = ?", register_params[:email].downcase])
    render json: {"error": "Email already registered !"}, status: :bad_request and return unless user.nil?
  end

  def set_igem_user
    #@igem_user = IgemUser.find_or_create_by(username: register_params[:username])
    @igem_user = IgemUser.find_by(["lower(username) = ?", register_params[:username].downcase])
    if @igem_user.nil?
      @igem_user = IgemUser.create(username: register_params[:username], custom: true)
      @igem_user.team = @team
    else
      if @team != @igem_user.team
        render json: {"error": "This team does not belong to this IgemUser"}, status: :unprocessable_entity
      end
    end

    render json: {"error": "This igem user is already registered"}, status: :unprocessable_entity if @igem_user.registered?
  end

  def set_team
    @team = Team.find_by(id: register_params[:team_id])
    render json: {"error": "This team does not exist"}, status: :not_found if @team.nil?
  end

  def register_params
    params.require(:user).permit(:email, :password, :username, :team_id)
  end

  def igem_user_params
    params.require(:user).permit(:username, :full_name)
  end
end
