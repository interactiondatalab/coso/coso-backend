class TasksController < ApplicationController
  before_action :authenticate_user!
  before_action :is_admin, except: [:index, :show, :create]
  before_action :set_task, only: [:show, :update, :destroy]

  include Admin

  # GET /tasks
  def index
    @tasks = Task.where(team: current_user.team).or(Task.where(team:nil)).all
    render json: @tasks.order(:name)
  end

  # GET /tasks/1
  def show
    render json: @task
  end

  # POST /tasks
  def create
    @task = Task.new(task_params)
    @task.team = current_user.team

    if @task.save
      render json: @task, status: :created, location: @task
    else
      render json: @task.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /tasks/1
  def update
    if @task.update(task_params)
      render json: @task
    else
      render json: @task.errors, status: :unprocessable_entity
    end
  end

  # DELETE /tasks/1
  def destroy
    if @task.destroy
      render status: :ok
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task
      if Task.exists?(params[:id])
        @task = Task.find(params[:id])
      else
        render json: {"error": "This task does not exist"}, status: :unprocessable_entity
      end
    end

    # Only allow a trusted parameter "white list" through.
    def task_params
      params.require(:task).permit(:category, :name, :description)
    end
end
