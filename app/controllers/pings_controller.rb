class PingsController < ApplicationController
  before_action :authenticate_user!
  before_action :is_admin, only: [:index, :show, :update, :destroy]
  before_action :set_ping, only: [:show, :update, :destroy]

  include Admin

  # GET /pings
  def index
    @pagy, @pings = pagy(Ping.all)
    render json: @pings
  end

  # GET /pings/1
  def show
    render json: @ping
  end

  # POST /pings
  def create
    @ping = Ping.new(ping_params)
    @ping.user = current_user
    if @ping.save
      render json: @ping, status: :created, location: @ping
    else
      render json: @ping.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /pings/1
  def update
    if @ping.update(ping_params)
      render json: @ping
    else
      render json: @ping.errors, status: :unprocessable_entity
    end
  end

  # DELETE /pings/1
  def destroy
    if @ping.destroy
      render status: :ok
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ping
      if Ping.exists?(params[:id])
        @ping = Ping.find(params[:id])
      else
        render json: {"error": "This ping does not exist"}, status: :unprocessable_entity
      end
    end

    # Only allow a trusted parameter "white list" through.
    def ping_params
      params.require(:ping).permit(:phone_id, :target_id, :target_phone_id, :rssi, :ping_at)
    end
end
