require 'date'

class IgemController < ApplicationController
  before_action :authenticate_user!
  before_action :is_admin, only: [:update_members, :update_edits, :update_pages]
  before_action :set_team, except: [:update_members, :update_edits, :update_pages]

  include Admin

  def get_team
    team = Team.find_by(igem_team_id: params[:igem_team_id])
    render json: {team_id: team.id, team_name: team.name, year: team.year}, status: :ok
  end

  def update_pages
    team = Team.find(params[:team_id])
    params[:pages].each do |page|
      url = page[0]
      name = page[1]
      page = IgemPage.find_or_create_by(team: team, url: url, name: name)
    end
    render json: {status: "ok"}, status: :ok
  end

  def update_edits
    team = Team.find(params[:team_id])
    params[:edits].each do |edit|
      unless edit[1].downcase == "igem_hq"
        user = IgemUser.where(["lower(username) = ?", edit[1].downcase]).first
        page = IgemPage.find_by(team: team, url: edit[0])
        if user.nil?
          render json: {error: "Cannot find username for edit"}, status: :not_found and return
        end
        edit_time = DateTime.strptime(edit[2].to_s,'%s')
        page_size = edit[3]
        edit_size = edit[4]
        edit = IgemEdit.find_or_create_by(team: team, igem_page: page, igem_user: user, edit_time: edit_time, edit_size: edit_size, page_size: page_size)
      end
    end
    render json: {status: "ok"}, status: :ok
  end

  def update_members
    team = Team.find(params[:team_id])
    params[:members].each do |member|
      team_role = member[3]
      username = member[4]
      full_name = member[5]
      igemuser = IgemUser.find_or_create_by(username: username, team: team)
      igemuser.team_role = team_role
      igemuser.full_name = full_name
      unless igemuser.save
        render json: {error: igemuser.errors}, status: :unprocessable_entity and return
      end
    end
    render json: {status: "ok"}, status: :ok
  end

  def set_team
    @team = current_user.team
  end
end
