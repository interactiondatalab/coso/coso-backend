class StatsController < ApplicationController
  before_action :authenticate_user!
  before_action :is_admin, only: [:index, :show, :update, :destroy]
  before_action :set_team, only: [:show, :update, :destroy]

  include Admin

  def streak
    current_user.compute_streaks
    render json: {longest_streak: current_user.longest_streak, current_streak: current_user.current_streak}
  end

  def tasks
    total_tasks = Log.where(user_id: current_user.id).count
    tasks_counts = current_user.logs.group_by(&:task_id).map do |task_id, logs|
       {
         name: Task.find(task_id).name,
         count: logs.count
       }
     end
     render json: {total_tasks: total_tasks, tasks_counts: tasks_counts}
  end

  def collaborators
    collaborators = LogUser.where(log_id: LogUser.where(user_id: current_user.id).pluck(:log_id))
                           .or(LogUser.where(log_id: Log.where(user_id: current_user.id)))
                           .group_by(&:user_id)
                           .map do |user_id, logusers|
      unless user_id == current_user.id
        {
          user_id: user_id,
          username: User.find_by(id: user_id)&.username,
          count: logusers.count
        }
      end
    end
    render json: {collaborators: collaborators.compact}
  end

  def team_log_nb_per_day
    logs = Log.where(team: current_user.team).group_by_day(:created).count
    render json: {logs: logs}, status: :ok
  end

  def team_logs
    logs = Log.where(team: current_user.team)
    render json: logs, each_serializer: LogSerializer, status: :ok
  end

  def global
    teams = User.distinct.where.not(team_id: 301).pluck(:team_id).count
    logs = Log.joins(:team).where.not(team_id: 301).count
    collaborations = LogUser.where.not(user_id: Team.find(301).users).count
    top5_streak = User.where.not(team_id: 301).order(longest_streak: :desc).first(5).map do |user|
      {
        username: user.username,
        longest_streak: user.longest_streak
      }
    end
    render json: {
      teams: teams,
      logs: logs,
      collaborations: collaborations,
      top5_streak: top5_streak
    }
  end

  def logs_frequency
    logs = Log.where(team: current_user.team)
    total = Float(logs.count)
    result = logs.joins(:task).group(:task_id).count.map do |task_id, count|
      task = Task.find(task_id)
      {
        task_id: task.id,
        category: task.category,
        name: task.name,
        description: task.description,
        count: count,
        frequency: count / total
      }
    end
    render json: result, status: :ok
  end

  def network
    logs = Log.where(team: current_user.team).preload(:user, :users, :task)
    result = {}
    logs.each do |log|
      unless log.user.nil?
        users = log.users.pluck(:username)
        users << log.user.username
        task_name = log.task.name
        users.each do |userA|
          if result[userA].nil?
            result[userA] = {}
          end
          users.each do |userB|
            if result[userA][userB].nil?
              result[userA][userB] = {}
            end
            if result[userA][userB][task_name].nil?
              result[userA][userB][task_name] = 0
            end
            result[userA][userB][task_name] += 1
          end
        end
      end
    end
    render json: result, status: :ok
  end

end
