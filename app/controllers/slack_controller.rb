class SlackController < ApplicationController
  before_action :authenticate_user!
  before_action :set_team

  def get_users_reaction_count
    reactions_users = SlackReaction.joins(:slack_user).group(:name, :display_name).count
    data = {}
    users = []
    reactions_users.keys.each do |key|
      if key[1]
        users << key[1]
      end
      unless data[key[1]]
        data[key[1]] = {total: 0, emojis: {}}
      end
      root = key[0].split("::")[0]
      unless data[key[1]][:emojis][root]
        data[key[1]][:emojis][root] = 0
      end
      data[key[1]][:emojis][root] += reactions_users[key]
      data[key[1]][:total] += reactions_users[key]
    end
    render json: {reactions_users: {data: data, users: users}}, status: :ok
  end

  def get_user_reaction_matrix
    reactions_users = SlackReaction.joins(:slack_user).group(:name, :display_name).count
    users = []
    emojis = []
    reactions_users.keys.each do |key|
      unless users.include?(key[1])
        users << key[1]
      end
      unless emojis.include?(key[0])
        emojis << key[0]
      end
    end
    data = []
    keys = []
    users.each do |user|
      tmp = {
        users: user
      }
      emojis.each do |emoji|
        root = emoji.split("::")[0]
        unless keys.include? root
          keys << root
        end
        unless tmp[root]
          tmp[root] = 0
        end
        if reactions_users[[emoji, user]]
          tmp[root] += reactions_users[[emoji, user]]
        end
      end
      data << tmp
    end
    render json: {reactions_users: {data: data, keys: keys}}, status: :ok
  end

  def get_daily_message
    messages = SlackMessage.where(team: @team).group_by_day(:created).count
    data = []
    messages.keys.each do |key|
      if messages[key] > 0
        data << {
          day: key.to_s,
          value: messages[key]
        }
      end
    end
    render json: {reactions: data}, status: :ok
  end

  def get_reactions_count
    reactions = SlackReaction.where(team: @team).joins(:slack_message).group_by_day(:created).count
    data = []
    reactions.keys.each do |key|
      if reactions[key] > 0
        data << {
          day: key.to_s,
          value: reactions[key]
        }
      end
    end
    render json: {reactions: data}, status: :ok
  end

  def get_reactions
    reactions = SlackReaction.joins(:slack_message).group_by_day(:created).group(:name).count
    data = []
    reactions.keys.each do |key|
      data << {
        day: key.to_s,
        value: reaction[key]
      }
    end
    render json: {reactions: reactions}, status: :ok
  end


  def get_mentions
    counts = SlackMention.group(:source_id, :target_id).order(:source_id).count
    users = SlackUser.where(team: @team).where.not(email: nil)
    keys = []
    matrix = []
    users.each do |source|
      keys << source.name
      tmp = []
      users.each do |target|
        count = counts[[source.id, target.id]]
        unless count.nil?
          tmp << count
        else
          tmp << 0
        end
      end
      matrix << tmp
    end
    render json: {mentions: {keys: keys, matrix: matrix}}, status: :ok
  end

  def get_users
    users = SlackUser.where(team: @team)
    render json: users, each_serializer: SlackUserSerializer, status: :ok
  end

  def is_linked
    if SlackAuth.where(team: @team).count > 0
      render json: {success: true}, status: :ok
    else
      render json: {success: false}, status: :not_found
    end
  end

  def set_team
    @team = current_user.team
  end

end
