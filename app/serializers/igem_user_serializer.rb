class IgemUserSerializer < ActiveModel::Serializer
  attributes :id,
             :username,
             :full_name,
             :custom

  belongs_to :user
end
