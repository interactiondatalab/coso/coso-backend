class NewsSerializer < ActiveModel::Serializer
  attributes :id,
             :title,
             :content,
             :image,
             :action_text,
             :action_url,
             :unread,
             :created_at

  def unread
    current_user.have_read? object
  end
end
