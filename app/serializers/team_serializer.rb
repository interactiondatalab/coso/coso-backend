class TeamSerializer < ActiveModel::Serializer
  attributes :id,
             :year,
             :name,
             :country

  has_many :users
  has_many :igem_users

  attributes :pi_count
  attributes :student_count
  attributes :student_leader
end
