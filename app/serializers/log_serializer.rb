class LogSerializer < ActiveModel::Serializer
  attributes :id,
             :user_id,
             :task_id,
             :users,
             :igem_users,
             :relation_categories,
             :team_id,
             :task_done_at,
             :task_finished_at

  def users
    object.users.group_by{ |user| user.team_id }
  end

  def igem_users
    object.igem_users.group_by{ |igem_user| igem_user.team_id }
  end

  def relation_categories
    object.relation_categories
  end
end
