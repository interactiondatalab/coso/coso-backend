class TaskSerializer < ActiveModel::Serializer
  attributes :id,
             :category,
             :name,
             :description

end
