class PhoneSerializer < ActiveModel::Serializer
  attributes :id,
             :user_id,
             :brand,
             :model,
             :imei,
             :os,
             :device_token
end
