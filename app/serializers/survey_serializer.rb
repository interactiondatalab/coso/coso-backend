class SurveySerializer < ActiveModel::Serializer
  attributes :id,
             :name,
             :description,
             :time,
             :minutes

  has_many :survey_fields

  def minutes
    unless object.time.nil?
      "#{object.time/60} minute".pluralize(object.time/60)
    else
      "1 minute"
    end
  end
end
