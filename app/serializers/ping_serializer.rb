class PingSerializer < ActiveModel::Serializer
  attributes :id,
             :user_id,
             :target_id,
             :phone_id,
             :target_phone_id,
             :rssi,
             :ping_at

end
