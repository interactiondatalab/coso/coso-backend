class SlackUserSerializer < ActiveModel::Serializer
  attributes :id,
             :user,
             :slack_user_id,
             :email,
             :name,
             :real_name,
             :display_name,
             :updated
end
