class UserSerializer < ActiveModel::Serializer
  attributes :id,
             :username,
             :email,
             :verified,
             :log_count

  has_many :phones
  has_one :igem_user
  belongs_to :team
end
