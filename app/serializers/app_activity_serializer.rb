class AppActivitySerializer < ActiveModel::Serializer
  attributes :id,
             :user_id,
             :phone_id,
             :start_at,
             :end_at
end
