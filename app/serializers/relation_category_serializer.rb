class RelationCategorySerializer < ActiveModel::Serializer
  attributes :id,
             :name
end
