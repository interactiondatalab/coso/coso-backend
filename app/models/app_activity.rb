class AppActivity < ApplicationRecord
  belongs_to :user
  belongs_to :phone

  validates :user, :phone, :start_at, presence: true
end
