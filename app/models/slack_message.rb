class SlackMessage < ApplicationRecord
  belongs_to :team
  belongs_to :slack_user
  belongs_to :slack_channel
  belongs_to :parent, class_name: 'SlackMessage', optional: true
  has_many :children, class_name: 'SlackMessage', foreign_key: 'parent_id'

  has_many :slack_reactions
end
