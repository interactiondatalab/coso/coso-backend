class SlackMention < ApplicationRecord
  belongs_to :source, class_name: 'SlackUser', foreign_key: 'source_id'
  belongs_to :target, :polymorphic => true
  belongs_to :slack_message
  belongs_to :team
end
