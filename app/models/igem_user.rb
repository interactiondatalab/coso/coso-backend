class IgemUser < ApplicationRecord

  belongs_to :user, optional: true
  belongs_to :team
  has_many :igem_edits

  validates :team, :username, presence: true
  validates :username, uniqueness: true
end
