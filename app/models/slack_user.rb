class SlackUser < ApplicationRecord
  belongs_to :team
  belongs_to :user, optional: true
  has_many :messages
  has_many :slack_channel_users
  has_many :slack_channels, through: :slack_channel_users
end
