# frozen_string_literal: true

class Notification < NotificationHandler::Notification
  include Notifications

  after_create :push_notifications

  def push_notifications
    self.deliver(:firebase)
    # We can add more methods later on like push notifications on apps
  end

  def title
    if type == "new_survey"
      "A new survey is available!"
    elsif type == "reminder"
      "Activity reminder"
    elsif type == "news"
      "Some news from the CoSo team!"
    end
  end

  def content
    if type == "new_survey"
      "Check your COSO App to find the new survey!"
    elsif type == "reminder"
      "Have you reported your activities this week?"
    elsif type == "new_news"
      "Read the news in the CoSo App!"
    end
  end

  def type_notification
    type
  end
end
