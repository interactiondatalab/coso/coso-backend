class Team < ApplicationRecord
  resourcify
  has_many :igem_users
  has_many :users

  validates :name, presence: true
  validates :year, presence: true

  def student_count
    student.count
  end

  def student_leader_count
    student_leader.count
  end

  def pi_count
    pi.count
  end

  def student
    User.with_role(:student, self)
  end

  def student_leader
    User.with_role(:student_leader, self)
  end

  def pi
    User.with_role(:pi, self)
  end
end
