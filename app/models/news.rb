class News < ApplicationRecord
  acts_as_readable

  belongs_to :team, optional: true

  after_create :notify_team_new_news

  def notify_team_new_news
    if team
      Notification.for_group(
        :members,
        args: [team],
        attrs: {
          category: :news,
          type: 'new_news',
          object: self
        }
      )
    else
      Team.all.each do |team|
        Notification.for_group(
          :members,
          args: [team],
          attrs: {
            category: :news,
            type: 'new_news',
            object: self
          }
        )
      end
    end
  end
end
