class User < ApplicationRecord
  rolify
  notification_target
  notification_object
  acts_as_reader

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :validatable,
         :confirmable, :timeoutable,
         :jwt_authenticatable, jwt_revocation_strategy: JWTBlacklist

  validates :email, :username, uniqueness: true, presence: true

  belongs_to :team
  has_one :igem_user

  has_many :phones
  has_many :logs
  has_many :app_activities
  has_many :survey_data
  has_many :pings

  after_create :set_default

  geocoded_by :last_sign_in_ip

  def compute_streaks
    qry  = <<-SQL
    WITH

        dates(date) AS (
          SELECT DISTINCT CAST(task_done_at AS DATE)
          FROM logs
          WHERE logs.user_id = #{id}
        ),

        groups AS (
          SELECT
            ROW_NUMBER() OVER (ORDER BY date) AS rn,
            date - (ROW_NUMBER() OVER (ORDER BY date) * interval '1' day) AS grp,
            date
          FROM dates
        )

      SELECT
        COUNT(*) AS consecutive_dates,
        MIN(date) AS min_date,
        MAX(date) AS max_date
      FROM groups
      GROUP BY grp
      ORDER BY 1 DESC, 2 DESC
    SQL
    r = ActiveRecord::Base.connection.execute(qry)
    self.longest_streak = r.to_a.pluck("consecutive_dates").max
    if self.longest_streak.nil?
      self.longest_streak = 0
    end
    tmp = r.to_a.map do |streak|
      if streak["max_date"] == Date.today.to_s
        streak["consecutive_dates"]
      end
    end
    if tmp[0]
      self.current_streak = tmp[0]
    else
      self.current_streak = 0
    end
    save
  end

  def log_count
    logs.count
  end

  private

  def set_default
    current_streak = 0
    longest_streak = 0
  end
end
