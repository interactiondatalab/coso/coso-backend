class Log < ApplicationRecord
  belongs_to :user
  belongs_to :team
  belongs_to :task

  has_many :log_igem_users
  has_many :igem_users, through: :log_igem_users
  has_many :log_users
  has_many :users, through: :log_users
  has_many :log_relation_categories
  has_many :relation_categories, through: :log_relation_categories

  validates :task_done_at, presence: true
  validates :task_finished_at, presence: true

  after_create :recompute_streaks

  def recompute_streaks
    user&.compute_streaks
  end

  def set_users(user_ids)
    unless user_ids.nil?
      user_ids.each do |user_id|
        if User.exists?(user_id)
          users << User.find(user_id)
        end
      end
    end
  end

  def set_igem_users(igem_user_ids)
    unless igem_user_ids.nil?
      igem_user_ids.each do |igem_user_id|
        if IgemUser.exists?(igem_user_id)
          igem_user = IgemUser.find(igem_user_id)
          igem_users << igem_user
          unless igem_user.user.nil?
            users << igem_user.user
          end
        end
      end
    end
  end

  def set_relation_categories(relation_categories_ids)
    unless relation_categories_ids.nil?
      relation_categories_ids.each do |relation_category_id|
        if RelationCategory.exists?(relation_category_id)
          relation_categories << RelationCategory.find(relation_category_id)
        end
      end
    end
  end
end
