class RelationCategoryValidator < ActiveModel::Validator
  def validate(record)
    if record.team
      teams = Team.pluck(:name)
      unless teams.include? record.name
        record.errors.add :name, "The name is not valid. It must be another igem team!"
      end
      if record.name == record.team.name
        record.errors.add :name, "The name is not valid. It cannot be your own igem team!"
      end
    end
  end
end


class RelationCategory < ApplicationRecord
  include ActiveModel::Validations

  belongs_to :team, optional: true

  validates :name, presence: true
  validates_with RelationCategoryValidator
end
