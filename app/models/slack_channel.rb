class SlackChannel < ApplicationRecord
  belongs_to :team
  has_many :slack_channel_users
  has_many :slack_users, through: :slack_channel_users
end
