class Ping < ApplicationRecord

  belongs_to :user
  belongs_to :target, :class_name => "User", :foreign_key => "target_id"

  belongs_to :phone
  belongs_to :target_phone, :class_name => "Phone", :foreign_key => "target_phone_id"

  validates :user, :target, :phone, :target_phone, :rssi, :ping_at, presence: true

  before_validation :set_target

  def set_target
    self.target = Phone.find(target_phone_id).user unless target_phone_id.nil?
  end

end
