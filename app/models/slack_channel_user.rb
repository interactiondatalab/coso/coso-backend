class SlackChannelUser < ApplicationRecord
  belongs_to :team
  belongs_to :slack_user
  belongs_to :slack_channel
end
