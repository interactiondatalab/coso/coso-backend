class CategoryValidator < ActiveModel::Validator
  def validate(record)
    categories = Task.where(team:nil).pluck(:category)
    unless categories.include? record.category
      record.errors.add :category, "The category is not valid. Valid categories: #{categories}!"
    end
  end
end

class Task < ApplicationRecord
  include ActiveModel::Validations
  validates_with CategoryValidator
  
  has_many :logs

  belongs_to :team, optional: true

  validates :category, presence: true
end
