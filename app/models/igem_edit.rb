class IgemEdit < ApplicationRecord
  belongs_to :team
  belongs_to :igem_page
  belongs_to :igem_user
end
