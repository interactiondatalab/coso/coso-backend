class SlackUsergroupMember < ApplicationRecord
  belongs_to :team
  belongs_to :slack_usergroup
  belongs_to :slack_user
end
