class SurveyField < ApplicationRecord
  belongs_to :survey

  validates :survey, :category, :name, :content, presence: true
end
