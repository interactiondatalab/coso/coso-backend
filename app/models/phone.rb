class Phone < ApplicationRecord
  belongs_to :user

  validates :imei, :os, presence: true

  def ping_count
    Ping.where(phone_id: id).count
  end

end
