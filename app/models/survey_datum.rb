class SurveyDatum < ApplicationRecord
  belongs_to :user
  belongs_to :survey_field
  belongs_to :survey

  validates :user, :survey_field, :survey, presence: true

end
