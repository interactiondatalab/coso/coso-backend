require 'date'
require 'csv'

class SlackAuth < ApplicationRecord
  belongs_to :team
  belongs_to :user

  after_find do |auth|
    puts "Hey there"
    @client = Slack::Web::Client.new(token: token)
  end

  def client
    @client
  end

  def save_csv(klass)
    headers = klass.new.attributes.keys
    file = "#{Rails.root}/public/#{klass.name}.csv"
    CSV.open(file, 'w', write_headers: true, headers: headers) do |writer|
      klass.where(team: team).each do |k|
        tmp = []
        headers.each do |h|
          tmp << k[h]
        end
        writer << tmp
      end
    end
  end

  def save_all_as_csv
    save_csv SlackChannel
    save_csv SlackUser
    save_csv SlackUsergroup
    save_csv SlackUsergroupMember
    save_csv SlackChannelUser
    save_csv SlackMention
    save_csv SlackMessage
    save_csv SlackReaction
  end

  def set_threads(channel_id)
    SlackMessage.where(slack_channel_id: channel_id).each do |message|
      if message.thread_ts
        parent = SlackMessage.find_by(slack_channel_id: channel_id, ts: message.thread_ts)
        message.parent = parent
        message.save
      end
    end
  end

  def save_from_api
    save_users
    save_channels
    save_usergroups
    SlackChannel.where(team: team).each do |channel|
      unless channel.is_archived
        save_members(channel.channel_id)
      end
      save_messages(channel.channel_id)
      set_threads(channel.channel_id)
    end
  end

  def save_channels
    start = true
    res = true
    while start or res.response_metadata.next_cursor != ""
      if start
        start = false
        res = @client.conversations_list
      else
        res = @client.conversations_list(cursor: res.response_metadata.next_cursor)
      end
      res.channels.each do |channel|
        attr = {
          team: team,
          created: DateTime.strptime(channel.created.to_s,'%s'),
          channel_id: channel.id,
          is_archived: channel.is_archived,
          is_channel: channel.is_channel,
          is_ext_shared: channel.is_ext_shared,
          is_general: channel.is_general,
          is_group: channel.is_group,
          is_im: channel.is_im,
          is_member: channel.is_member,
          is_mpim: channel.is_mpim,
          is_org_shared: channel.is_org_shared,
          is_pending_ext_shared: channel.is_pending_ext_shared,
          is_private: channel.is_private,
          is_shared: channel.is_shared,
          name: channel.name,
          name_normalized: channel.name_normalized,
          num_members: channel.num_members,
          topic: channel.topic.value
        }
        chan = SlackChannel.find_or_create_by(team:team, channel_id: channel.id)
        chan.update(attr)
      end
    end
  end

  def save_messages(channel_id)
    slack_channel = SlackChannel.find_by(channel_id: channel_id)
    start = true
    res = true
    while start or !res.response_metadata.nil?
      if start
        start = false
        res = @client.conversations_history(channel:channel_id)
      else
        res = @client.conversations_history(channel:channel_id, cursor: res.response_metadata.next_cursor)
      end
      res.messages.each do |message|
        slack_user = SlackUser.find_by(slack_user_id: message.user)
        attr = {
          team: team,
          slack_channel: slack_channel,
          slack_user: slack_user,
          created: DateTime.strptime(message.ts.to_s,'%s'),
          thread_ts: message.thread_ts.to_s,
          ts: message.ts.to_s
        }
        msg = SlackMessage.find_or_create_by(attr)
        attr[:text] = message.text
        msg.update(attr)
        unless message.reactions.nil?
          message.reactions.each do |reaction|
            attr = {
              team: team,
              slack_message: msg,
              name: reaction.name
            }
            reaction.users.each do |user|
              attr[:slack_user] = SlackUser.find_by(slack_user_id: user)
              reaction = SlackReaction.find_or_create_by(attr)
            end
          end
        end
        /<@([A-Z0-9]{9})>/.match(message.text).to_a.drop(1).each do |mention|
          if mention[0] == "S"
            target = SlackUsergroup.find_or_create_by(slack_usergroup_id: mention)
          elsif mention[0] == "U"
            target = SlackUser.find_or_create_by(slack_user_id: mention)
          end
          SlackMention.find_or_create_by(team:team, slack_message: msg, source: slack_user, target: target)
        end
      end
    end
  end

  def save_members(channel_id)
    start = true
    res = true
    while start or res.response_metadata.next_cursor != ""
      if start
        start = false
        res = @client.conversations_members(channel:channel_id)
      else
        res = @client.conversations_members(channel:channel_id, cursor: res.response_metadata.next_cursor)
      end
      res.members.each do |member|
        slack_user = SlackUser.find_or_create_by(team: team, slack_user_id: member)
        SlackChannelUser.find_or_create_by(team:team, slack_channel: SlackChannel.find_by(channel_id: channel_id), slack_user: slack_user)
      end
    end
  end

  def save_usergroups
    res = @client.usergroups_list
    res.usergroups.each do |group|
      attr = {
        team: team,
        created: DateTime.strptime(group.date_create.to_s,'%s'),
        updated: DateTime.strptime(group.date_update.to_s,'%s'),
        deleted: DateTime.strptime(group.date_delete.to_s,'%s'),
        description: group.description,
        handle: group.handle,
        usergroup_id: group.id,
        is_external: group.is_external,
        is_subteam: group.is_subteam,
        is_usergroup: group.is_usergroup,
        name: group.name,
        user_count: group.user_count
      }
      slack_usergroup = SlackUsergroup.find_or_create_by(team: team, usergroup_id: group.id)
      slack_usergroup.update(attr)
      members = @client.usergroups_users_list(usergroup: group.id)
      members.users.each do |member|
        slack_user = SlackUser.find_by(slack_user_id: member)
        SlackUsergroupMember.find_or_create_by(team: team, slack_usergroup: slack_usergroup, slack_user: slack_user)
      end
    end
  end

  def save_users
    start = true
    res = true
    while start or res.response_metadata.next_cursor != ""
      if start
        start = false
        res = @client.users_list(include_locale:true)
      else
        res = @client.users_list(include_locale:true, cursor: res.response_metadata.next_cursor)
      end
      res.members.each do |member|
        slack_user = SlackUser.find_or_create_by(team: team, slack_user_id: member.id)
        attr = {
          team: team,
          slack_user_id: member.id,
          user: User.find_by(email: member.profile.email),
          email: member.profile.email,
          name: member.name,
          real_name: member.profile.real_name,
          display_name: member.profile.display_name,
          tz: member.tz,
          tz_label: member.tz_label,
          tz_offset: member.tz_offset,
          updated: DateTime.strptime(member.updated.to_s,'%s')
        }
        slack_user.update(attr)
      end
    end
  end
end
