class IgemPage < ApplicationRecord
  belongs_to :team
  has_many :igem_edits
end
