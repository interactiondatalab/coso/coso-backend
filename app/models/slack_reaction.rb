class SlackReaction < ApplicationRecord
  belongs_to :slack_user
  belongs_to :slack_message
  belongs_to :team

  def count
    SlackReaction.where(msg: msg, name: name).count
  end

  def created
    slack_message.created
  end

  def created_day
    slack_message.created.to_date
  end
end
