class SurveyDataNoAuth < ApplicationRecord
  belongs_to :igem_user
  belongs_to :survey_field
  belongs_to :survey

  validates :igem_user, :survey_field, :survey, :content, presence: true
end
