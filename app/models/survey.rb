class Survey < ApplicationRecord
  acts_as_readable
  belongs_to :team, optional: true
  has_many :survey_fields
  has_many :survey_data

  attribute :time, :interval

  validates :name, :description, presence: true

  after_create :notify_team_new_survey

  def notify_team_new_survey
    if team
      Notification.for_group(
        :members,
        args: [team],
        attrs: {
          category: :survey,
          type: 'new_survey',
          object: self
        }
      )
    else
      Team.all.each do |team|
        Notification.for_group(
          :members,
          args: [team],
          attrs: {
            category: :survey,
            type: 'new_survey',
            object: self
          }
        )
      end
    end

  end
end
