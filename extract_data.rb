require 'csv'
ActiveRecord::Base.logger.level = 1

igem_users = IgemUser.where.not(team_id: 301).order(:team_id)

# users_headers = ["username", "is_registered", "team_name", "country", "longest_streak", "current_streak", "nb_phones", "os"]
#
# csv = CSV.generate do |csv|
#   csv << users_headers
#
#   igem_users.each do |igem_user|
#     if igem_user.team.registered
#       os = "unknown"
#       nb_phones = 0
#       longest_streak = 0
#       current_streak = 0
#
#       if igem_user.registered
#         longest_streak = igem_user.user.longest_streak
#         current_streak = igem_user.user.current_streak
#
#         nb_phones = igem_user.user.phones.count
#         if nb_phones > 0
#           os = igem_user.user.phones[0].os
#         end
#       end
#
#       csv << [igem_user.id, igem_user.registered, igem_user.team.name, igem_user.team.country, longest_streak, current_streak, nb_phones, os]
#     end
#   end
# end
#
# puts csv
#run command: cat extract_data.rb | sudo heroku run --no-tty -a coso-backend rails runner - > users_data.csv


logs_headers = ["task_name", "username", "team_name", "task_with", "date_task"]

csv = CSV.generate do |csv|
  csv << logs_headers

  igem_users.each do |igem_user|
    unless igem_user.user.nil?
      user = Log.where(user_id: igem_user.user.id).order(:id)

      user.each do |log|
        log_user = LogUser.where(log_id: log.id).order(:user_id)
        task_with = log_user.map do |lu|
          lu.user.igem_user_id
        end

        csv << [log.task.name, log.user.igem_user.id, log.user.team.name, task_with.join(', '), log.task_done_at]
      end
    end
  end
end
#
puts csv
