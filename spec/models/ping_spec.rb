require 'rails_helper'

RSpec.describe Ping, type: :model do
  it 'should have valid factory' do
    expect(FactoryBot.build(:ping)).to be_valid
  end

  it 'should have a user' do
    ping = FactoryBot.create(:ping)
    expect(ping.user).to be_present
  end

  it 'should have a phone' do
    ping = FactoryBot.create(:ping)
    expect(ping.phone).to be_present
  end

  it 'should have a target' do
    ping = FactoryBot.create(:ping)
    expect(ping.target).to be_present
  end

  it 'should have a target phone' do
    ping = FactoryBot.create(:ping)
    expect(ping.target_phone).to be_present
  end

  it 'should have a rssi' do
    ping = FactoryBot.create(:ping)
    expect(ping.rssi).to be_present
  end

  it 'should have a start time' do
    ping = FactoryBot.create(:ping)
    expect(ping.ping_start_at).to be_present
  end

  it 'should have a end time' do
    ping = FactoryBot.create(:ping)
    expect(ping.ping_end_at).to be_present
  end
end
