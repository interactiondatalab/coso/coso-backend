require 'rails_helper'

RSpec.describe SurveyDatum, type: :model do
  it 'should have valid factory' do
    expect(FactoryBot.build(:survey_datum)).to be_valid
  end

  it 'should have a user' do
    survey_datum = FactoryBot.create(:survey_datum)
    expect(survey_datum.user).to be_present
  end

  it 'should have a survey' do
    survey_datum = FactoryBot.create(:survey_datum)
    expect(survey_datum.survey).to be_present
  end

  it 'should have a survey field' do
    survey_datum = FactoryBot.create(:survey_datum)
    expect(survey_datum.survey_field).to be_present
  end

  it 'should have a content' do
    survey_datum = FactoryBot.create(:survey_datum)
    expect(survey_datum.content).to be_present
  end
end
