require 'rails_helper'

RSpec.describe LogUser, type: :model do
  it 'should have valid factory' do
    expect(FactoryBot.build(:log_user)).to be_valid
  end

  it 'should have has a user' do
    log_user = FactoryBot.build(:log_user)
    expect(log_user.user).to be_present
  end

  it 'should have has a log' do
    log_user = FactoryBot.build(:log_user)
    expect(log_user.log).to be_present
  end

end
