require 'rails_helper'

RSpec.describe SurveyField, type: :model do
  it 'should have valid factory' do
    expect(FactoryBot.build(:survey_field)).to be_valid
  end

  it 'should have a survey' do
    survey_field = FactoryBot.create(:survey_field)
    expect(survey_field.survey).to be_present
  end

  it 'should be valid as radio_list' do
    expect(FactoryBot.build(:radio_list)).to be_valid
  end

  it 'should be valid as checkbox_list' do
    expect(FactoryBot.build(:checkbox_list)).to be_valid
  end

  it 'should be valid as radio_button' do
    expect(FactoryBot.build(:radio_button)).to be_valid
  end

  it 'should be valid as textfield' do
    expect(FactoryBot.build(:textfield)).to be_valid
  end

  it 'should be valid as select' do
    expect(FactoryBot.build(:select)).to be_valid
  end

end
