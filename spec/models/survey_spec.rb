require 'rails_helper'

RSpec.describe Survey, type: :model do
  it 'should have valid factory' do
    expect(FactoryBot.build(:survey)).to be_valid
  end

  it 'should have a team' do
    survey = FactoryBot.create(:survey)
    expect(survey.team).to be_present
  end
end
