require 'rails_helper'

RSpec.describe Phone, type: :model do
  it 'should have valid factory' do
    expect(FactoryBot.build(:phone)).to be_valid
  end

  it 'should have a user' do
    phone = FactoryBot.create(:phone)
    expect(phone.user).to be_present
  end
end
