require 'rails_helper'

RSpec.describe Log, type: :model do
  it 'should have valid factory' do
    expect(FactoryBot.build(:log)).to be_valid
  end

  it 'should have a user' do
    log = FactoryBot.create(:log)
    expect(log.user).to be_present
  end

  it 'should have participants' do
    log = FactoryBot.create(:log)
    expect(log.users.count).to_not eq 0
  end

  it 'should have a task' do
    log = FactoryBot.create(:log)
    expect(log.task).to be_present
  end

  it 'should have participants from other teams' do
    log = FactoryBot.create(:log_with_other_team)
    expect(log.users.where.not(team_id: log.user.team_id).count).to_not eq 0
  end
end
