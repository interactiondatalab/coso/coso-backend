require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'validation' do
    it 'should have valid factory' do
      expect(FactoryBot.build(:user)).to be_valid
    end

    it 'should require email' do
      user = FactoryBot.build(:user, email: '')
      expect(user).not_to be_valid
    end

    it 'should require username' do
      user = FactoryBot.build(:user, username: '')
      expect(user).not_to be_valid
    end
  end
end
