require 'rails_helper'

RSpec.describe Team, type: :model do
  it 'should have valid factory' do
    expect(FactoryBot.build(:team)).to be_valid
  end

  it 'should have team members' do
    team = FactoryBot.create(:team)
    expect(team.igem_users.count).to_not eq 0
  end

  it 'should require year' do
    team = FactoryBot.build(:team, year: nil)
    expect(team).not_to be_valid
  end

  it 'should require teamname' do
    team = FactoryBot.build(:team, name: '')
    expect(team).not_to be_valid
  end
end
