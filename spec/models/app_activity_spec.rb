require 'rails_helper'

RSpec.describe AppActivity, type: :model do
  it 'should have valid factory' do
    expect(FactoryBot.build(:app_activity)).to be_valid
  end

  it 'should have a user' do
    app_activity = FactoryBot.create(:app_activity)
    expect(app_activity.user).to be_present
  end

  it 'should have a phone' do
    app_activity = FactoryBot.create(:app_activity)
    expect(app_activity.phone).to be_present
  end

  it 'should have a start_at' do
    app_activity = FactoryBot.create(:app_activity)
    expect(app_activity.start_at).to be_present
  end


end
