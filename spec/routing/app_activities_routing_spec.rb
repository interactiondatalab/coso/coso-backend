require "rails_helper"

RSpec.describe AppActivitiesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/api/app_activities").to route_to("app_activities#index")
    end

    it "routes to #show" do
      expect(:get => "/api/app_activities/1").to route_to("app_activities#show", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/api/app_activities").to route_to("app_activities#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/api/app_activities/1").to route_to("app_activities#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/api/app_activities/1").to route_to("app_activities#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/api/app_activities/1").to route_to("app_activities#destroy", :id => "1")
    end
  end
end
