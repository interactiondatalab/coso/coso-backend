require "rails_helper"

RSpec.describe SurveysController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/api/surveys").to route_to("surveys#index")
    end

    it "routes to #show" do
      expect(:get => "/api/surveys/1").to route_to("surveys#show", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/api/surveys").to route_to("surveys#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/api/surveys/1").to route_to("surveys#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/api/surveys/1").to route_to("surveys#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/api/surveys/1").to route_to("surveys#destroy", :id => "1")
    end
  end
end
