require "rails_helper"

RSpec.describe PingsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/api/pings").to route_to("pings#index")
    end

    it "routes to #show" do
      expect(:get => "/api/pings/1").to route_to("pings#show", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/api/pings").to route_to("pings#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/api/pings/1").to route_to("pings#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/api/pings/1").to route_to("pings#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/api/pings/1").to route_to("pings#destroy", :id => "1")
    end
  end
end
