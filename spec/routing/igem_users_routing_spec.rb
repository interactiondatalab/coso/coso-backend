require "rails_helper"

RSpec.describe IgemUsersController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/igem_users").to route_to("igem_users#index")
    end

    it "routes to #show" do
      expect(:get => "/igem_users/1").to route_to("igem_users#show", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/igem_users").to route_to("igem_users#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/igem_users/1").to route_to("igem_users#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/igem_users/1").to route_to("igem_users#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/igem_users/1").to route_to("igem_users#destroy", :id => "1")
    end
  end
end
