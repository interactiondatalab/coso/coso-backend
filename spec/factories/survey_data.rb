FactoryBot.define do
  factory :survey_datum do
    user
    survey_field
    survey
    content { {"some": "content"} }
  end
end
