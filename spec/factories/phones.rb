FactoryBot.define do
  factory :phone, aliases: [:target_phone] do
    user
    imei { FFaker::PhoneNumber.imei }
    os { FFaker::Product.model }
    brand { FFaker::Product.brand }
    model { FFaker::Product.model }
  end

  factory :phone_invalid, parent: :phone do
    imei { "" }
  end
end
