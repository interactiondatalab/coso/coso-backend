FactoryBot.define do
  factory :user, aliases: [:target] do
    team
    password { FFaker::Internet.password }
    email { FFaker::Internet.unique.free_email }
    username { FFaker::Internet.unique.user_name }
  end

  factory :user_invalid, parent: :user do
    username { "" }
  end
end
