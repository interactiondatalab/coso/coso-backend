FactoryBot.define do
  factory :survey do
    team
    name { FFaker::Movie.title}
    description { FFaker::Lorem.sentence }
    time { '5 minutes' }
  end

  factory :survey_invalid, parent: :survey do
    name { "" }
  end
end
