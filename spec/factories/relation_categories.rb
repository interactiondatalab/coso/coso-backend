FactoryBot.define do
  factory :relation_category do
    name { FFaker::Job.title }
  end
end
