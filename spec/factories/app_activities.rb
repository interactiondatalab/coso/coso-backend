FactoryBot.define do
  factory :app_activity do
    user
    phone
    start_at { DateTime.now }
    end_at { DateTime.now }
  end

  factory :app_activity_invalid, parent: :app_activity do
    start_at { nil }
  end
end
