FactoryBot.define do
  factory :task do
    category { FFaker::Job.title }
    name { FFaker::Job.title }
    description {  FFaker::Lorem.sentence }
  end

  factory :task_invalid, parent: :task do
    category { "" }
  end
end
