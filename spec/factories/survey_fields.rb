FactoryBot.define do
  factory :survey_field do
    survey
    category { FFaker::Job.title }
    name { FFaker::Name.last_name }
    required { false }
    content { {"content": "is key"} }
  end

  factory :radio_list, parent: :survey_field do
    category { "inputRadioList" }
    content { {
        "options": [
            {
                "pos": 1,
                "label": "Someone",
                "value": "someone_id"
            },
            {
                "pos": 2,
                "label": "Someone else",
                "value": "someone_else_id"
            }
        ]} }
  end

  factory :checkbox_list, parent: :survey_field do
    category { "inputCheckbox" }
    content { {
        "options": [
            {
                "pos": 1,
                "label": "Someone",
                "value": "someone_id"
            },
            {
                "pos": 2,
                "label": "Someone else",
                "value": "someone_else_id"
            }
        ]} }
  end

  factory :radio_button, parent: :survey_field do
    category { "inputRadioButton" }
    content { {
        "options": [
            {
                "pos": 1,
                "label": "Yes",
                "value": true
            },
            {
                "pos": 2,
                "label": "No",
                "value": false
            }
        ]} }
  end

  factory :slider, parent: :survey_field do
    category { "inputSlider" }
    content { {
        "min": 0,
        "max": 10,
        "step": 1
        } }
  end

  factory :date, parent: :survey_field do
    category { "inputDate" }
    content { { "no_options": true } }
  end

  factory :textfield, parent: :survey_field do
    category { "inputTextfield" }
    content { {
        "placeholder": "Some text field",
        "value": "somevalue"
         } }
  end

  factory :inputfield, parent: :survey_field do
    category { "inputField" }
    content { {
        "placeholder": "Some text field",
        "value": "somevalue"
         } }
  end

  factory :select, parent: :survey_field do
    category { "inputSelect" }
    content { {
        "placeholder": "Some text field",
        "value": "somevalue",
        "options": [
            {
                "pos": 1,
                "label": "Team1",
                "value": "team1_id"
            },
            {
                "pos": 2,
                "label": "Team1",
                "value": "team2_id"
            }
        ]
         } }
  end

  factory :select_user, parent: :survey_field do
    category { "inputSelectUser" }
    content { {
        "placeholder": "Some text field",
        "value": "somevalue",
        "options": [
            {
                "pos": 1,
                "label": "Team1",
                "value": "team1_id"
            },
            {
                "pos": 2,
                "label": "Team1",
                "value": "team2_id"
            }
        ]
         } }
  end

end
