FactoryBot.define do
  factory :igem_user do
    team
    username { FFaker::Internet.user_name }
  end
end
