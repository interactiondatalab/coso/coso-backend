FactoryBot.define do
  factory :team do
    name { FFaker::Internet.unique.user_name }
    year { 2020 }
    url { FFaker::Internet.http_url }
    country { FFaker::Address.country }
    #igem_team_id { rand(1...545) }

    # after :create do |team|
    #   igem_users = FactoryBot.create_list(:igem_user, 10, team: team)
    #
    #   igem_users.each do |igem_user|
    #     user = FactoryBot.create(:user, team: team)
    #     # user.update(igem_user: igem_user)
    #     # user.update(igem_user_id: igem_user.id)
    #
    #     team.igem_users << igem_user
    #     team.users << user
    #
    #     role = rand(1...5)
    #     if role == 1
    #       user.add_role :student, team
    #     elsif role == 2
    #       user.add_role :pi, team
    #     elsif role == 3
    #       user.add_role :student_leader, team
    #     elsif role == 4
    #       user.add_role :advisor, team
    #     elsif role == 5
    #       user.add_role :instructor, team
    #     end
    #   end
    #end
  end

  factory :team_invalid, parent: :team do
    name { "" }
  end
end
