FactoryBot.define do
  factory :log do
    user
    team
    task
    task_done_at { DateTime.now }

    after :create do |log|
      log.team.users.limit(5).each do |user|
        log.users << user
      end
    end
  end

  factory :log_invalid, parent: :log do
    task_done_at { nil }
  end

  factory :log_with_other_team, parent: :log do
    after :create do |log|
      team2 = FactoryBot.create(:team)
      team2.users.limit(3).each do |user|
        log.users << user
      end
    end
  end
end
