FactoryBot.define do
  factory :ping do
    user
    phone
    target
    target_phone
    rssi { -rand(0...200) }
    ping_start_at { DateTime.now }
    ping_end_at { DateTime.now }
  end

  factory :ping_invalid, parent: :ping do
    rssi { nil }
  end
end
