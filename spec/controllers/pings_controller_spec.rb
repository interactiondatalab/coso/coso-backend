require 'rails_helper'
require 'devise/jwt/test_helpers'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.
#
# Also compared to earlier versions of this generator, there are no longer any
# expectations of assigns and templates rendered. These features have been
# removed from Rails core in Rails 5, but can be added back in via the
# `rails-controller-testing` gem.

RSpec.describe PingsController, type: :controller do

  # This should return the minimal set of attributes required to create a valid
  # Ping. As you add validations to Ping, be sure to
  # adjust the attributes here as well.
  before :all do
    @phone_source = FactoryBot.create(:phone)
    @phone_target = FactoryBot.create(:phone)
    @user = FactoryBot.create(:user)
    @ping = FactoryBot.build(:ping, user: @phone_source.user, phone: @phone_source, target_phone: @phone_target)
  end

  let(:auth_headers) do
    headers = { 'Accept' => 'application/json', 'Content-Type' => 'application/json' }
    Devise::JWT::TestHelpers.auth_headers(headers, @user)
  end

  before :each do
    request.headers.merge! auth_headers
  end

  let(:valid_attributes) {
    @ping.attributes
  }

  let(:invalid_attributes) {
    FactoryBot.attributes_for(:ping_invalid)
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # PingsController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET #index" do
    it "returns a success response" do
      ping = Ping.create! valid_attributes
      get :index, params: {}
      expect(response).to be_successful
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      ping = Ping.create! valid_attributes
      get :show, params: {id: ping.to_param}
      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Ping" do
        expect {
          post :create, params: {ping: valid_attributes}
        }.to change(Ping, :count).by(1)
      end

      it "renders a JSON response with the new ping" do

        post :create, params: {ping: valid_attributes}
        expect(response).to have_http_status(:created)
        expect(response.content_type).to eq('application/json; charset=utf-8')
        expect(response.location).to eq(ping_url(Ping.last))
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the new ping" do

        post :create, params: {ping: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json; charset=utf-8')
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      before :all do
        @ping = FactoryBot.build(:ping, user: @phone_source.user, phone: @phone_source, target_phone: @phone_target)
      end

      let(:new_attributes) {
        @ping.attributes
      }

      it "updates the requested ping" do
        ping = Ping.create! valid_attributes
        put :update, params: {id: ping.to_param, ping: new_attributes}
        ping.reload
        expect(ping.phone_id).to eq @ping.phone_id
      end

      it "renders a JSON response with the ping" do
        ping = Ping.create! valid_attributes

        put :update, params: {id: ping.to_param, ping: valid_attributes}
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to eq('application/json; charset=utf-8')
      end
    end

    context "with invalid params" do
      it "renders a JSON response with errors for the ping" do
        ping = Ping.create! valid_attributes

        put :update, params: {id: ping.to_param, ping: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json; charset=utf-8')
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested ping" do
      ping = Ping.create! valid_attributes
      expect {
        delete :destroy, params: {id: ping.to_param}
      }.to change(Ping, :count).by(-1)
    end
  end

end
