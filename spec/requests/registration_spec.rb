require 'rails_helper'

RSpec.describe 'POST /api/signup', type: :request do
  before do
    @user = FactoryBot.create(:user)
  end

  let(:url) { '/api/signup' }
  let(:params) do
    {
      user: {
        username: "Iamregistered",
        igem_username: @user.igem_username,
        email: 'user@example.com',
        password: 'password'
      }
    }
  end

  context 'when user is unauthenticated' do
    before { post url, params: params }

    it 'returns 201' do
      expect(response.status).to eq 201
    end

    it 'returns a new user' do
      json_response = JSON.parse(response.body)
      expect(json_response['username']).to match(params[:user][:username])
    end
  end

  context 'when user already exists' do
    let(:bad_params) do
      {
        user: {
          username: @user.username,
          igem_username: @user.igem_username,
          email: @user.email,
          password: 'password'
        }
      }
    end

    before do
      @user = FactoryBot.create(:user)
      @user.username = bad_params[:user][:username]
      @user.igem_username = bad_params[:user][:igem_username]
      @user.email = bad_params[:user][:email]
      @user.registered = true
      @user.save
      post url, params: bad_params
    end

    it 'returns bad request status' do
      expect(response.status).to eq 400
    end

    it 'returns validation errors' do
      json_response = JSON.parse(response.body)
      expect(json_response['error']).to eq('User already registered !')
    end
  end
end
