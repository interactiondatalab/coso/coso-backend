require 'rails_helper'
require 'devise/jwt/test_helpers'

RSpec.describe "Pings", type: :request do

  before do
    @user = FactoryBot.create(:user)
  end

  let (:auth_headers) do
    headers = { 'Accept' => 'application/json', 'Content-Type' => 'application/json' }
    Devise::JWT::TestHelpers.auth_headers(headers, @user)
  end

  describe "GET /pings" do
    it "works! (now write some real specs)" do
      get pings_path, headers: auth_headers
      expect(response).to have_http_status(200)
    end
  end
end
