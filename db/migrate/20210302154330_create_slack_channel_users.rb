class CreateSlackChannelUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :slack_channel_users do |t|
      t.belongs_to :slack_user
      t.belongs_to :slack_channel
      t.belongs_to :team
      t.timestamps
    end
  end
end
