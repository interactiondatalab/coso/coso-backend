class CreateLogRelationCategories < ActiveRecord::Migration[6.0]
  def change
    create_table :log_relation_categories do |t|
      t.bigint :log_id, null: false
      t.bigint :relation_category_id, null: false
      t.timestamps
    end
  end
end
