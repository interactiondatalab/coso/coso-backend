class CreateSlackUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :slack_users do |t|
      t.belongs_to :team
      t.belongs_to :user, optional: true
      t.string :slack_user_id
      t.string :email
      t.string :name
      t.string :real_name
      t.string :display_name
      t.datetime :updated
      t.timestamps
    end
  end
end
