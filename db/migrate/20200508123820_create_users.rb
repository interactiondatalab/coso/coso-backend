class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.bigint :igem_user_id
      t.bigint :team_id
      t.string :username
      t.boolean :verified
      t.timestamps
    end
  end
end
