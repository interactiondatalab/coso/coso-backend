class CreateSlackUsergroupMembers < ActiveRecord::Migration[6.0]
  def change
    create_table :slack_usergroup_members do |t|
      t.belongs_to :team
      t.belongs_to :slack_usergroup
      t.belongs_to :slack_user
      t.timestamps
    end
  end
end
