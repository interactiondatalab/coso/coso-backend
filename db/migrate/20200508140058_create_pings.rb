class CreatePings < ActiveRecord::Migration[6.0]
  def change
    create_table :pings do |t|
      t.bigint :user_id, null: false
      t.bigint :phone_id, null: false
      t.bigint :target_id, null: false
      t.bigint :target_phone_id, null: false
      t.integer :rssi, null: false
      t.datetime :ping_at, null: false
      t.timestamps
    end
  end
end
