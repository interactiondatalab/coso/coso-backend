class CreateIgemPages < ActiveRecord::Migration[6.0]
  def change
    create_table :igem_pages do |t|
      t.references :team
      t.string :url
      t.string :name
      t.timestamps
    end
  end
end
