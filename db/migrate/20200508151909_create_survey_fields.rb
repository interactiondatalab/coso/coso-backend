class CreateSurveyFields < ActiveRecord::Migration[6.0]
  def change
    create_table :survey_fields do |t|
      t.bigint :survey_id, null: false
      t.string :category, null: false
      t.string :name, null: false
      t.boolean :required, default: false
      t.jsonb :content, null: false
      t.timestamps
    end
  end
end
