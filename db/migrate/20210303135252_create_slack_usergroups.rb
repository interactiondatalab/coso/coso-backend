class CreateSlackUsergroups < ActiveRecord::Migration[6.0]
  def change
    create_table :slack_usergroups do |t|
      t.belongs_to :team
      t.datetime :created
      t.datetime :updated
      t.datetime :deleted
      t.string :description
      t.string :handle
      t.string :usergroup_id
      t.boolean :is_external
      t.boolean :is_subteam
      t.boolean :is_usergroup
      t.string :name
      t.bigint :user_count
      t.timestamps
    end
  end
end
