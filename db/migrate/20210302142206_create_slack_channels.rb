class CreateSlackChannels < ActiveRecord::Migration[6.0]
  def change
    create_table :slack_channels do |t|
      t.belongs_to :team
      t.datetime :created
      t.string :channel_id
      t.boolean :is_archived
      t.boolean :is_channel
      t.boolean :is_ext_shared
      t.boolean :is_general
      t.boolean :is_group
      t.boolean :is_im
      t.boolean :is_member
      t.boolean :is_mpim
      t.boolean :is_org_shared
      t.boolean :is_pending_ext_shared
      t.boolean :is_private
      t.boolean :is_shared
      t.string :name
      t.string :name_normalized
      t.bigint :num_members
      t.string :topic
      t.timestamps
    end
  end
end
