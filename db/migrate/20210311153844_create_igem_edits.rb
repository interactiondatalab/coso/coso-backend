class CreateIgemEdits < ActiveRecord::Migration[6.0]
  def change
    create_table :igem_edits do |t|
      t.references :team
      t.references :igem_user
      t.references :igem_page
      t.datetime :edit_time
      t.bigint :edit_size
      t.bigint :page_size
      t.timestamps
    end
  end
end
