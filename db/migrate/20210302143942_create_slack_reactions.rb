class CreateSlackReactions < ActiveRecord::Migration[6.0]
  def change
    create_table :slack_reactions do |t|
      t.string :name
      t.belongs_to :team
      t.belongs_to :slack_user
      t.belongs_to :slack_message
      t.timestamps
    end
  end
end
