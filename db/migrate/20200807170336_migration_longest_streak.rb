class MigrationLongestStreak < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :longest_streak, :bigint
    add_column :users, :current_streak, :bigint
  end
end
