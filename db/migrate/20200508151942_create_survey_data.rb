class CreateSurveyData < ActiveRecord::Migration[6.0]
  def change
    create_table :survey_data do |t|
      t.bigint :user_id, null: false
      t.bigint :survey_field_id, null: false
      t.bigint :survey_id, null: false
      t.jsonb :content, null: false
      t.timestamps
    end
  end
end
