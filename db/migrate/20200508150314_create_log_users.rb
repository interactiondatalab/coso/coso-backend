class CreateLogUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :log_users do |t|
      t.bigint :log_id, null: false
      t.bigint :user_id, null: false
      t.timestamps
    end
  end
end
