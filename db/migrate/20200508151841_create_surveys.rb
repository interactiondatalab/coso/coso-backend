class CreateSurveys < ActiveRecord::Migration[6.0]
  def change
    create_table :surveys do |t|
      t.bigint :team_id, null: false
      t.string :name, null: false
      t.string :description, null:false
      t.interval :time
      t.timestamps
    end
  end
end
