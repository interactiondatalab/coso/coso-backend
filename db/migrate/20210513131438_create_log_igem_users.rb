class CreateLogIgemUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :log_igem_users do |t|
      t.bigint :log_id, null: false
      t.bigint :igem_user_id, null: false
      t.timestamps
    end
  end
end
