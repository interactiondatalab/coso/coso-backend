class CreateIgemUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :igem_users do |t|
      t.bigint :user_id
      t.bigint :team_id
      t.string :username
      t.boolean :registered, default: false
      t.timestamps
    end
  end
end
