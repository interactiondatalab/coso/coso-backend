class CreateSlackMessages < ActiveRecord::Migration[6.0]
  def change
    create_table :slack_messages do |t|
      t.belongs_to :team
      t.belongs_to :slack_channel
      t.belongs_to :slack_user
      t.string :message_id
      t.string :text
      t.datetime :created
      t.string :ts
      t.timestamps
    end
  end
end
