class CreatePhones < ActiveRecord::Migration[6.0]
  def change
    create_table :phones do |t|
      t.bigint :user_id
      t.string :imei
      t.string :os
      t.string :brand
      t.string :model
      t.string :device_token
      t.boolean :active, default: false
      t.timestamps
    end
  end
end
