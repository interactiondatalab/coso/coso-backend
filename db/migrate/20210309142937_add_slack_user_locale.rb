class AddSlackUserLocale < ActiveRecord::Migration[6.0]
  def change
    add_column :slack_users, :tz, :string
    add_column :slack_users, :tz_label, :string
    add_column :slack_users, :tz_offset, :string
    add_column :slack_users, :is_bot, :boolean
    add_column :slack_users, :image_1024, :string
    add_column :slack_users, :image_192, :string
    add_column :slack_users, :image_48, :string
  end
end
