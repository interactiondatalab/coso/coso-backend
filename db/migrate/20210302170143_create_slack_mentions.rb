class CreateSlackMentions < ActiveRecord::Migration[6.0]
  def change
    create_table :slack_mentions do |t|
      t.bigint :source_id
      t.references :target, :polymorphic => true
      t.belongs_to :slack_message
      t.belongs_to :team
      t.timestamps
    end
  end
end
