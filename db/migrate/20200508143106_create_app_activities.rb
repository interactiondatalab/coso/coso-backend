class CreateAppActivities < ActiveRecord::Migration[6.0]
  def change
    create_table :app_activities do |t|
      t.bigint :user_id, null: false
      t.bigint :phone_id, null: false
      t.datetime :start_at, null: false
      t.datetime :end_at
      t.timestamps
    end
  end
end
