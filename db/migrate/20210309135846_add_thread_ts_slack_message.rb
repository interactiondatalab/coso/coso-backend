class AddThreadTsSlackMessage < ActiveRecord::Migration[6.0]
  def change
    add_column :slack_messages, :thread_ts, :string
    add_column :slack_messages, :parent_id, :bigint
  end
end
