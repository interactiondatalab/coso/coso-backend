class MigrationAddFullNameToIgemUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :igem_users, :full_name, :string
  end
end
