class AddIgemUserCustomStatus < ActiveRecord::Migration[6.0]
  def change
    add_column :igem_users, :custom, :boolean, default: false
  end
end
