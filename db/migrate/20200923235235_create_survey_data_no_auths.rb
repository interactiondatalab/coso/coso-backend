class CreateSurveyDataNoAuths < ActiveRecord::Migration[6.0]
  def change
    create_table :survey_data_no_auths do |t|
      t.bigint :igem_user_id
      t.bigint :survey_field_id
      t.bigint :survey_id
      t.jsonb :content

      t.timestamps
    end
  end
end
