class CreateTeams < ActiveRecord::Migration[6.0]
  def change
    create_table :teams do |t|
      t.string :name, null: false
      t.integer :year, null: false
      t.integer :igem_team_id
      t.string :country
      t.boolean :registered, default: false
      t.string :url
      t.index [:name, :year], name: 'name_year_uniqueness', unique: true
      t.timestamps
    end
  end
end
