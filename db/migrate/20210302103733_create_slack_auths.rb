class CreateSlackAuths < ActiveRecord::Migration[6.0]
  def change
    create_table :slack_auths do |t|
      t.string :token
      t.string :scope
      t.string :slack_user_id
      t.string :slack_team_id
      t.string :slack_team_name
      t.string :slack_enterprise_id
      t.belongs_to :team
      t.belongs_to :user
      t.timestamps
    end
  end
end
