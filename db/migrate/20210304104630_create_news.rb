class CreateNews < ActiveRecord::Migration[6.0]
  def change
    create_table :news do |t|
      t.belongs_to :team, optional: true
      t.string :content
      t.string :image
      t.string :title
      t.string :action_text
      t.string :action_url
      t.timestamps
    end
  end
end
