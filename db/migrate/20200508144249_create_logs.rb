class CreateLogs < ActiveRecord::Migration[6.0]
  def change
    create_table :logs do |t|
      t.bigint :user_id, null: false
      t.bigint :task_id, null: false
      t.bigint :team_id, null: false
      t.datetime :task_done_at, null: false
      t.timestamps
    end
  end
end
