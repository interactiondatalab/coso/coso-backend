class AddTeamRelationCategory < ActiveRecord::Migration[6.1]
  def change
    add_column :relation_categories, :team_id, :bigint
  end
end
