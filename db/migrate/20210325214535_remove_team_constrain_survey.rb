class RemoveTeamConstrainSurvey < ActiveRecord::Migration[6.1]
  def change
    change_column :surveys, :team_id, :bigint, :null => true
  end
end
