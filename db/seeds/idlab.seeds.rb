team = Team.find_by(igem_team_id: 301)
if team.nil?
  team = Team.create(name: 'IDLab', year: 2020, country: 'France', igem_team_id: 301)
else
  team.update(name: 'IDLab', year: 2020, country: 'France', igem_team_id: 301)
end
if 'Accepted' == 'Accepted'
  team.registered = true
else
  team.registered = false
end
team.save
