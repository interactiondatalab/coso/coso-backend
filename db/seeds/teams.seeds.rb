
    team = Team.find_by(igem_team_id: 3456)
    if team.nil?
      team = Team.create(name: 'Aachen', year: 2020, country: 'Germany', igem_team_id: 3456)
    else
      team.update(name: 'Aachen', year: 2020, country: 'Germany', igem_team_id: 3456)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3386)
    if team.nil?
      team = Team.create(name: 'Aalto-Helsinki', year: 2020, country: 'Finland', igem_team_id: 3386)
    else
      team.update(name: 'Aalto-Helsinki', year: 2020, country: 'Finland', igem_team_id: 3386)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3504)
    if team.nil?
      team = Team.create(name: 'AFCM-Egypt', year: 2020, country: 'Egypt', igem_team_id: 3504)
    else
      team.update(name: 'AFCM-Egypt', year: 2020, country: 'Egypt', igem_team_id: 3504)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3656)
    if team.nil?
      team = Team.create(name: 'AHUT-ZJU-China', year: 2020, country: 'China', igem_team_id: 3656)
    else
      team.update(name: 'AHUT-ZJU-China', year: 2020, country: 'China', igem_team_id: 3656)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3465)
    if team.nil?
      team = Team.create(name: 'Aix-Marseille', year: 2020, country: 'France', igem_team_id: 3465)
    else
      team.update(name: 'Aix-Marseille', year: 2020, country: 'France', igem_team_id: 3465)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3445)
    if team.nil?
      team = Team.create(name: 'Alma', year: 2020, country: 'United States', igem_team_id: 3445)
    else
      team.update(name: 'Alma', year: 2020, country: 'United States', igem_team_id: 3445)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3437)
    if team.nil?
      team = Team.create(name: 'Amsterdam', year: 2020, country: 'Netherlands', igem_team_id: 3437)
    else
      team.update(name: 'Amsterdam', year: 2020, country: 'Netherlands', igem_team_id: 3437)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3368)
    if team.nil?
      team = Team.create(name: 'AshesiGhana', year: 2020, country: 'Ghana', igem_team_id: 3368)
    else
      team.update(name: 'AshesiGhana', year: 2020, country: 'Ghana', igem_team_id: 3368)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3576)
    if team.nil?
      team = Team.create(name: 'ASTWS-China', year: 2020, country: 'China', igem_team_id: 3576)
    else
      team.update(name: 'ASTWS-China', year: 2020, country: 'China', igem_team_id: 3576)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3520)
    if team.nil?
      team = Team.create(name: 'Athens', year: 2020, country: 'Greece', igem_team_id: 3520)
    else
      team.update(name: 'Athens', year: 2020, country: 'Greece', igem_team_id: 3520)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3626)
    if team.nil?
      team = Team.create(name: 'AUC-EGYPT', year: 2020, country: 'Egypt', igem_team_id: 3626)
    else
      team.update(name: 'AUC-EGYPT', year: 2020, country: 'Egypt', igem_team_id: 3626)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3569)
    if team.nil?
      team = Team.create(name: 'Austin_UTexas', year: 2020, country: 'United States', igem_team_id: 3569)
    else
      team.update(name: 'Austin_UTexas', year: 2020, country: 'United States', igem_team_id: 3569)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3651)
    if team.nil?
      team = Team.create(name: 'Baltimore_BioCrew', year: 2020, country: 'United States', igem_team_id: 3651)
    else
      team.update(name: 'Baltimore_BioCrew', year: 2020, country: 'United States', igem_team_id: 3651)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3598)
    if team.nil?
      team = Team.create(name: 'Beijing_4ELEVEN', year: 2020, country: 'China', igem_team_id: 3598)
    else
      team.update(name: 'Beijing_4ELEVEN', year: 2020, country: 'China', igem_team_id: 3598)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3394)
    if team.nil?
      team = Team.create(name: 'BGU-Israel', year: 2020, country: 'Israel', igem_team_id: 3394)
    else
      team.update(name: 'BGU-Israel', year: 2020, country: 'Israel', igem_team_id: 3394)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3399)
    if team.nil?
      team = Team.create(name: 'BHSF', year: 2020, country: 'China', igem_team_id: 3399)
    else
      team.update(name: 'BHSF', year: 2020, country: 'China', igem_team_id: 3399)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3410)
    if team.nil?
      team = Team.create(name: 'Bielefeld-CeBiTec', year: 2020, country: 'Germany', igem_team_id: 3410)
    else
      team.update(name: 'Bielefeld-CeBiTec', year: 2020, country: 'Germany', igem_team_id: 3410)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3471)
    if team.nil?
      team = Team.create(name: 'BIT', year: 2020, country: 'China', igem_team_id: 3471)
    else
      team.update(name: 'BIT', year: 2020, country: 'China', igem_team_id: 3471)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3496)
    if team.nil?
      team = Team.create(name: 'BIT-China', year: 2020, country: 'China', igem_team_id: 3496)
    else
      team.update(name: 'BIT-China', year: 2020, country: 'China', igem_team_id: 3496)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3512)
    if team.nil?
      team = Team.create(name: 'BITSPilani-Goa_India', year: 2020, country: 'India', igem_team_id: 3512)
    else
      team.update(name: 'BITSPilani-Goa_India', year: 2020, country: 'India', igem_team_id: 3512)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3693)
    if team.nil?
      team = Team.create(name: 'BJ101HS', year: 2020, country: 'China', igem_team_id: 3693)
    else
      team.update(name: 'BJ101HS', year: 2020, country: 'China', igem_team_id: 3693)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3329)
    if team.nil?
      team = Team.create(name: 'BNDS_China', year: 2020, country: 'China', igem_team_id: 3329)
    else
      team.update(name: 'BNDS_China', year: 2020, country: 'China', igem_team_id: 3329)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3506)
    if team.nil?
      team = Team.create(name: 'BNU-China', year: 2020, country: 'China', igem_team_id: 3506)
    else
      team.update(name: 'BNU-China', year: 2020, country: 'China', igem_team_id: 3506)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3514)
    if team.nil?
      team = Team.create(name: 'BOKU-Vienna', year: 2020, country: 'Austria', igem_team_id: 3514)
    else
      team.update(name: 'BOKU-Vienna', year: 2020, country: 'Austria', igem_team_id: 3514)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3364)
    if team.nil?
      team = Team.create(name: 'Botchan_Lab_Tokyo', year: 2020, country: 'Japan', igem_team_id: 3364)
    else
      team.update(name: 'Botchan_Lab_Tokyo', year: 2020, country: 'Japan', igem_team_id: 3364)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3590)
    if team.nil?
      team = Team.create(name: 'Brno_Czech_Republic', year: 2020, country: 'Czech Republic', igem_team_id: 3590)
    else
      team.update(name: 'Brno_Czech_Republic', year: 2020, country: 'Czech Republic', igem_team_id: 3590)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3699)
    if team.nil?
      team = Team.create(name: 'BUCT', year: 2020, country: 'China', igem_team_id: 3699)
    else
      team.update(name: 'BUCT', year: 2020, country: 'China', igem_team_id: 3699)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3664)
    if team.nil?
      team = Team.create(name: 'BUCT-China', year: 2020, country: 'China', igem_team_id: 3664)
    else
      team.update(name: 'BUCT-China', year: 2020, country: 'China', igem_team_id: 3664)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3629)
    if team.nil?
      team = Team.create(name: 'Calgary', year: 2020, country: 'Canada', igem_team_id: 3629)
    else
      team.update(name: 'Calgary', year: 2020, country: 'Canada', igem_team_id: 3629)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3467)
    if team.nil?
      team = Team.create(name: 'CAU_China', year: 2020, country: 'China', igem_team_id: 3467)
    else
      team.update(name: 'CAU_China', year: 2020, country: 'China', igem_team_id: 3467)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3551)
    if team.nil?
      team = Team.create(name: 'CCA_San_Diego', year: 2020, country: 'United States', igem_team_id: 3551)
    else
      team.update(name: 'CCA_San_Diego', year: 2020, country: 'United States', igem_team_id: 3551)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3648)
    if team.nil?
      team = Team.create(name: 'CCU_Taiwan', year: 2020, country: 'Taiwan', igem_team_id: 3648)
    else
      team.update(name: 'CCU_Taiwan', year: 2020, country: 'Taiwan', igem_team_id: 3648)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3477)
    if team.nil?
      team = Team.create(name: 'Chalmers-Gothenburg', year: 2020, country: 'Sweden', igem_team_id: 3477)
    else
      team.update(name: 'Chalmers-Gothenburg', year: 2020, country: 'Sweden', igem_team_id: 3477)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3588)
    if team.nil?
      team = Team.create(name: 'CLS_CLSG_UK', year: 2020, country: 'United Kingdom', igem_team_id: 3588)
    else
      team.update(name: 'CLS_CLSG_UK', year: 2020, country: 'United Kingdom', igem_team_id: 3588)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3363)
    if team.nil?
      team = Team.create(name: 'Concordia-Montreal', year: 2020, country: 'Canada', igem_team_id: 3363)
    else
      team.update(name: 'Concordia-Montreal', year: 2020, country: 'Canada', igem_team_id: 3363)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3419)
    if team.nil?
      team = Team.create(name: 'Cornell', year: 2020, country: 'United States', igem_team_id: 3419)
    else
      team.update(name: 'Cornell', year: 2020, country: 'United States', igem_team_id: 3419)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3661)
    if team.nil?
      team = Team.create(name: 'CPU_CHINA', year: 2020, country: 'China', igem_team_id: 3661)
    else
      team.update(name: 'CPU_CHINA', year: 2020, country: 'China', igem_team_id: 3661)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3431)
    if team.nil?
      team = Team.create(name: 'CSMU_Taiwan', year: 2020, country: 'Taiwan', igem_team_id: 3431)
    else
      team.update(name: 'CSMU_Taiwan', year: 2020, country: 'Taiwan', igem_team_id: 3431)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3452)
    if team.nil?
      team = Team.create(name: 'CSU_CHINA', year: 2020, country: 'China', igem_team_id: 3452)
    else
      team.update(name: 'CSU_CHINA', year: 2020, country: 'China', igem_team_id: 3452)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3446)
    if team.nil?
      team = Team.create(name: 'CU-Boulder', year: 2020, country: 'United States', igem_team_id: 3446)
    else
      team.update(name: 'CU-Boulder', year: 2020, country: 'United States', igem_team_id: 3446)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3694)
    if team.nil?
      team = Team.create(name: 'DeNovocastrians', year: 2020, country: 'Australia', igem_team_id: 3694)
    else
      team.update(name: 'DeNovocastrians', year: 2020, country: 'Australia', igem_team_id: 3694)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3652)
    if team.nil?
      team = Team.create(name: 'DNHS_SanDiego_CA', year: 2020, country: 'United States', igem_team_id: 3652)
    else
      team.update(name: 'DNHS_SanDiego_CA', year: 2020, country: 'United States', igem_team_id: 3652)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3385)
    if team.nil?
      team = Team.create(name: 'DTU-Denmark', year: 2020, country: 'Denmark', igem_team_id: 3385)
    else
      team.update(name: 'DTU-Denmark', year: 2020, country: 'Denmark', igem_team_id: 3385)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3583)
    if team.nil?
      team = Team.create(name: 'Duesseldorf', year: 2020, country: 'Germany', igem_team_id: 3583)
    else
      team.update(name: 'Duesseldorf', year: 2020, country: 'Germany', igem_team_id: 3583)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3354)
    if team.nil?
      team = Team.create(name: 'DUT_China', year: 2020, country: 'China', igem_team_id: 3354)
    else
      team.update(name: 'DUT_China', year: 2020, country: 'China', igem_team_id: 3354)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3704)
    if team.nil?
      team = Team.create(name: 'ECNUAS', year: 2020, country: 'China', igem_team_id: 3704)
    else
      team.update(name: 'ECNUAS', year: 2020, country: 'China', igem_team_id: 3704)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3480)
    if team.nil?
      team = Team.create(name: 'ECUST_China', year: 2020, country: 'China', igem_team_id: 3480)
    else
      team.update(name: 'ECUST_China', year: 2020, country: 'China', igem_team_id: 3480)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3380)
    if team.nil?
      team = Team.create(name: 'Edinburgh', year: 2020, country: 'United Kingdom', igem_team_id: 3380)
    else
      team.update(name: 'Edinburgh', year: 2020, country: 'United Kingdom', igem_team_id: 3380)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3600)
    if team.nil?
      team = Team.create(name: 'EPFL', year: 2020, country: 'Switzerland', igem_team_id: 3600)
    else
      team.update(name: 'EPFL', year: 2020, country: 'Switzerland', igem_team_id: 3600)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3692)
    if team.nil?
      team = Team.create(name: 'Estonia_TUIT', year: 2020, country: 'Estonia', igem_team_id: 3692)
    else
      team.update(name: 'Estonia_TUIT', year: 2020, country: 'Estonia', igem_team_id: 3692)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3453)
    if team.nil?
      team = Team.create(name: 'Evry_Paris-Saclay', year: 2020, country: 'France', igem_team_id: 3453)
    else
      team.update(name: 'Evry_Paris-Saclay', year: 2020, country: 'France', igem_team_id: 3453)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3371)
    if team.nil?
      team = Team.create(name: 'Exeter', year: 2020, country: 'United Kingdom', igem_team_id: 3371)
    else
      team.update(name: 'Exeter', year: 2020, country: 'United Kingdom', igem_team_id: 3371)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3550)
    if team.nil?
      team = Team.create(name: 'FAFU-CHINA', year: 2020, country: 'China', igem_team_id: 3550)
    else
      team.update(name: 'FAFU-CHINA', year: 2020, country: 'China', igem_team_id: 3550)
    end
    if 'Pending' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3498)
    if team.nil?
      team = Team.create(name: 'FCB-UANL', year: 2020, country: 'Mexico', igem_team_id: 3498)
    else
      team.update(name: 'FCB-UANL', year: 2020, country: 'Mexico', igem_team_id: 3498)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3466)
    if team.nil?
      team = Team.create(name: 'FDR-HB_Peru', year: 2020, country: 'Peru', igem_team_id: 3466)
    else
      team.update(name: 'FDR-HB_Peru', year: 2020, country: 'Peru', igem_team_id: 3466)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3702)
    if team.nil?
      team = Team.create(name: 'FSU', year: 2020, country: 'United States', igem_team_id: 3702)
    else
      team.update(name: 'FSU', year: 2020, country: 'United States', igem_team_id: 3702)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3606)
    if team.nil?
      team = Team.create(name: 'Fudan', year: 2020, country: 'China', igem_team_id: 3606)
    else
      team.update(name: 'Fudan', year: 2020, country: 'China', igem_team_id: 3606)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3417)
    if team.nil?
      team = Team.create(name: 'Gaston_Day_School', year: 2020, country: 'United States', igem_team_id: 3417)
    else
      team.update(name: 'Gaston_Day_School', year: 2020, country: 'United States', igem_team_id: 3417)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3500)
    if team.nil?
      team = Team.create(name: 'GA_State_SW_Jiaotong', year: 2020, country: 'United States', igem_team_id: 3500)
    else
      team.update(name: 'GA_State_SW_Jiaotong', year: 2020, country: 'United States', igem_team_id: 3500)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3458)
    if team.nil?
      team = Team.create(name: 'GDSYZX', year: 2020, country: 'China', igem_team_id: 3458)
    else
      team.update(name: 'GDSYZX', year: 2020, country: 'China', igem_team_id: 3458)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3427)
    if team.nil?
      team = Team.create(name: 'GO_Paris-Saclay', year: 2020, country: 'France', igem_team_id: 3427)
    else
      team.update(name: 'GO_Paris-Saclay', year: 2020, country: 'France', igem_team_id: 3427)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3593)
    if team.nil?
      team = Team.create(name: 'GreatBay_SCIE', year: 2020, country: 'China', igem_team_id: 3593)
    else
      team.update(name: 'GreatBay_SCIE', year: 2020, country: 'China', igem_team_id: 3593)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3596)
    if team.nil?
      team = Team.create(name: 'GreatBay_SZ', year: 2020, country: 'China', igem_team_id: 3596)
    else
      team.update(name: 'GreatBay_SZ', year: 2020, country: 'China', igem_team_id: 3596)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3463)
    if team.nil?
      team = Team.create(name: 'Grenoble_Alpes', year: 2020, country: 'France', igem_team_id: 3463)
    else
      team.update(name: 'Grenoble_Alpes', year: 2020, country: 'France', igem_team_id: 3463)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3507)
    if team.nil?
      team = Team.create(name: 'Groningen', year: 2020, country: 'Netherlands', igem_team_id: 3507)
    else
      team.update(name: 'Groningen', year: 2020, country: 'Netherlands', igem_team_id: 3507)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3659)
    if team.nil?
      team = Team.create(name: 'GunnVistaPingry_US', year: 2020, country: 'United States', igem_team_id: 3659)
    else
      team.update(name: 'GunnVistaPingry_US', year: 2020, country: 'United States', igem_team_id: 3659)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3618)
    if team.nil?
      team = Team.create(name: 'GW_DC', year: 2020, country: 'United States', igem_team_id: 3618)
    else
      team.update(name: 'GW_DC', year: 2020, country: 'United States', igem_team_id: 3618)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3595)
    if team.nil?
      team = Team.create(name: 'GZ_HFI', year: 2020, country: 'China', igem_team_id: 3595)
    else
      team.update(name: 'GZ_HFI', year: 2020, country: 'China', igem_team_id: 3595)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3622)
    if team.nil?
      team = Team.create(name: 'Hainan_China', year: 2020, country: 'China', igem_team_id: 3622)
    else
      team.update(name: 'Hainan_China', year: 2020, country: 'China', igem_team_id: 3622)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3362)
    if team.nil?
      team = Team.create(name: 'Hamburg', year: 2020, country: 'Germany', igem_team_id: 3362)
    else
      team.update(name: 'Hamburg', year: 2020, country: 'Germany', igem_team_id: 3362)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3338)
    if team.nil?
      team = Team.create(name: 'Hannover', year: 2020, country: 'Germany', igem_team_id: 3338)
    else
      team.update(name: 'Hannover', year: 2020, country: 'Germany', igem_team_id: 3338)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3677)
    if team.nil?
      team = Team.create(name: 'Harvard', year: 2020, country: 'United States', igem_team_id: 3677)
    else
      team.update(name: 'Harvard', year: 2020, country: 'United States', igem_team_id: 3677)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3657)
    if team.nil?
      team = Team.create(name: 'Heidelberg', year: 2020, country: 'Germany', igem_team_id: 3657)
    else
      team.update(name: 'Heidelberg', year: 2020, country: 'Germany', igem_team_id: 3657)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3581)
    if team.nil?
      team = Team.create(name: 'HKUST', year: 2020, country: 'Hong Kong', igem_team_id: 3581)
    else
      team.update(name: 'HKUST', year: 2020, country: 'Hong Kong', igem_team_id: 3581)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3382)
    if team.nil?
      team = Team.create(name: 'HK_CPU-WFN-WYY', year: 2020, country: 'Hong Kong', igem_team_id: 3382)
    else
      team.update(name: 'HK_CPU-WFN-WYY', year: 2020, country: 'Hong Kong', igem_team_id: 3382)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3414)
    if team.nil?
      team = Team.create(name: 'HK_HCY', year: 2020, country: 'Hong Kong', igem_team_id: 3414)
    else
      team.update(name: 'HK_HCY', year: 2020, country: 'Hong Kong', igem_team_id: 3414)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3561)
    if team.nil?
      team = Team.create(name: 'HK_SSC', year: 2020, country: 'Hong Kong', igem_team_id: 3561)
    else
      team.update(name: 'HK_SSC', year: 2020, country: 'Hong Kong', igem_team_id: 3561)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3511)
    if team.nil?
      team = Team.create(name: 'Hong_Kong_CityU', year: 2020, country: 'Hong Kong', igem_team_id: 3511)
    else
      team.update(name: 'Hong_Kong_CityU', year: 2020, country: 'Hong Kong', igem_team_id: 3511)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3644)
    if team.nil?
      team = Team.create(name: 'Hong_Kong_HKU', year: 2020, country: 'Hong Kong', igem_team_id: 3644)
    else
      team.update(name: 'Hong_Kong_HKU', year: 2020, country: 'Hong Kong', igem_team_id: 3644)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3378)
    if team.nil?
      team = Team.create(name: 'HZAU-China', year: 2020, country: 'China', igem_team_id: 3378)
    else
      team.update(name: 'HZAU-China', year: 2020, country: 'China', igem_team_id: 3378)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3535)
    if team.nil?
      team = Team.create(name: 'HZNFHS_Hangzhou', year: 2020, country: 'China', igem_team_id: 3535)
    else
      team.update(name: 'HZNFHS_Hangzhou', year: 2020, country: 'China', igem_team_id: 3535)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3420)
    if team.nil?
      team = Team.create(name: 'iBowu-China', year: 2020, country: 'China', igem_team_id: 3420)
    else
      team.update(name: 'iBowu-China', year: 2020, country: 'China', igem_team_id: 3420)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3461)
    if team.nil?
      team = Team.create(name: 'ICS_BKK', year: 2020, country: 'Thailand', igem_team_id: 3461)
    else
      team.update(name: 'ICS_BKK', year: 2020, country: 'Thailand', igem_team_id: 3461)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3582)
    if team.nil?
      team = Team.create(name: 'IISER-Pune-India', year: 2020, country: 'India', igem_team_id: 3582)
    else
      team.update(name: 'IISER-Pune-India', year: 2020, country: 'India', igem_team_id: 3582)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3519)
    if team.nil?
      team = Team.create(name: 'IISER-Tirupati_India', year: 2020, country: 'India', igem_team_id: 3519)
    else
      team.update(name: 'IISER-Tirupati_India', year: 2020, country: 'India', igem_team_id: 3519)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3646)
    if team.nil?
      team = Team.create(name: 'IISER_Berhampur', year: 2020, country: 'India', igem_team_id: 3646)
    else
      team.update(name: 'IISER_Berhampur', year: 2020, country: 'India', igem_team_id: 3646)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3450)
    if team.nil?
      team = Team.create(name: 'IISER_Bhopal', year: 2020, country: 'India', igem_team_id: 3450)
    else
      team.update(name: 'IISER_Bhopal', year: 2020, country: 'India', igem_team_id: 3450)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3401)
    if team.nil?
      team = Team.create(name: 'IISER_Mohali', year: 2020, country: 'India', igem_team_id: 3401)
    else
      team.update(name: 'IISER_Mohali', year: 2020, country: 'India', igem_team_id: 3401)
    end
    if 'Withdrawn' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3564)
    if team.nil?
      team = Team.create(name: 'IIT_Roorkee', year: 2020, country: 'India', igem_team_id: 3564)
    else
      team.update(name: 'IIT_Roorkee', year: 2020, country: 'India', igem_team_id: 3564)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3559)
    if team.nil?
      team = Team.create(name: 'Imperial_College', year: 2020, country: 'United Kingdom', igem_team_id: 3559)
    else
      team.update(name: 'Imperial_College', year: 2020, country: 'United Kingdom', igem_team_id: 3559)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3409)
    if team.nil?
      team = Team.create(name: 'Ionis_Paris', year: 2020, country: 'France', igem_team_id: 3409)
    else
      team.update(name: 'Ionis_Paris', year: 2020, country: 'France', igem_team_id: 3409)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3665)
    if team.nil?
      team = Team.create(name: 'JACOXH_China', year: 2020, country: 'China', igem_team_id: 3665)
    else
      team.update(name: 'JACOXH_China', year: 2020, country: 'China', igem_team_id: 3665)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3402)
    if team.nil?
      team = Team.create(name: 'Jiangnan_China', year: 2020, country: 'China', igem_team_id: 3402)
    else
      team.update(name: 'Jiangnan_China', year: 2020, country: 'China', igem_team_id: 3402)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3447)
    if team.nil?
      team = Team.create(name: 'Jilin_China', year: 2020, country: 'China', igem_team_id: 3447)
    else
      team.update(name: 'Jilin_China', year: 2020, country: 'China', igem_team_id: 3447)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3605)
    if team.nil?
      team = Team.create(name: 'JNFLS', year: 2020, country: 'China', igem_team_id: 3605)
    else
      team.update(name: 'JNFLS', year: 2020, country: 'China', igem_team_id: 3605)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3685)
    if team.nil?
      team = Team.create(name: 'KAIT_Japan', year: 2020, country: 'Japan', igem_team_id: 3685)
    else
      team.update(name: 'KAIT_Japan', year: 2020, country: 'Japan', igem_team_id: 3685)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3635)
    if team.nil?
      team = Team.create(name: 'KCL_UK', year: 2020, country: 'United Kingdom', igem_team_id: 3635)
    else
      team.update(name: 'KCL_UK', year: 2020, country: 'United Kingdom', igem_team_id: 3635)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3478)
    if team.nil?
      team = Team.create(name: 'KEYSTONE', year: 2020, country: 'China', igem_team_id: 3478)
    else
      team.update(name: 'KEYSTONE', year: 2020, country: 'China', igem_team_id: 3478)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3592)
    if team.nil?
      team = Team.create(name: 'KEYSTONE_A', year: 2020, country: 'China', igem_team_id: 3592)
    else
      team.update(name: 'KEYSTONE_A', year: 2020, country: 'China', igem_team_id: 3592)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3643)
    if team.nil?
      team = Team.create(name: 'Korea-SIS', year: 2020, country: 'Korea, Republic Of', igem_team_id: 3643)
    else
      team.update(name: 'Korea-SIS', year: 2020, country: 'Korea, Republic Of', igem_team_id: 3643)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3573)
    if team.nil?
      team = Team.create(name: 'Korea_HS', year: 2020, country: 'Korea, Republic Of', igem_team_id: 3573)
    else
      team.update(name: 'Korea_HS', year: 2020, country: 'Korea, Republic Of', igem_team_id: 3573)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3654)
    if team.nil?
      team = Team.create(name: 'KSA_KOREA', year: 2020, country: 'Korea, Republic Of', igem_team_id: 3654)
    else
      team.update(name: 'KSA_KOREA', year: 2020, country: 'Korea, Republic Of', igem_team_id: 3654)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3421)
    if team.nil?
      team = Team.create(name: 'KUAS_Korea', year: 2020, country: 'Korea, Republic Of', igem_team_id: 3421)
    else
      team.update(name: 'KUAS_Korea', year: 2020, country: 'Korea, Republic Of', igem_team_id: 3421)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3433)
    if team.nil?
      team = Team.create(name: 'KU_ISTANBUL', year: 2020, country: 'Turkey', igem_team_id: 3433)
    else
      team.update(name: 'KU_ISTANBUL', year: 2020, country: 'Turkey', igem_team_id: 3433)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3411)
    if team.nil?
      team = Team.create(name: 'Lambert_GA', year: 2020, country: 'United States', igem_team_id: 3411)
    else
      team.update(name: 'Lambert_GA', year: 2020, country: 'United States', igem_team_id: 3411)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3343)
    if team.nil?
      team = Team.create(name: 'Leiden', year: 2020, country: 'Netherlands', igem_team_id: 3343)
    else
      team.update(name: 'Leiden', year: 2020, country: 'Netherlands', igem_team_id: 3343)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3624)
    if team.nil?
      team = Team.create(name: 'Lethbridge', year: 2020, country: 'Canada', igem_team_id: 3624)
    else
      team.update(name: 'Lethbridge', year: 2020, country: 'Canada', igem_team_id: 3624)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3349)
    if team.nil?
      team = Team.create(name: 'Lethbridge_HS', year: 2020, country: 'Canada', igem_team_id: 3349)
    else
      team.update(name: 'Lethbridge_HS', year: 2020, country: 'Canada', igem_team_id: 3349)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3647)
    if team.nil?
      team = Team.create(name: 'Linkoping', year: 2020, country: 'Sweden', igem_team_id: 3647)
    else
      team.update(name: 'Linkoping', year: 2020, country: 'Sweden', igem_team_id: 3647)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3552)
    if team.nil?
      team = Team.create(name: 'LINKS_China', year: 2020, country: 'China', igem_team_id: 3552)
    else
      team.update(name: 'LINKS_China', year: 2020, country: 'China', igem_team_id: 3552)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3668)
    if team.nil?
      team = Team.create(name: 'Lubbock_TTU', year: 2020, country: 'United States', igem_team_id: 3668)
    else
      team.update(name: 'Lubbock_TTU', year: 2020, country: 'United States', igem_team_id: 3668)
    end
    if 'Withdrawn' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3619)
    if team.nil?
      team = Team.create(name: 'Lund', year: 2020, country: 'Sweden', igem_team_id: 3619)
    else
      team.update(name: 'Lund', year: 2020, country: 'Sweden', igem_team_id: 3619)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3340)
    if team.nil?
      team = Team.create(name: 'Manchester', year: 2020, country: 'United Kingdom', igem_team_id: 3340)
    else
      team.update(name: 'Manchester', year: 2020, country: 'United Kingdom', igem_team_id: 3340)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3359)
    if team.nil?
      team = Team.create(name: 'MichiganState', year: 2020, country: 'United States', igem_team_id: 3359)
    else
      team.update(name: 'MichiganState', year: 2020, country: 'United States', igem_team_id: 3359)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3376)
    if team.nil?
      team = Team.create(name: 'Mingdao', year: 2020, country: 'Taiwan', igem_team_id: 3376)
    else
      team.update(name: 'Mingdao', year: 2020, country: 'Taiwan', igem_team_id: 3376)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3621)
    if team.nil?
      team = Team.create(name: 'MIT', year: 2020, country: 'United States', igem_team_id: 3621)
    else
      team.update(name: 'MIT', year: 2020, country: 'United States', igem_team_id: 3621)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3470)
    if team.nil?
      team = Team.create(name: 'MIT_MAHE', year: 2020, country: 'India', igem_team_id: 3470)
    else
      team.update(name: 'MIT_MAHE', year: 2020, country: 'India', igem_team_id: 3470)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3700)
    if team.nil?
      team = Team.create(name: 'Montpellier', year: 2020, country: 'France', igem_team_id: 3700)
    else
      team.update(name: 'Montpellier', year: 2020, country: 'France', igem_team_id: 3700)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3636)
    if team.nil?
      team = Team.create(name: 'Moscow', year: 2020, country: 'Russian Federation', igem_team_id: 3636)
    else
      team.update(name: 'Moscow', year: 2020, country: 'Russian Federation', igem_team_id: 3636)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3565)
    if team.nil?
      team = Team.create(name: 'Moscow-Russia', year: 2020, country: 'Russian Federation', igem_team_id: 3565)
    else
      team.update(name: 'Moscow-Russia', year: 2020, country: 'Russian Federation', igem_team_id: 3565)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3587)
    if team.nil?
      team = Team.create(name: 'MRIIRS_FARIDABAD', year: 2020, country: 'India', igem_team_id: 3587)
    else
      team.update(name: 'MRIIRS_FARIDABAD', year: 2020, country: 'India', igem_team_id: 3587)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3476)
    if team.nil?
      team = Team.create(name: 'MSP-Maastricht', year: 2020, country: 'Netherlands', igem_team_id: 3476)
    else
      team.update(name: 'MSP-Maastricht', year: 2020, country: 'Netherlands', igem_team_id: 3476)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3341)
    if team.nil?
      team = Team.create(name: 'Nanjing-China', year: 2020, country: 'China', igem_team_id: 3341)
    else
      team.update(name: 'Nanjing-China', year: 2020, country: 'China', igem_team_id: 3341)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3524)
    if team.nil?
      team = Team.create(name: 'Nanjing_high_school', year: 2020, country: 'China', igem_team_id: 3524)
    else
      team.update(name: 'Nanjing_high_school', year: 2020, country: 'China', igem_team_id: 3524)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3492)
    if team.nil?
      team = Team.create(name: 'Nanjing_NFLS', year: 2020, country: 'China', igem_team_id: 3492)
    else
      team.update(name: 'Nanjing_NFLS', year: 2020, country: 'China', igem_team_id: 3492)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3518)
    if team.nil?
      team = Team.create(name: 'Nantes', year: 2020, country: 'France', igem_team_id: 3518)
    else
      team.update(name: 'Nantes', year: 2020, country: 'France', igem_team_id: 3518)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3408)
    if team.nil?
      team = Team.create(name: 'NAU-CHINA', year: 2020, country: 'China', igem_team_id: 3408)
    else
      team.update(name: 'NAU-CHINA', year: 2020, country: 'China', igem_team_id: 3408)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3490)
    if team.nil?
      team = Team.create(name: 'NCKU_Tainan', year: 2020, country: 'Taiwan', igem_team_id: 3490)
    else
      team.update(name: 'NCKU_Tainan', year: 2020, country: 'Taiwan', igem_team_id: 3490)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3370)
    if team.nil?
      team = Team.create(name: 'NCTU_Formosa', year: 2020, country: 'Taiwan', igem_team_id: 3370)
    else
      team.update(name: 'NCTU_Formosa', year: 2020, country: 'Taiwan', igem_team_id: 3370)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3350)
    if team.nil?
      team = Team.create(name: 'NEFU_China', year: 2020, country: 'China', igem_team_id: 3350)
    else
      team.update(name: 'NEFU_China', year: 2020, country: 'China', igem_team_id: 3350)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3611)
    if team.nil?
      team = Team.create(name: 'NEU_CHINA', year: 2020, country: 'China', igem_team_id: 3611)
    else
      team.update(name: 'NEU_CHINA', year: 2020, country: 'China', igem_team_id: 3611)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3513)
    if team.nil?
      team = Team.create(name: 'NFLS', year: 2020, country: 'China', igem_team_id: 3513)
    else
      team.update(name: 'NFLS', year: 2020, country: 'China', igem_team_id: 3513)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3631)
    if team.nil?
      team = Team.create(name: 'NJMU-China', year: 2020, country: 'China', igem_team_id: 3631)
    else
      team.update(name: 'NJMU-China', year: 2020, country: 'China', igem_team_id: 3631)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3384)
    if team.nil?
      team = Team.create(name: 'NJTech_China', year: 2020, country: 'China', igem_team_id: 3384)
    else
      team.update(name: 'NJTech_China', year: 2020, country: 'China', igem_team_id: 3384)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3335)
    if team.nil?
      team = Team.create(name: 'NJU-China', year: 2020, country: 'China', igem_team_id: 3335)
    else
      team.update(name: 'NJU-China', year: 2020, country: 'China', igem_team_id: 3335)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3522)
    if team.nil?
      team = Team.create(name: 'NOFLS_YZ', year: 2020, country: 'China', igem_team_id: 3522)
    else
      team.update(name: 'NOFLS_YZ', year: 2020, country: 'China', igem_team_id: 3522)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3356)
    if team.nil?
      team = Team.create(name: 'Nottingham', year: 2020, country: 'United Kingdom', igem_team_id: 3356)
    else
      team.update(name: 'Nottingham', year: 2020, country: 'United Kingdom', igem_team_id: 3356)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3675)
    if team.nil?
      team = Team.create(name: 'NOVA_LxPortugal', year: 2020, country: 'Portugal', igem_team_id: 3675)
    else
      team.update(name: 'NOVA_LxPortugal', year: 2020, country: 'Portugal', igem_team_id: 3675)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3486)
    if team.nil?
      team = Team.create(name: 'NTHU_Taiwan', year: 2020, country: 'Taiwan', igem_team_id: 3486)
    else
      team.update(name: 'NTHU_Taiwan', year: 2020, country: 'Taiwan', igem_team_id: 3486)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3396)
    if team.nil?
      team = Team.create(name: 'NUDT_CHINA', year: 2020, country: 'China', igem_team_id: 3396)
    else
      team.update(name: 'NUDT_CHINA', year: 2020, country: 'China', igem_team_id: 3396)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3351)
    if team.nil?
      team = Team.create(name: 'NWU-CHINA-A', year: 2020, country: 'China', igem_team_id: 3351)
    else
      team.update(name: 'NWU-CHINA-A', year: 2020, country: 'China', igem_team_id: 3351)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3388)
    if team.nil?
      team = Team.create(name: 'NWU-CHINA-B', year: 2020, country: 'China', igem_team_id: 3388)
    else
      team.update(name: 'NWU-CHINA-B', year: 2020, country: 'China', igem_team_id: 3388)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3691)
    if team.nil?
      team = Team.create(name: 'NYC Earthians', year: 2020, country: 'United States', igem_team_id: 3691)
    else
      team.update(name: 'NYC Earthians', year: 2020, country: 'United States', igem_team_id: 3691)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3682)
    if team.nil?
      team = Team.create(name: 'NYMU-Taipei', year: 2020, country: 'Taiwan', igem_team_id: 3682)
    else
      team.update(name: 'NYMU-Taipei', year: 2020, country: 'Taiwan', igem_team_id: 3682)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3658)
    if team.nil?
      team = Team.create(name: 'NYU_Abu_Dhabi', year: 2020, country: 'United Arab Emirates', igem_team_id: 3658)
    else
      team.update(name: 'NYU_Abu_Dhabi', year: 2020, country: 'United Arab Emirates', igem_team_id: 3658)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3412)
    if team.nil?
      team = Team.create(name: 'OhioState', year: 2020, country: 'United States', igem_team_id: 3412)
    else
      team.update(name: 'OhioState', year: 2020, country: 'United States', igem_team_id: 3412)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3696)
    if team.nil?
      team = Team.create(name: 'OSA', year: 2020, country: 'China', igem_team_id: 3696)
    else
      team.update(name: 'OSA', year: 2020, country: 'China', igem_team_id: 3696)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3328)
    if team.nil?
      team = Team.create(name: 'OUC-China', year: 2020, country: 'China', igem_team_id: 3328)
    else
      team.update(name: 'OUC-China', year: 2020, country: 'China', igem_team_id: 3328)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3579)
    if team.nil?
      team = Team.create(name: 'Paris_Bettencourt', year: 2020, country: 'France', igem_team_id: 3579)
    else
      team.update(name: 'Paris_Bettencourt', year: 2020, country: 'France', igem_team_id: 3579)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3464)
    if team.nil?
      team = Team.create(name: 'Patras', year: 2020, country: 'Greece', igem_team_id: 3464)
    else
      team.update(name: 'Patras', year: 2020, country: 'Greece', igem_team_id: 3464)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3645)
    if team.nil?
      team = Team.create(name: 'Peking', year: 2020, country: 'China', igem_team_id: 3645)
    else
      team.update(name: 'Peking', year: 2020, country: 'China', igem_team_id: 3645)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3666)
    if team.nil?
      team = Team.create(name: 'Pittsburgh', year: 2020, country: 'United States', igem_team_id: 3666)
    else
      team.update(name: 'Pittsburgh', year: 2020, country: 'United States', igem_team_id: 3666)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3642)
    if team.nil?
      team = Team.create(name: 'PSGTECH_COIMBATORE', year: 2020, country: 'India', igem_team_id: 3642)
    else
      team.update(name: 'PSGTECH_COIMBATORE', year: 2020, country: 'India', igem_team_id: 3642)
    end
    if 'Withdrawn' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3503)
    if team.nil?
      team = Team.create(name: 'PuiChing_Macau', year: 2020, country: 'Macao', igem_team_id: 3503)
    else
      team.update(name: 'PuiChing_Macau', year: 2020, country: 'Macao', igem_team_id: 3503)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3439)
    if team.nil?
      team = Team.create(name: 'Purdue', year: 2020, country: 'United States', igem_team_id: 3439)
    else
      team.update(name: 'Purdue', year: 2020, country: 'United States', igem_team_id: 3439)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3683)
    if team.nil?
      team = Team.create(name: 'PYMS_GZ_China', year: 2020, country: 'China', igem_team_id: 3683)
    else
      team.update(name: 'PYMS_GZ_China', year: 2020, country: 'China', igem_team_id: 3683)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3430)
    if team.nil?
      team = Team.create(name: 'Qdai', year: 2020, country: 'Japan', igem_team_id: 3430)
    else
      team.update(name: 'Qdai', year: 2020, country: 'Japan', igem_team_id: 3430)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3457)
    if team.nil?
      team = Team.create(name: 'QHFZ-China', year: 2020, country: 'China', igem_team_id: 3457)
    else
      team.update(name: 'QHFZ-China', year: 2020, country: 'China', igem_team_id: 3457)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3515)
    if team.nil?
      team = Team.create(name: 'Queens_Canada', year: 2020, country: 'Canada', igem_team_id: 3515)
    else
      team.update(name: 'Queens_Canada', year: 2020, country: 'Canada', igem_team_id: 3515)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3395)
    if team.nil?
      team = Team.create(name: 'RDFZ-China', year: 2020, country: 'China', igem_team_id: 3395)
    else
      team.update(name: 'RDFZ-China', year: 2020, country: 'China', igem_team_id: 3395)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3366)
    if team.nil?
      team = Team.create(name: 'REC-CHENNAI', year: 2020, country: 'India', igem_team_id: 3366)
    else
      team.update(name: 'REC-CHENNAI', year: 2020, country: 'India', igem_team_id: 3366)
    end
    if 'Withdrawn' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3614)
    if team.nil?
      team = Team.create(name: 'ROADS_SY', year: 2020, country: 'China', igem_team_id: 3614)
    else
      team.update(name: 'ROADS_SY', year: 2020, country: 'China', igem_team_id: 3614)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3346)
    if team.nil?
      team = Team.create(name: 'Rochester', year: 2020, country: 'United States', igem_team_id: 3346)
    else
      team.update(name: 'Rochester', year: 2020, country: 'United States', igem_team_id: 3346)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3670)
    if team.nil?
      team = Team.create(name: 'RUM-UPRM', year: 2020, country: 'United States', igem_team_id: 3670)
    else
      team.update(name: 'RUM-UPRM', year: 2020, country: 'United States', igem_team_id: 3670)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3544)
    if team.nil?
      team = Team.create(name: 'SCU-China', year: 2020, country: 'China', igem_team_id: 3544)
    else
      team.update(name: 'SCU-China', year: 2020, country: 'China', igem_team_id: 3544)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3672)
    if team.nil?
      team = Team.create(name: 'SCU-WestChina', year: 2020, country: 'China', igem_team_id: 3672)
    else
      team.update(name: 'SCU-WestChina', year: 2020, country: 'China', igem_team_id: 3672)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3333)
    if team.nil?
      team = Team.create(name: 'SCUT_China', year: 2020, country: 'China', igem_team_id: 3333)
    else
      team.update(name: 'SCUT_China', year: 2020, country: 'China', igem_team_id: 3333)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3616)
    if team.nil?
      team = Team.create(name: 'SDSZ_China', year: 2020, country: 'China', igem_team_id: 3616)
    else
      team.update(name: 'SDSZ_China', year: 2020, country: 'China', igem_team_id: 3616)
    end
    if 'Withdrawn' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3602)
    if team.nil?
      team = Team.create(name: 'SDU-Denmark', year: 2020, country: 'Denmark', igem_team_id: 3602)
    else
      team.update(name: 'SDU-Denmark', year: 2020, country: 'Denmark', igem_team_id: 3602)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3684)
    if team.nil?
      team = Team.create(name: 'SEHS-China', year: 2020, country: 'China', igem_team_id: 3684)
    else
      team.update(name: 'SEHS-China', year: 2020, country: 'China', igem_team_id: 3684)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3454)
    if team.nil?
      team = Team.create(name: 'ShanghaiTech_China', year: 2020, country: 'China', igem_team_id: 3454)
    else
      team.update(name: 'ShanghaiTech_China', year: 2020, country: 'China', igem_team_id: 3454)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3526)
    if team.nil?
      team = Team.create(name: 'Shanghai_city', year: 2020, country: 'China', igem_team_id: 3526)
    else
      team.update(name: 'Shanghai_city', year: 2020, country: 'China', igem_team_id: 3526)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3525)
    if team.nil?
      team = Team.create(name: 'Shanghai_high_school', year: 2020, country: 'China', igem_team_id: 3525)
    else
      team.update(name: 'Shanghai_high_school', year: 2020, country: 'China', igem_team_id: 3525)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3527)
    if team.nil?
      team = Team.create(name: 'Shanghai_HS', year: 2020, country: 'China', igem_team_id: 3527)
    else
      team.update(name: 'Shanghai_HS', year: 2020, country: 'China', igem_team_id: 3527)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3585)
    if team.nil?
      team = Team.create(name: 'Shanghai_HS_United', year: 2020, country: 'China', igem_team_id: 3585)
    else
      team.update(name: 'Shanghai_HS_United', year: 2020, country: 'China', igem_team_id: 3585)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3633)
    if team.nil?
      team = Team.create(name: 'Shanghai_SFLS_SPBS', year: 2020, country: 'China', igem_team_id: 3633)
    else
      team.update(name: 'Shanghai_SFLS_SPBS', year: 2020, country: 'China', igem_team_id: 3633)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3584)
    if team.nil?
      team = Team.create(name: 'Shanghai_United', year: 2020, country: 'China', igem_team_id: 3584)
    else
      team.update(name: 'Shanghai_United', year: 2020, country: 'China', igem_team_id: 3584)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3594)
    if team.nil?
      team = Team.create(name: 'SHSBNU_China', year: 2020, country: 'China', igem_team_id: 3594)
    else
      team.update(name: 'SHSBNU_China', year: 2020, country: 'China', igem_team_id: 3594)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3521)
    if team.nil?
      team = Team.create(name: 'SHSID', year: 2020, country: 'China', igem_team_id: 3521)
    else
      team.update(name: 'SHSID', year: 2020, country: 'China', igem_team_id: 3521)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3365)
    if team.nil?
      team = Team.create(name: 'SJTU-BioX-Shanghai', year: 2020, country: 'China', igem_team_id: 3365)
    else
      team.update(name: 'SJTU-BioX-Shanghai', year: 2020, country: 'China', igem_team_id: 3365)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3460)
    if team.nil?
      team = Team.create(name: 'SJTU-software', year: 2020, country: 'China', igem_team_id: 3460)
    else
      team.update(name: 'SJTU-software', year: 2020, country: 'China', igem_team_id: 3460)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3497)
    if team.nil?
      team = Team.create(name: 'SMMU-China', year: 2020, country: 'China', igem_team_id: 3497)
    else
      team.update(name: 'SMMU-China', year: 2020, country: 'China', igem_team_id: 3497)
    end
    if 'Withdrawn' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3628)
    if team.nil?
      team = Team.create(name: 'SMS_Shenzhen', year: 2020, country: 'China', igem_team_id: 3628)
    else
      team.update(name: 'SMS_Shenzhen', year: 2020, country: 'China', igem_team_id: 3628)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3373)
    if team.nil?
      team = Team.create(name: 'Sorbonne_U_Paris', year: 2020, country: 'France', igem_team_id: 3373)
    else
      team.update(name: 'Sorbonne_U_Paris', year: 2020, country: 'France', igem_team_id: 3373)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3697)
    if team.nil?
      team = Team.create(name: 'Stanford', year: 2020, country: 'United States', igem_team_id: 3697)
    else
      team.update(name: 'Stanford', year: 2020, country: 'United States', igem_team_id: 3697)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3440)
    if team.nil?
      team = Team.create(name: 'Stockholm', year: 2020, country: 'Sweden', igem_team_id: 3440)
    else
      team.update(name: 'Stockholm', year: 2020, country: 'Sweden', igem_team_id: 3440)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3428)
    if team.nil?
      team = Team.create(name: 'Stony_Brook', year: 2020, country: 'United States', igem_team_id: 3428)
    else
      team.update(name: 'Stony_Brook', year: 2020, country: 'United States', igem_team_id: 3428)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3379)
    if team.nil?
      team = Team.create(name: 'Stuttgart', year: 2020, country: 'Germany', igem_team_id: 3379)
    else
      team.update(name: 'Stuttgart', year: 2020, country: 'Germany', igem_team_id: 3379)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3634)
    if team.nil?
      team = Team.create(name: 'St_Andrews', year: 2020, country: 'United States', igem_team_id: 3634)
    else
      team.update(name: 'St_Andrews', year: 2020, country: 'United States', igem_team_id: 3634)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3389)
    if team.nil?
      team = Team.create(name: 'SUNY_Oneonta', year: 2020, country: 'United States', igem_team_id: 3389)
    else
      team.update(name: 'SUNY_Oneonta', year: 2020, country: 'United States', igem_team_id: 3389)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3423)
    if team.nil?
      team = Team.create(name: 'SUSTech_Shenzhen', year: 2020, country: 'China', igem_team_id: 3423)
    else
      team.update(name: 'SUSTech_Shenzhen', year: 2020, country: 'China', igem_team_id: 3423)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3502)
    if team.nil?
      team = Team.create(name: 'SYSU-CHINA', year: 2020, country: 'China', igem_team_id: 3502)
    else
      team.update(name: 'SYSU-CHINA', year: 2020, country: 'China', igem_team_id: 3502)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3575)
    if team.nil?
      team = Team.create(name: 'SYSU-Software', year: 2020, country: 'China', igem_team_id: 3575)
    else
      team.update(name: 'SYSU-Software', year: 2020, country: 'China', igem_team_id: 3575)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3686)
    if team.nil?
      team = Team.create(name: 'SZ-SHD', year: 2020, country: 'China', igem_team_id: 3686)
    else
      team.update(name: 'SZ-SHD', year: 2020, country: 'China', igem_team_id: 3686)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3487)
    if team.nil?
      team = Team.create(name: 'SZPT-CHINA', year: 2020, country: 'China', igem_team_id: 3487)
    else
      team.update(name: 'SZPT-CHINA', year: 2020, country: 'China', igem_team_id: 3487)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3360)
    if team.nil?
      team = Team.create(name: 'SZU-China', year: 2020, country: 'China', igem_team_id: 3360)
    else
      team.update(name: 'SZU-China', year: 2020, country: 'China', igem_team_id: 3360)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3352)
    if team.nil?
      team = Team.create(name: 'TAS_Taipei', year: 2020, country: 'Taiwan', igem_team_id: 3352)
    else
      team.update(name: 'TAS_Taipei', year: 2020, country: 'Taiwan', igem_team_id: 3352)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3413)
    if team.nil?
      team = Team.create(name: 'TAU_Israel', year: 2020, country: 'Israel', igem_team_id: 3413)
    else
      team.update(name: 'TAU_Israel', year: 2020, country: 'Israel', igem_team_id: 3413)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3653)
    if team.nil?
      team = Team.create(name: 'Technion-Israel', year: 2020, country: 'Israel', igem_team_id: 3653)
    else
      team.update(name: 'Technion-Israel', year: 2020, country: 'Israel', igem_team_id: 3653)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3505)
    if team.nil?
      team = Team.create(name: 'Thessaly', year: 2020, country: 'Greece', igem_team_id: 3505)
    else
      team.update(name: 'Thessaly', year: 2020, country: 'Greece', igem_team_id: 3505)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3468)
    if team.nil?
      team = Team.create(name: 'TJUSLS_China', year: 2020, country: 'China', igem_team_id: 3468)
    else
      team.update(name: 'TJUSLS_China', year: 2020, country: 'China', igem_team_id: 3468)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3475)
    if team.nil?
      team = Team.create(name: 'Tongji_China', year: 2020, country: 'China', igem_team_id: 3475)
    else
      team.update(name: 'Tongji_China', year: 2020, country: 'China', igem_team_id: 3475)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3483)
    if team.nil?
      team = Team.create(name: 'Tongji_Software', year: 2020, country: 'China', igem_team_id: 3483)
    else
      team.update(name: 'Tongji_Software', year: 2020, country: 'China', igem_team_id: 3483)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3570)
    if team.nil?
      team = Team.create(name: 'Toulouse_INSA-UPS', year: 2020, country: 'France', igem_team_id: 3570)
    else
      team.update(name: 'Toulouse_INSA-UPS', year: 2020, country: 'France', igem_team_id: 3570)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3599)
    if team.nil?
      team = Team.create(name: 'TPR_China', year: 2020, country: 'China', igem_team_id: 3599)
    else
      team.update(name: 'TPR_China', year: 2020, country: 'China', igem_team_id: 3599)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3369)
    if team.nil?
      team = Team.create(name: 'Tsinghua', year: 2020, country: 'China', igem_team_id: 3369)
    else
      team.update(name: 'Tsinghua', year: 2020, country: 'China', igem_team_id: 3369)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3374)
    if team.nil?
      team = Team.create(name: 'Tsinghua-A', year: 2020, country: 'China', igem_team_id: 3374)
    else
      team.update(name: 'Tsinghua-A', year: 2020, country: 'China', igem_team_id: 3374)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3407)
    if team.nil?
      team = Team.create(name: 'TUDelft', year: 2020, country: 'Netherlands', igem_team_id: 3407)
    else
      team.update(name: 'TUDelft', year: 2020, country: 'Netherlands', igem_team_id: 3407)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3510)
    if team.nil?
      team = Team.create(name: 'Tuebingen', year: 2020, country: 'Germany', igem_team_id: 3510)
    else
      team.update(name: 'Tuebingen', year: 2020, country: 'Germany', igem_team_id: 3510)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3429)
    if team.nil?
      team = Team.create(name: 'TU_Darmstadt', year: 2020, country: 'Germany', igem_team_id: 3429)
    else
      team.update(name: 'TU_Darmstadt', year: 2020, country: 'Germany', igem_team_id: 3429)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3589)
    if team.nil?
      team = Team.create(name: 'TU_Kaiserslautern', year: 2020, country: 'Germany', igem_team_id: 3589)
    else
      team.update(name: 'TU_Kaiserslautern', year: 2020, country: 'Germany', igem_team_id: 3589)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3392)
    if team.nil?
      team = Team.create(name: 'UCAS-China', year: 2020, country: 'China', igem_team_id: 3392)
    else
      team.update(name: 'UCAS-China', year: 2020, country: 'China', igem_team_id: 3392)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3436)
    if team.nil?
      team = Team.create(name: 'UChicago', year: 2020, country: 'United States', igem_team_id: 3436)
    else
      team.update(name: 'UChicago', year: 2020, country: 'United States', igem_team_id: 3436)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3601)
    if team.nil?
      team = Team.create(name: 'UCL', year: 2020, country: 'United Kingdom', igem_team_id: 3601)
    else
      team.update(name: 'UCL', year: 2020, country: 'United Kingdom', igem_team_id: 3601)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3617)
    if team.nil?
      team = Team.create(name: 'UCopenhagen', year: 2020, country: 'Denmark', igem_team_id: 3617)
    else
      team.update(name: 'UCopenhagen', year: 2020, country: 'Denmark', igem_team_id: 3617)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3426)
    if team.nil?
      team = Team.create(name: 'UCSC', year: 2020, country: 'United States', igem_team_id: 3426)
    else
      team.update(name: 'UCSC', year: 2020, country: 'United States', igem_team_id: 3426)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3669)
    if team.nil?
      team = Team.create(name: 'UC_Davis', year: 2020, country: 'United States', igem_team_id: 3669)
    else
      team.update(name: 'UC_Davis', year: 2020, country: 'United States', igem_team_id: 3669)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3613)
    if team.nil?
      team = Team.create(name: 'UC_San_Diego', year: 2020, country: 'United States', igem_team_id: 3613)
    else
      team.update(name: 'UC_San_Diego', year: 2020, country: 'United States', igem_team_id: 3613)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3404)
    if team.nil?
      team = Team.create(name: 'UESTC-Software', year: 2020, country: 'China', igem_team_id: 3404)
    else
      team.update(name: 'UESTC-Software', year: 2020, country: 'China', igem_team_id: 3404)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3424)
    if team.nil?
      team = Team.create(name: 'UFlorida', year: 2020, country: 'United States', igem_team_id: 3424)
    else
      team.update(name: 'UFlorida', year: 2020, country: 'United States', igem_team_id: 3424)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3701)
    if team.nil?
      team = Team.create(name: 'UGent2_Belgium', year: 2020, country: 'Belgium', igem_team_id: 3701)
    else
      team.update(name: 'UGent2_Belgium', year: 2020, country: 'Belgium', igem_team_id: 3701)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3687)
    if team.nil?
      team = Team.create(name: 'UGent_Belgium', year: 2020, country: 'Belgium', igem_team_id: 3687)
    else
      team.update(name: 'UGent_Belgium', year: 2020, country: 'Belgium', igem_team_id: 3687)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3353)
    if team.nil?
      team = Team.create(name: 'UiOslo_Norway', year: 2020, country: 'Norway', igem_team_id: 3353)
    else
      team.update(name: 'UiOslo_Norway', year: 2020, country: 'Norway', igem_team_id: 3353)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3627)
    if team.nil?
      team = Team.create(name: 'UIUC_Illinois', year: 2020, country: 'United States', igem_team_id: 3627)
    else
      team.update(name: 'UIUC_Illinois', year: 2020, country: 'United States', igem_team_id: 3627)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3493)
    if team.nil?
      team = Team.create(name: 'ULaval', year: 2020, country: 'Canada', igem_team_id: 3493)
    else
      team.update(name: 'ULaval', year: 2020, country: 'Canada', igem_team_id: 3493)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3679)
    if team.nil?
      team = Team.create(name: 'UMaryland', year: 2020, country: 'United States', igem_team_id: 3679)
    else
      team.update(name: 'UMaryland', year: 2020, country: 'United States', igem_team_id: 3679)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3630)
    if team.nil?
      team = Team.create(name: 'UM_Macau', year: 2020, country: 'Macao', igem_team_id: 3630)
    else
      team.update(name: 'UM_Macau', year: 2020, country: 'Macao', igem_team_id: 3630)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3482)
    if team.nil?
      team = Team.create(name: 'UNILausanne', year: 2020, country: 'Switzerland', igem_team_id: 3482)
    else
      team.update(name: 'UNILausanne', year: 2020, country: 'Switzerland', igem_team_id: 3482)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3558)
    if team.nil?
      team = Team.create(name: 'UNSW_Australia', year: 2020, country: 'Australia', igem_team_id: 3558)
    else
      team.update(name: 'UNSW_Australia', year: 2020, country: 'Australia', igem_team_id: 3558)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3425)
    if team.nil?
      team = Team.create(name: 'UofUppsala', year: 2020, country: 'Sweden', igem_team_id: 3425)
    else
      team.update(name: 'UofUppsala', year: 2020, country: 'Sweden', igem_team_id: 3425)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3612)
    if team.nil?
      team = Team.create(name: 'UPCH_Peru', year: 2020, country: 'Peru', igem_team_id: 3612)
    else
      team.update(name: 'UPCH_Peru', year: 2020, country: 'Peru', igem_team_id: 3612)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3484)
    if team.nil?
      team = Team.create(name: 'UPF_Barcelona', year: 2020, country: 'Spain', igem_team_id: 3484)
    else
      team.update(name: 'UPF_Barcelona', year: 2020, country: 'Spain', igem_team_id: 3484)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3347)
    if team.nil?
      team = Team.create(name: 'USAFA', year: 2020, country: 'United States', igem_team_id: 3347)
    else
      team.update(name: 'USAFA', year: 2020, country: 'United States', igem_team_id: 3347)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3336)
    if team.nil?
      team = Team.create(name: 'USTC-Software', year: 2020, country: 'China', igem_team_id: 3336)
    else
      team.update(name: 'USTC-Software', year: 2020, country: 'China', igem_team_id: 3336)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3339)
    if team.nil?
      team = Team.create(name: 'UTTyler', year: 2020, country: 'United States', igem_team_id: 3339)
    else
      team.update(name: 'UTTyler', year: 2020, country: 'United States', igem_team_id: 3339)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3443)
    if team.nil?
      team = Team.create(name: 'UUlm', year: 2020, country: 'Germany', igem_team_id: 3443)
    else
      team.update(name: 'UUlm', year: 2020, country: 'Germany', igem_team_id: 3443)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3610)
    if team.nil?
      team = Team.create(name: 'UZurich', year: 2020, country: 'Switzerland', igem_team_id: 3610)
    else
      team.update(name: 'UZurich', year: 2020, country: 'Switzerland', igem_team_id: 3610)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3416)
    if team.nil?
      team = Team.create(name: 'Vilnius-Lithuania', year: 2020, country: 'Lithuania', igem_team_id: 3416)
    else
      team.update(name: 'Vilnius-Lithuania', year: 2020, country: 'Lithuania', igem_team_id: 3416)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3615)
    if team.nil?
      team = Team.create(name: 'Virginia', year: 2020, country: 'United States', igem_team_id: 3615)
    else
      team.update(name: 'Virginia', year: 2020, country: 'United States', igem_team_id: 3615)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3438)
    if team.nil?
      team = Team.create(name: 'VIT_Vellore', year: 2020, country: 'India', igem_team_id: 3438)
    else
      team.update(name: 'VIT_Vellore', year: 2020, country: 'India', igem_team_id: 3438)
    end
    if 'Withdrawn' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3625)
    if team.nil?
      team = Team.create(name: 'Warwick', year: 2020, country: 'United Kingdom', igem_team_id: 3625)
    else
      team.update(name: 'Warwick', year: 2020, country: 'United Kingdom', igem_team_id: 3625)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3580)
    if team.nil?
      team = Team.create(name: 'Waseda', year: 2020, country: 'Japan', igem_team_id: 3580)
    else
      team.update(name: 'Waseda', year: 2020, country: 'Japan', igem_team_id: 3580)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3381)
    if team.nil?
      team = Team.create(name: 'Waterloo', year: 2020, country: 'Canada', igem_team_id: 3381)
    else
      team.update(name: 'Waterloo', year: 2020, country: 'Canada', igem_team_id: 3381)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3562)
    if team.nil?
      team = Team.create(name: 'WHU-China', year: 2020, country: 'China', igem_team_id: 3562)
    else
      team.update(name: 'WHU-China', year: 2020, country: 'China', igem_team_id: 3562)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3517)
    if team.nil?
      team = Team.create(name: 'William_and_Mary', year: 2020, country: 'United States', igem_team_id: 3517)
    else
      team.update(name: 'William_and_Mary', year: 2020, country: 'United States', igem_team_id: 3517)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3578)
    if team.nil?
      team = Team.create(name: 'Worldshaper-Nanjing', year: 2020, country: 'China', igem_team_id: 3578)
    else
      team.update(name: 'Worldshaper-Nanjing', year: 2020, country: 'China', igem_team_id: 3578)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3577)
    if team.nil?
      team = Team.create(name: 'Worldshaper-Shanghai', year: 2020, country: 'China', igem_team_id: 3577)
    else
      team.update(name: 'Worldshaper-Shanghai', year: 2020, country: 'China', igem_team_id: 3577)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3638)
    if team.nil?
      team = Team.create(name: 'Worldshaper-Wuhan', year: 2020, country: 'China', igem_team_id: 3638)
    else
      team.update(name: 'Worldshaper-Wuhan', year: 2020, country: 'China', igem_team_id: 3638)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3568)
    if team.nil?
      team = Team.create(name: 'XH-China', year: 2020, country: 'China', igem_team_id: 3568)
    else
      team.update(name: 'XH-China', year: 2020, country: 'China', igem_team_id: 3568)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3698)
    if team.nil?
      team = Team.create(name: 'XHD-ShanDong-China', year: 2020, country: 'China', igem_team_id: 3698)
    else
      team.update(name: 'XHD-ShanDong-China', year: 2020, country: 'China', igem_team_id: 3698)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3560)
    if team.nil?
      team = Team.create(name: 'XHD-Wuhan-China', year: 2020, country: 'China', igem_team_id: 3560)
    else
      team.update(name: 'XHD-Wuhan-China', year: 2020, country: 'China', igem_team_id: 3560)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3523)
    if team.nil?
      team = Team.create(name: 'Xiamen_city', year: 2020, country: 'China', igem_team_id: 3523)
    else
      team.update(name: 'Xiamen_city', year: 2020, country: 'China', igem_team_id: 3523)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3331)
    if team.nil?
      team = Team.create(name: 'XJTU-China', year: 2020, country: 'China', igem_team_id: 3331)
    else
      team.update(name: 'XJTU-China', year: 2020, country: 'China', igem_team_id: 3331)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3332)
    if team.nil?
      team = Team.create(name: 'XMU-China', year: 2020, country: 'China', igem_team_id: 3332)
    else
      team.update(name: 'XMU-China', year: 2020, country: 'China', igem_team_id: 3332)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3398)
    if team.nil?
      team = Team.create(name: 'ZJU-China', year: 2020, country: 'China', igem_team_id: 3398)
    else
      team.update(name: 'ZJU-China', year: 2020, country: 'China', igem_team_id: 3398)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
    team = Team.find_by(igem_team_id: 3406)
    if team.nil?
      team = Team.create(name: 'ZJUT_China_B', year: 2020, country: 'China', igem_team_id: 3406)
    else
      team.update(name: 'ZJUT_China_B', year: 2020, country: 'China', igem_team_id: 3406)
    end
    if 'Accepted' == 'Accepted'
      team.registered = true
    else
      team.registered = false
    end
    team.save
