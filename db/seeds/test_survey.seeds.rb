def get_users(team_id)
  g = Team.find(team_id).igem_users.pluck(:id, :full_name).map do |u|
    { "label": u[1], "value": u[0] }
  end
end

survey_name = "Survey test"
survey_description = "Survey description"
survey_time = "00:30:00"

Team.where(id: 1).each do |team|
  survey = Survey.find_by(team_id: team.id, name: survey_name)

  if survey.nil?
    survey = Survey.create(team_id: team.id, name: survey_name, description: survey_description, time: survey_time)

    ############ Question 21
    sf = SurveyField.new(survey_id: survey.id, category: "inputCountry", name: "What city and country did you live in during the competition cycle?", required: true)
    sf.content = {"no_options": true}
    sf.save

    ############ Question 22
    sf = SurveyField.new(survey_id: survey.id, category: "inputDate", name: "What is your year of birth?", required: true)
    sf.content = {"years": true}
    sf.save

    sf = SurveyField.new(survey_id: survey.id, category: "inputPercentage", name: "For your part of the project, please indicate the percentage of your work that fitseach of the following descriptions.", required: true)
    sf.content = {"text": "Enter the percentage for each descriptions. The sum must be equal to 100.",
                  "options": [{"pos": 1, "label": "Independentwork flow, where work and activities are performed by you and your teammates independently and do not flow between you", "value": 0},
                              {"pos": 2, "label": "Sequentialwork flow, where work and activities flow between you and your team mates in one direction", "value": 1},
                              {"pos": 3, "label": "Reciprocalwork flow, where work and activities flow between you and your teammates in a reciprocal \"back and forth\" manner over a period oftime", "value": 2},
                              {"pos": 4, "label": "Teamwork flow, where you and your teammates diagnose, problem solve and collaborate as a group at thesame timeto deal with the work", "value": 3},
                  ]}
    sf.save

  end
end
