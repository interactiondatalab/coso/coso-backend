team_id = 257
survey_example = nil
# survey_example = Survey.find_by(name: "Survey example")

if survey_example.nil?
  puts "Creating Survey"
  survey_example = Survey.create(team_id: team_id, name: "Survey example", description: "Survey example for the iGEM-ties/CRI team", time: "12:00:00")

  survey_field = SurveyField.new
  survey_field.survey_id = survey_example.id
  survey_field.name = "Example inputRadioList"
  survey_field.category = "inputRadioList"
  survey_field.required = true
  survey_field.content = {
    "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet commodo nulla facilisi nullam vehicula ipsum a arcu. At in tellus integer feugiat scelerisque varius morbi enim. Purus in mollis nunc sed id semper risus.",
    "options":
      [
        {"pos":1, "label":"Someone", "value":"someone_id"},
        {"pos":2, "label":"Someoneelse", "value":"someone_else_id"}
      ]
    }
  survey_field.save

  survey_field = SurveyField.new
  survey_field.survey_id = survey_example.id
  survey_field.category = "inputRadioMatrix"
  survey_field.name = "To what extent did your team do each of the following?"
  survey_field.required = true
  survey_field.content = {"text": "Please rate accordingly.",
                "options": {"columns": [{"pos": 1, "label": "1", "sublabel": "Not at all", "value": 1},
                                        {"pos": 2, "label": "2", "value": 2},
                                        {"pos": 3, "label": "3", "value": 3},
                                        {"pos": 4, "label": "4", "value": 4},
                                        {"pos": 5, "label": "5", "sublabel": "Very much so", "value": 5}
                                       ],
                            "rows": [{"pos": 1, "label": "Each team member had a set of rules, protocols or standards to follow when engaging in a task", "value": 1},
                                     {"pos": 2, "label": "There was a set of rules, protocols or standards to follow when engaging in each task", "value": 2},
                                     {"pos": 3, "label": "I followed specific protocols even when I thought there was a better way to perform that task", "value": 3},
                                     {"pos": 4, "label": "The instructors or advisors checked students’ lab notebooks at least once per week", "value": 4},
                                     {"pos": 5, "label": "There was a clear hierarchy in the team, such that students reported to team leaders and team leaders reported to instructors.", "value": 5},
                                     {"pos": 6, "label": "Each student in the team specialized in a single task", "value": 6},
                                     {"pos": 7, "label": "Each task was performed by multiple members of the team", "value": 7},
                                     {"pos": 8, "label": "Each member had the authority to make changes to their part of the project", "value": 8}
                                    ]
              }}
  survey_field.save

  survey_field = SurveyField.new
  survey_field.survey_id = survey_example.id
  survey_field.name = "Example inputCheckbox"
  survey_field.category = "inputCheckbox"
  survey_field.required = true
  survey_field.content = {
    "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet commodo nulla facilisi nullam vehicula ipsum a arcu. At in tellus integer feugiat scelerisque varius morbi enim. Purus in mollis nunc sed id semper risus.",
    "options":
      [
        {"pos":1,"label":"Someone","value":"someone_id"},
        {"pos":2,"label":"Someoneelse","value":"someone_else_id"}
      ]
    }
  survey_field.save

  survey_field = SurveyField.new
  survey_field.survey_id = survey_example.id
  survey_field.name = "Example inputRadioButton"
  survey_field.category = "inputRadioButton"
  survey_field.required = true
  survey_field.content = {
    "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet commodo nulla facilisi nullam vehicula ipsum a arcu. At in tellus integer feugiat scelerisque varius morbi enim. Purus in mollis nunc sed id semper risus.",
    "options":
    [
      {"pos":1,"label":"Yes","value":true},
      {"pos":2,"label":"No","value":false}
    ]
  }
  survey_field.save

  survey_field = SurveyField.new
  survey_field.survey_id = survey_example.id
  survey_field.name = "Example inputSlider"
  survey_field.category = "inputSlider"
  survey_field.required = true
  survey_field.content = {
    "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet commodo nulla facilisi nullam vehicula ipsum a arcu. At in tellus integer feugiat scelerisque varius morbi enim. Purus in mollis nunc sed id semper risus.",
    "max":10,
    "min":0,
    "step":1,
    "labels": {
      "min": "Not at all",
      "max": "A lot"
    }
  }
  survey_field.save

  survey_field = SurveyField.new
  survey_field.survey_id = survey_example.id
  survey_field.name = "Example inputDate"
  survey_field.category = "inputDate"
  survey_field.required = true
  survey_field.content = {
    "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet commodo nulla facilisi nullam vehicula ipsum a arcu. At in tellus integer feugiat scelerisque varius morbi enim. Purus in mollis nunc sed id semper risus.",
    "no_options":true
  }
  survey_field.save

  survey_field = SurveyField.new
  survey_field.survey_id = survey_example.id
  survey_field.name = "Example listNames"
  survey_field.category = "listNames"
  survey_field.required = true
  survey_field.content = {
    "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet commodo nulla facilisi nullam vehicula ipsum a arcu. At in tellus integer feugiat scelerisque varius morbi enim. Purus in mollis nunc sed id semper risus.",
    "no_options":true
  }
  survey_field.save

  survey_field = SurveyField.new
  survey_field.survey_id = survey_example.id
  survey_field.name = "Example inputTextfield"
  survey_field.category = "inputTextfield"
  survey_field.required = true
  survey_field.content = {
    "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet commodo nulla facilisi nullam vehicula ipsum a arcu. At in tellus integer feugiat scelerisque varius morbi enim. Purus in mollis nunc sed id semper risus.",
    "value":"somevalue",
    "placeholder":"Some text field"
  }
  survey_field.save

  survey_field = SurveyField.new
  survey_field.survey_id = survey_example.id
  survey_field.name = "Example inputField"
  survey_field.category = "inputField"
  survey_field.required = true
  survey_field.content = {
    "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet commodo nulla facilisi nullam vehicula ipsum a arcu. At in tellus integer feugiat scelerisque varius morbi enim. Purus in mollis nunc sed id semper risus.",
    "value":"somevalue",
    "placeholder":"Some text field"}
  survey_field.save

  survey_field = SurveyField.new
  survey_field.survey_id = survey_example.id
  survey_field.name = "Example inputSelect"
  survey_field.category = "inputSelect"
  survey_field.required = true
  survey_field.content = {
    "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet commodo nulla facilisi nullam vehicula ipsum a arcu. At in tellus integer feugiat scelerisque varius morbi enim. Purus in mollis nunc sed id semper risus.",
    "options":
    [
      {"pos":1,"label":"Team1","value":"team1_id"},
      {"pos":2,"label":"Team2","value":"team2_id"}
    ],
    "placeholder":"Sometextfield"
  }
  survey_field.save
end
