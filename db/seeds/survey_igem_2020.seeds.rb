def get_users(team_id)
  g = Team.find(team_id).igem_users.pluck(:id, :full_name).map do |u|
    { "label": u[1], "value": u[0] }
  end
end

survey_name = "iGEM Team Interaction Study"
survey_description = "Join the study and take part in the first large scale description of iGEM team work!"
survey_time = "00:30:00"

Team.where.not(id: 301).each do |team|
  survey = Survey.find_by(team_id: team.id, name: survey_name)

  if survey.nil?
    survey = Survey.create(team_id: team.id, name: survey_name, description: survey_description, time: survey_time)

    ############ Question 1
    sf = SurveyField.new(survey_id: survey.id, category: "inputCheckList", name: "What are your academic fields of study?", required: false)
    sf.content = {"text": "Select all that apply.",
      "options": [{"pos": 1, "label": "Business Management / Administration", "value": 0},
                  {"pos": 2, "label": "Communication", "value": 1},
                  {"pos": 3, "label": "Computer and Information Sciences", "value": 2},
                  {"pos": 4, "label": "Education", "value": 3},
                  {"pos": 5, "label": "Engineering", "value": 4},
                  {"pos": 6, "label": "Humanities", "value": 5},
                  {"pos": 7, "label": "Life / Biological Sciences", "value": 6},
                  {"pos": 8, "label": "Mathematics and Statistics", "value": 7},
                  {"pos": 9, "label": "Physical Sciences", "value": 8},
                  {"pos": 10, "label": "Social Sciences", "value": 9},
                  {"pos": 11, "label": "Other", "value": 10}
                 ]}
    sf.save

    ############ Question 2
    sf = SurveyField.new(survey_id: survey.id, category: "inputRadioList", name: "How many years of research or laboratory experience did you have not including previous participation in iGEM before the start of this year’s competition?", required: false)
    sf.content = {"text": "",
      "options": [{"pos": 1, "label": "None", "value": 0},
                  {"pos": 2, "label": "Less than 1 year", "value": 1},
                  {"pos": 3, "label": "More than 1 year but less than 2", "value": 2},
                  {"pos": 4, "label": "More than 2 years but less than 3", "value": 3},
                  {"pos": 5, "label": "More than 3 years but less than 4", "value": 4},
                  {"pos": 6, "label": "More than 4 years", "value": 5}
                 ]}
    sf.save

    ############ Question 3
    sf = SurveyField.new(survey_id: survey.id, category: "listNames", name: "Which of your teammates did you know personally before February, 2020?", required: false)
    sf.content = {"text": "Select your teammates by name from the list below.  You may need to scroll down to see all of them.", "options": get_users(team.id)}
    sf.save
    survey_field_listnames_id = sf.id

    ############ Question 4
    sf = SurveyField.new(survey_id: survey.id, category: "inputRadioMatrix", name: "How many years had you known each of these people?", required: false)
    sf.content = {"text": "",
      "options": {"columns": [{"pos": 1, "label": "Less than 1 year", "value": 0},
                              {"pos": 2, "label": "2 years", "value": 1},
                              {"pos": 3, "label": "3 years", "value": 2},
                              {"pos": 4, "label": "4 years", "value": 3},
                              {"pos": 5, "label": "5 or more years", "value": 4}
                             ],
                  "rows": [],
                  "row_dependency": {"survey_field_id": survey_field_listnames_id}
                }}
    sf.save

    ############ Question 5
    sf = SurveyField.new(survey_id: survey.id, category: "inputRadioMatrix", name: "How close was your relationship with each person at the start of this year’s competition?", required: false)
    sf.content = {"text": "",
      "options": {"columns": [{"pos": 1, "label": "Not at all close", "value": 0},
                              {"pos": 2, "label": "A little close", "value": 1},
                              {"pos": 3, "label": "Moderately close", "value": 2},
                              {"pos": 4, "label": "Very close", "value": 3},
                              {"pos": 5, "label": "Extremely close", "value": 4}
                             ],
                  "rows": [],
                  "row_dependency": {"survey_field_id": survey_field_listnames_id}
                }}
    sf.save

    ############ Question 6
    sf = SurveyField.new(survey_id: survey.id, category: "inputRadioList", name: "Was there an application or selection process to join the team?", required: false)
    sf.content = {"text": "",
      "options": [{"pos": 1, "label": "Yes", "value": 1},
                  {"pos": 2, "label": "No", "value": 0},
                  {"pos": 3, "label": "Don't know", "value": 2}
                 ]}
    sf.save

    ############ Question 7
    sf = SurveyField.new(survey_id: survey.id, category: "inputRadioList", name: "Did your iGEM team provide stipends, fellowships or other financial support for team members?", required: false)
    sf.content = {"text": "",
      "options": [{"pos": 1, "label": "Yes", "value": 1},
                  {"pos": 2, "label": "No", "value": 0},
                  {"pos": 3, "label": "Don't know", "value": 2}
             ]}
    sf.save
    survey_field_stipend_id = sf.id

    ############ Question 8
    sf = SurveyField.new(survey_id: survey.id, category: "inputRadioList", name: "Please tell us the approximate amount of funds used for your team’s project this year.", required: false)
    sf.content = {"text": "The research funds should include all costs for conducting the research, participation and registration fees for iGEM, financial support for team members and travel if applicable.",
      "activable": {"survey_field_id": survey_field_stipend_id, "values_accepted": [1]},
      "options": [{"pos": 1, "label": "Less than $5,000", "value": 0},
                  {"pos": 2, "label": "More than $5,000 but not more than $10,000", "value": 1},
                  {"pos": 3, "label": "More than $10,000 but not more than $15,000", "value": 2},
                  {"pos": 4, "label": "More than $15,000 but not more than $20,000", "value": 3},
                  {"pos": 5, "label": "More than $20,000 but not more than $25,000", "value": 4},
                  {"pos": 6, "label": "More than $25,000 but not more than $30,000", "value": 5},
                  {"pos": 7, "label": "More than $30,000", "value": 6}
                 ]}
    sf.save

    ############ Question 9
    sf = SurveyField.new(survey_id: survey.id, category: "inputRadioList", name: "In what month did your team first begin working on the project including coming up with ideas and planning?", required: false)
    sf.content = {"text": "",
      "options": [{"pos": 1, "label": "November, 2019", "value": 0},
                  {"pos": 2, "label": "December, 2020", "value": 1},
                  {"pos": 3, "label": "January, 2020", "value": 2},
                  {"pos": 4, "label": "February, 2020", "value": 3},
                  {"pos": 5, "label": "March, 2020", "value": 4},
                  {"pos": 6, "label": "April, 2020", "value": 5},
                  {"pos": 7, "label": "June, 2020", "value": 6},
                  {"pos": 8, "label": "July, 2020", "value": 7},
                  {"pos": 9, "label": "August, 2020", "value": 8},
                  {"pos": 10, "label": "September, 2020", "value": 9}
                 ]}
    sf.save

    ############ Question 10
    sf = SurveyField.new(survey_id: survey.id, category: "inputRadioMatrix", name: "What tasks did you perform for your team’s iGEM project this year?", required: false)
    sf.content = {"text": "",
                  "options": {"columns": [{"pos": 1, "label": "No role", "value": 0},
                                          {"pos": 2, "label": "Minor role", "value": 1},
                                          {"pos": 3, "label": "Main role", "value": 2}
                                         ],
                              "rows": [{"pos": 1, "label": "<b>Conceptualization</b></br>Brainstorming or developing ideas for the project", "value": 0},
                                       {"pos": 2, "label": "<b>Investigation</b></br>Performing the experiments or data/evidence collection", "value": 1},
                                       {"pos": 3, "label": "<b>Data Curation</b></br>Annotating (producing metadata), cleaning and maintaining research data for initial use and later use", "value": 2},
                                       {"pos": 4, "label": "<b>Analysis</b></br>Analyzing quantitative or qualitative data", "value": 3},
                                       {"pos": 5, "label": "<b>Lab Maintenance</b></br>Cleaning, organizing and preparing the research facilities or lab", "value": 4},
                                       {"pos": 6, "label": "<b>Fundraising</b></br>Raising money or donations of materials or services for the team’s activities", "value": 5},
                                       {"pos": 7, "label": "<b>Public Engagement</b></br>Preparing or implementing tools or activities to engage the broader community", "value": 6},
                                       {"pos": 8, "label": "<b>Entrepreneurship</b></br>Developing business models and other commercial applications", "value": 7},
                                       {"pos": 9, "label": "<b>Reading</b></br>Peer-reviewed scientific research articles, white papers, regulations or guidelines and other documents", "value": 8},
                                       {"pos": 10, "label": "<b>Safety</b></br>Performing activities to ensure compliance with the safety guidelines or requirements of iGEM, your institution or government", "value": 9},
                                       {"pos": 11, "label": "<b>Software</b></br>Developing, implementing and testing computer programs and code", "value": 10},
                                       {"pos": 12, "label": "<b>Hardware</b></br>Designing, building or testing mechanical, electrical or optical hardware systems", "value": 11},
                                       {"pos": 13, "label": "<b>Project Administration</b></br>Managing and coordinating the project activities, planning and execution", "value": 12},
                                       {"pos": 14, "label": "<b>Visualization</b></br>Preparation, creation and/or presentation of the work including data visualization, user interfaces, videos and all types of art", "value": 13},
                                       {"pos": 15, "label": "<b>Writing</b></br>Writing, reviewing or editing content for the wiki or other documents to be shared outside the team", "value": 14}
                                     ]
                 }}
    sf.save
    survey_field_listtasks_id = sf.id

    ############ Question 11
    sf = SurveyField.new(survey_id: survey.id, category: "inputCheckList", name: "Which aspects of the project did your work on data collection, analysis and/or interpretation contribute to?", required: false)
    sf.content = {"text": "Select all that apply.", "activable": {"survey_field_id": survey_field_listtasks_id, "values_accepted": [1,2], "questions": [1,2,3]},
      "options": [{"pos": 1, "label": "Characterizing an existing part", "value": 0},
                  {"pos": 2, "label": "Contributing a new part", "value": 1},
                  {"pos": 3, "label": "Human practices", "value": 2},
                  {"pos": 4, "label": "Other", "value": 3}
                 ]}
    sf.save

    ############ Question 12
    sf = SurveyField.new(survey_id: survey.id, category: "inputCheckList", name: "Which aspects of the project did your public engagement work contribute to?", required: false)
    sf.content = {"text": "Select all that apply.", "activable": {"survey_field_id": survey_field_listtasks_id, "values_accepted": [1,2], "questions": [6]},
      "options": [{"pos": 1, "label": "Entrepreneurship", "value": 0},
                  {"pos": 2, "label": "Human practices", "value": 1},
                  {"pos": 3, "label": "Education and outreach", "value": 2},
                  {"pos": 4, "label": "Other", "value": 3}
                 ]}
    sf.save

    ############ Question 13
    sf = SurveyField.new(survey_id: survey.id, category: "inputCheckboxMatrix", name: "Which of your teammates did you work with on each task?", required: false)
    sf.content = {"text": "",
                  "options": {"columns": get_users(team.id),
                              "rows": [],
                              "row_dependency": {"survey_field_id": survey_field_listtasks_id, "skip_values": [0,1]}
                 }}
    sf.save

    ############ Question 14
    sf = SurveyField.new(survey_id: survey.id, category: "inputRadioMatrix", name: "To what extent did your team do each of the following?", required: false)
    sf.content = {"text": "Please rate accordingly.",
                  "options": {"columns": [{"pos": 1, "label": "1", "sublabel": "Not at all", "value": 0},
                                          {"pos": 2, "label": "2", "value": 1},
                                          {"pos": 3, "label": "3", "value": 2},
                                          {"pos": 4, "label": "4", "value": 3},
                                          {"pos": 5, "label": "5", "sublabel": "Very much so", "value": 4}
                                         ],
                              "rows": [{"pos": 1, "label": "The project choice was the result of a group decision among all the members of the team", "value": 0},
                                       {"pos": 2, "label": "Decisions about the initial research protocol were made collectively by the whole research group", "value": 1},
                                       {"pos": 3, "label": "The existing equipment in our lab was an important factor in choosing this project", "value": 2},
                                       {"pos": 4, "label": "During the course of the project, students developed their own changes in the research protocol", "value": 3},
                                       {"pos": 5, "label": "The whole team met every week to share information on project progress", "value": 4},
                                       {"pos": 6, "label": "Team members kept physical or digital lab notebooks to document their work", "value": 5},
                                       {"pos": 7, "label": "The team leaders checked the students’ lab notebooks at least once per week", "value": 6},
                                      ]
                 }}
    sf.save

    sf = SurveyField.new(survey_id: survey.id, category: "inputRadioMatrix", name: "To what extent did your team do each of the following?", required: false)
    sf.content = {"text": "Please rate accordingly.",
                  "options": {"columns": [{"pos": 1, "label": "1", "sublabel": "Not at all", "value": 0},
                                          {"pos": 2, "label": "2", "value": 1},
                                          {"pos": 3, "label": "3", "value": 2},
                                          {"pos": 4, "label": "4", "value": 3},
                                          {"pos": 5, "label": "5", "sublabel": "Very much so", "value": 4}
                                         ],
                              "rows": [{"pos": 1, "label": "Each team member had a set of rules, protocols or standards to follow when engaging in a task", "value": 7},
                                       {"pos": 2, "label": "There was a set of rules, protocols or standards to follow when engaging in each task", "value": 8},
                                       {"pos": 3, "label": "I followed specific protocols even when I thought there was a better way to perform that task", "value": 9},
                                       {"pos": 4, "label": "The instructors or advisors checked students’ lab notebooks at least once per week", "value": 10},
                                       {"pos": 5, "label": "There was a clear hierarchy in the team, such that students reported to team leaders and team leaders reported to instructors.", "value": 11},
                                       {"pos": 6, "label": "Each student in the team specialized in a single task", "value": 12},
                                       {"pos": 7, "label": "Each task was performed by multiple members of the team", "value": 13},
                                       {"pos": 8, "label": "Each member had the authority to make changes to their part of the project", "value": 14}
                                      ]
                }}
    sf.save

    ############ Question 15
    sf = SurveyField.new(survey_id: survey.id, category: "inputRadioMatrix", name: "Many schools, colleges and universities closed down or limited access to their buildings for non-essential personnel as a result of the Covid-19 pandemic. For the institution with which your iGEM team is affiliated, <b>please tell us the level of access you had to your own research facilities in each month</b>.", required: false)
    sf.content = {"text": "",
                  "options": {"columns": [{"pos": 1, "label": "Full access", "value": 0},
                                          {"pos": 2, "label": "Partial access", "value": 1},
                                          {"pos": 3, "label": "No access", "value": 2}
                                         ],
                              "rows": [{"pos": 1, "label": "February", "value": 0},
                                      {"pos": 2, "label": "March", "value": 1},
                                      {"pos": 3, "label": "April", "value": 2},
                                      {"pos": 4, "label": "May", "value": 3},
                                      {"pos": 5, "label": "June", "value": 4},
                                      {"pos": 6, "label": "July", "value": 5},
                                      {"pos": 7, "label": "September", "value": 6},
                                      {"pos": 8, "label": "October", "value": 7}
                                    ]
                 }}
    sf.save

    ############ Question 16
    sf = SurveyField.new(survey_id: survey.id, category: "inputRadioMatrix", name: "To what extent did you have difficulty accessing potentially important research facilities, databases and information for your project?", required: false)
    sf.content = {"text": "Please rate accordingly.",
                  "options": {"columns": [{"pos": 1, "label": "Did not need", "value": 0},
                                          {"pos": 2, "label": "1", "sublabel": "No difficulty getting access", "value": 1},
                                          {"pos": 3, "label": "2", "value": 2},
                                          {"pos": 4, "label": "3", "value": 3},
                                          {"pos": 5, "label": "4", "value": 4},
                                          {"pos": 6, "label": "5", "sublabel": "Great difficulty getting access", "value": 5}
                                         ],
                              "rows": [{"pos": 1, "label": "Access to external research facilities", "value": 0},
                                       {"pos": 2, "label": "Databases of journals or published papers", "value": 1},
                                       {"pos": 3, "label": "Research tool databases (genomes, materials, etc.)", "value": 2},
                                       {"pos": 4, "label": "Access to information from other iGEM teams competing this year", "value": 3},
                                       {"pos": 5, "label": "Access to information from other iGEM teams that competed in previous years", "value": 4}
                                     ]
                 }}
    sf.save

    ############ Question 17
    # sf = SurveyField.new(survey_id: survey.id, category: "inputSlider", name: "To what extent did the people in your team have one-person jobs: that is, in order to get the work out, to what extent do team members independently accomplish their own assigned tasks?", required: false)
    # sf.content = {"text": "1=Very little; 7=To a great extent", "min": 1, "max": 7, "step": 1}
    # sf.save
    sf = SurveyField.new(survey_id: survey.id, category: "inputRadioMatrix", name: "To what extent did the people in your team have one-person jobs: that is, in order to get the work out, to what extent do team members independently accomplish their own assigned tasks?", required: false)
    sf.content = {"text": "Please rate accordingly.",
                  "options": {"columns": [{"pos": 1, "label": "1", "sublabel": "Very little", "value": 0},
                                          {"pos": 2, "label": "2", "value": 1},
                                          {"pos": 3, "label": "3", "value": 2},
                                          {"pos": 4, "label": "4", "value": 3},
                                          {"pos": 5, "label": "5", "value": 4},
                                          {"pos": 6, "label": "6", "value": 5},
                                          {"pos": 7, "label": "7", "sublabel": "To a great extent", "value": 6}
                                         ],
                              "rows": [{"pos": 1, "label": "", "value": 0}
                                      ]
                 }}
    sf.save

    ############ Question 18
    # sf = SurveyField.new(survey_id: survey.id, category: "inputSlider", name: "To what extent do all the team members meet together to discuss how each task, case or problem should be handled in order to complete the project", required: false)
    # sf.content = {"text": "1=Very little; 7=To a great extent", "min": 1, "max": 7, "step": 1}
    # sf.save
    sf = SurveyField.new(survey_id: survey.id, category: "inputRadioMatrix", name: "To what extent do all the team members meet together to discuss how each task, case or problem should be handled in order to complete the project?", required: false)
    sf.content = {"text": "Please rate accordingly.",
                  "options": {"columns": [{"pos": 1, "label": "1", "sublabel": "Very little", "value": 0},
                                          {"pos": 2, "label": "2", "value": 1},
                                          {"pos": 3, "label": "3", "value": 2},
                                          {"pos": 4, "label": "4", "value": 3},
                                          {"pos": 5, "label": "5", "value": 4},
                                          {"pos": 6, "label": "6", "value": 5},
                                          {"pos": 7, "label": "7", "sublabel": "To a great extent", "value": 6}
                                         ],
                              "rows": [{"pos": 1, "label": "", "value": 0}
                                      ]
                 }}
    sf.save

    ############ Question 19
    sf = SurveyField.new(survey_id: survey.id, category: "inputPercentage", name: "For your part of the project, please indicate the percentage of your work that fits each of the following descriptions.", required: false)
    sf.content = {"text": "Enter the percentage that fits each description below. The responses should add up to 100.",
                  "options": [{"pos": 1, "label": "<b>INDEPENDENT</b> work flow where work and activities are performed by you and your teammates independently and do not flow between you", "value": 0},
                              {"pos": 2, "label": "<b>SEQUENTIAL</b> work flow where work and activities flow between you and your team mates in one direction", "value": 1},
                              {"pos": 3, "label": "<b>RECIPROCAL</b> work flow where work and activities flow between you and your teammates in a reciprocal \"back and forth\" manner over a period of time", "value": 2},
                              {"pos": 4, "label": "<b>TEAM</b> work flow where you and your teammates diagnose, problem solve and collaborate as a group at the same time to deal with the work", "value": 3},
                  ]}
    sf.save

    ############ Question 20
    sf = SurveyField.new(survey_id: survey.id, category: "inputRadioMatrix", name: "Please tell us how often, during the competition cycle you communicated with other members of your team", required: false)
    sf.content = {"text": "",
                  "options": {"columns": [{"pos": 1, "label": "Never", "value": 0},
                                          {"pos": 2, "label": "Rarely", "value": 1},
                                          {"pos": 3, "label": "Monthly", "value": 2},
                                          {"pos": 4, "label": "Bi-weekly", "value": 3},
                                          {"pos": 5, "label": "Daily", "value": 4}
                                         ],
                              "rows": [{"pos": 1, "label": "In-person communication (face-to-face)", "value": 0},
                                       {"pos": 2, "label": "Voice communication (e.g., telephone)", "value": 1},
                                       {"pos": 3, "label": "Videochat (e.g., skype or facetime)", "value": 2},
                                       {"pos": 4, "label": "Synchronous text communication (e.g. texting)", "value": 3},
                                       {"pos": 5, "label": "Asynchronous text communication (e.g. email)", "value": 4}
                                     ]
                 }}
    sf.save

    ############ Question 21
    sf = SurveyField.new(survey_id: survey.id, category: "inputCountry", name: "What city and country did you live in during the competition cycle?", required: false)
    sf.content = {"no_options": true}
    sf.save

    ############ Question 22
    sf = SurveyField.new(survey_id: survey.id, category: "inputDate", name: "What is your year of birth?", required: false)
    sf.content = {"years": true}
    sf.save

    ############ Question 23
    sf = SurveyField.new(survey_id: survey.id, category: "inputRadioList", name: "What is your current gender?", required: false)
    sf.content = {"text": "",
      "options": [{"pos": 1, "label": "Woman", "value": 0},
                  {"pos": 2, "label": "Man", "value": 1},
                  {"pos": 3, "label": "Transgender", "value": 2},
                  {"pos": 4, "label": "A gender not listed here", "value": 3},
                  {"pos": 5, "label": "I don't know", "value": 4},
                  {"pos": 6, "label": "Prefer not to answer", "value": 5}
                 ]}
    sf.save

  end
end
