task = Task.find_by(name: "Brainstorming")
if task.nil?
  task = Task.create(category: "Brainstorming", name: "Brainstorming", description: "")
end
task = Task.find_by(name: "Meetings")
if task.nil?
  task = Task.create(category: "Meetings", name: "Meetings", description: "")
end
task = Task.find_by(name: "Planning tasks")
if task.nil?
  task = Task.create(category: "Planning tasks", name: "Planning tasks", description: "")
end
task = Task.find_by(name: "Developing protocols")
if task.nil?
  task = Task.create(category: "Developing protocols", name: "Developing protocols", description: "")
end
task = Task.find_by(name: "Reading papers")
if task.nil?
  task = Task.create(category: "Reading papers", name: "Reading papers", description: "")
end
task = Task.find_by(name: "Education event")
if task.nil?
  task = Task.create(category: "Education event", name: "Education event", description: "")
end
task = Task.find_by(name: "Meetups")
if task.nil?
  task = Task.create(category: "Meetups", name: "Meetups", description: "")
end
task = Task.find_by(name: "Collaborating with tasks")
if task.nil?
  task = Task.create(category: "Collaborating with tasks", name: "Collaborating with tasks", description: "")
end
task = Task.find_by(name: "Lab maintenance")
if task.nil?
  task = Task.create(category: "Lab maintenance", name: "Lab maintenance", description: "")
end
task = Task.find_by(name: "Running assays")
if task.nil?
  task = Task.create(category: "Running assays", name: "Running assays", description: "")
end
task = Task.find_by(name: "Preparing samples")
if task.nil?
  task = Task.create(category: "Preparing samples", name: "Preparing samples", description: "")
end
task = Task.find_by(name: "Analysing results")
if task.nil?
  task = Task.create(category: "Analysing results", name: "Analysing results", description: "")
end
task = Task.find_by(name: "Interpreting results")
if task.nil?
  task = Task.create(category: "Interpreting results", name: "Interpreting results", description: "")
end
task = Task.find_by(name: "Preparing docs to share outside")
if task.nil?
  task = Task.create(category: "Preparing docs to share outside", name: "Preparing docs to share outside", description: "")
end
task = Task.find_by(name: "Public outreach event")
if task.nil?
  task = Task.create(category: "Public outreach event", name: "Public outreach event", description: "")
end
task = Task.find_by(name: "Writing / preparing presentations")
if task.nil?
  task = Task.create(category: "Writing / preparing presentations", name: "Writing / preparing presentations", description: "")
end
task = Task.find_by(name: "Project administration")
if task.nil?
  task = Task.create(category: "Project administration", name: "Project administration", description: "")
end
task = Task.find_by(name: "Software development")
if task.nil?
  task = Task.create(category: "Software development", name: "Software development", description: "")
end
task = Task.find_by(name: "Hardware development")
if task.nil?
  task = Task.create(category: "Hardware development", name: "Hardware development", description: "")
end
