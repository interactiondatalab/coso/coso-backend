relation_category = RelationCategory.find_by(name: "Another iGEM team")
if relation_category.nil?
  relation_category = RelationCategory.create(name: "Another iGEM team")
end
relation_category = RelationCategory.find_by(name: "iGEM HQ")
if relation_category.nil?
  relation_category = RelationCategory.create(name: "iGEM HQ")
end
relation_category = RelationCategory.find_by(name: "My school/university/institution")
if relation_category.nil?
  relation_category = RelationCategory.create(name: "My school/university/institution")
end
relation_category = RelationCategory.find_by(name: "Company/Nonprofit")
if relation_category.nil?
  relation_category = RelationCategory.create(name: "Company/Nonprofit")
end
relation_category = RelationCategory.find_by(name: "Government")
if relation_category.nil?
  relation_category = RelationCategory.create(name: "Government")
end
relation_category = RelationCategory.find_by(name: "Other")
if relation_category.nil?
  relation_category = RelationCategory.create(name: "Other")
end
