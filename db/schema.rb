# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_05_13_131439) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "app_activities", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "phone_id", null: false
    t.datetime "start_at", null: false
    t.datetime "end_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "badges_sashes", force: :cascade do |t|
    t.integer "badge_id"
    t.integer "sash_id"
    t.boolean "notified_user", default: false
    t.datetime "created_at"
    t.index ["badge_id", "sash_id"], name: "index_badges_sashes_on_badge_id_and_sash_id"
    t.index ["badge_id"], name: "index_badges_sashes_on_badge_id"
    t.index ["sash_id"], name: "index_badges_sashes_on_sash_id"
  end

  create_table "igem_edits", force: :cascade do |t|
    t.bigint "team_id"
    t.bigint "igem_user_id"
    t.bigint "igem_page_id"
    t.datetime "edit_time"
    t.bigint "edit_size"
    t.bigint "page_size"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["igem_page_id"], name: "index_igem_edits_on_igem_page_id"
    t.index ["igem_user_id"], name: "index_igem_edits_on_igem_user_id"
    t.index ["team_id"], name: "index_igem_edits_on_team_id"
  end

  create_table "igem_pages", force: :cascade do |t|
    t.bigint "team_id"
    t.string "url"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["team_id"], name: "index_igem_pages_on_team_id"
  end

  create_table "igem_users", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "team_id"
    t.string "username"
    t.boolean "registered", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "full_name"
    t.boolean "custom", default: false
    t.string "team_role"
  end

  create_table "jwt_blacklist", force: :cascade do |t|
    t.string "jti", null: false
    t.index ["jti"], name: "index_jwt_blacklist_on_jti"
  end

  create_table "jwt_blacklists", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "log_igem_users", force: :cascade do |t|
    t.bigint "log_id", null: false
    t.bigint "igem_user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "log_relation_categories", force: :cascade do |t|
    t.bigint "log_id", null: false
    t.bigint "relation_category_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "log_users", force: :cascade do |t|
    t.bigint "log_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "logs", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "task_id", null: false
    t.bigint "team_id", null: false
    t.datetime "task_done_at", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "task_finished_at"
  end

  create_table "merit_actions", force: :cascade do |t|
    t.integer "user_id"
    t.string "action_method"
    t.integer "action_value"
    t.boolean "had_errors", default: false
    t.string "target_model"
    t.integer "target_id"
    t.text "target_data"
    t.boolean "processed", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "merit_activity_logs", force: :cascade do |t|
    t.integer "action_id"
    t.string "related_change_type"
    t.integer "related_change_id"
    t.string "description"
    t.datetime "created_at"
  end

  create_table "merit_score_points", force: :cascade do |t|
    t.bigint "score_id"
    t.integer "num_points", default: 0
    t.string "log"
    t.datetime "created_at"
    t.index ["score_id"], name: "index_merit_score_points_on_score_id"
  end

  create_table "merit_scores", force: :cascade do |t|
    t.bigint "sash_id"
    t.string "category", default: "default"
    t.index ["sash_id"], name: "index_merit_scores_on_sash_id"
  end

  create_table "news", force: :cascade do |t|
    t.bigint "team_id"
    t.string "content"
    t.string "image"
    t.string "title"
    t.string "action_text"
    t.string "action_url"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["team_id"], name: "index_news_on_team_id"
  end

  create_table "notification_settings_subscriptions", force: :cascade do |t|
    t.string "subscriber_type"
    t.bigint "subscriber_id"
    t.string "subscribable_type"
    t.bigint "subscribable_id"
    t.text "settings"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["subscribable_type", "subscribable_id"], name: "idx_subscriptions_subscribable_type_subscribable_id"
    t.index ["subscriber_type", "subscriber_id"], name: "idx_subscriptions_subscriber_type_subscriber_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.string "target_type"
    t.bigint "target_id"
    t.string "object_type"
    t.bigint "object_id"
    t.boolean "read", default: false, null: false
    t.text "metadata"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "subscription_id"
    t.string "category"
    t.string "type"
    t.index ["object_type", "object_id"], name: "index_notifications_on_object_type_and_object_id"
    t.index ["read"], name: "index_notifications_on_read"
    t.index ["target_type", "target_id"], name: "index_notifications_on_target_type_and_target_id"
  end

  create_table "phones", force: :cascade do |t|
    t.bigint "user_id"
    t.string "imei"
    t.string "os"
    t.string "brand"
    t.string "model"
    t.string "device_token"
    t.boolean "active", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "pings", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "phone_id", null: false
    t.bigint "target_id", null: false
    t.bigint "target_phone_id", null: false
    t.integer "rssi", null: false
    t.datetime "ping_at", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "read_marks", id: :serial, force: :cascade do |t|
    t.string "readable_type", null: false
    t.integer "readable_id"
    t.string "reader_type", null: false
    t.integer "reader_id"
    t.datetime "timestamp"
    t.index ["readable_type", "readable_id"], name: "index_read_marks_on_readable"
    t.index ["reader_id", "reader_type", "readable_type", "readable_id"], name: "read_marks_reader_readable_index", unique: true
    t.index ["reader_type", "reader_id"], name: "index_read_marks_on_reader"
  end

  create_table "relation_categories", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "team_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "sashes", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "slack_auths", force: :cascade do |t|
    t.string "token"
    t.string "scope"
    t.string "slack_user_id"
    t.string "slack_team_id"
    t.string "slack_team_name"
    t.string "slack_enterprise_id"
    t.bigint "team_id"
    t.bigint "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["team_id"], name: "index_slack_auths_on_team_id"
    t.index ["user_id"], name: "index_slack_auths_on_user_id"
  end

  create_table "slack_channel_users", force: :cascade do |t|
    t.bigint "slack_user_id"
    t.bigint "slack_channel_id"
    t.bigint "team_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["slack_channel_id"], name: "index_slack_channel_users_on_slack_channel_id"
    t.index ["slack_user_id"], name: "index_slack_channel_users_on_slack_user_id"
    t.index ["team_id"], name: "index_slack_channel_users_on_team_id"
  end

  create_table "slack_channels", force: :cascade do |t|
    t.bigint "team_id"
    t.datetime "created"
    t.string "channel_id"
    t.boolean "is_archived"
    t.boolean "is_channel"
    t.boolean "is_ext_shared"
    t.boolean "is_general"
    t.boolean "is_group"
    t.boolean "is_im"
    t.boolean "is_member"
    t.boolean "is_mpim"
    t.boolean "is_org_shared"
    t.boolean "is_pending_ext_shared"
    t.boolean "is_private"
    t.boolean "is_shared"
    t.string "name"
    t.string "name_normalized"
    t.bigint "num_members"
    t.string "topic"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["team_id"], name: "index_slack_channels_on_team_id"
  end

  create_table "slack_mentions", force: :cascade do |t|
    t.bigint "source_id"
    t.string "target_type"
    t.bigint "target_id"
    t.bigint "slack_message_id"
    t.bigint "team_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["slack_message_id"], name: "index_slack_mentions_on_slack_message_id"
    t.index ["target_type", "target_id"], name: "index_slack_mentions_on_target_type_and_target_id"
    t.index ["team_id"], name: "index_slack_mentions_on_team_id"
  end

  create_table "slack_messages", force: :cascade do |t|
    t.bigint "team_id"
    t.bigint "slack_channel_id"
    t.bigint "slack_user_id"
    t.string "message_id"
    t.string "text"
    t.datetime "created"
    t.string "ts"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "thread_ts"
    t.bigint "parent_id"
    t.index ["slack_channel_id"], name: "index_slack_messages_on_slack_channel_id"
    t.index ["slack_user_id"], name: "index_slack_messages_on_slack_user_id"
    t.index ["team_id"], name: "index_slack_messages_on_team_id"
  end

  create_table "slack_reactions", force: :cascade do |t|
    t.string "name"
    t.bigint "team_id"
    t.bigint "slack_user_id"
    t.bigint "slack_message_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["slack_message_id"], name: "index_slack_reactions_on_slack_message_id"
    t.index ["slack_user_id"], name: "index_slack_reactions_on_slack_user_id"
    t.index ["team_id"], name: "index_slack_reactions_on_team_id"
  end

  create_table "slack_usergroup_members", force: :cascade do |t|
    t.bigint "team_id"
    t.bigint "slack_usergroup_id"
    t.bigint "slack_user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["slack_user_id"], name: "index_slack_usergroup_members_on_slack_user_id"
    t.index ["slack_usergroup_id"], name: "index_slack_usergroup_members_on_slack_usergroup_id"
    t.index ["team_id"], name: "index_slack_usergroup_members_on_team_id"
  end

  create_table "slack_usergroups", force: :cascade do |t|
    t.bigint "team_id"
    t.datetime "created"
    t.datetime "updated"
    t.datetime "deleted"
    t.string "description"
    t.string "handle"
    t.string "usergroup_id"
    t.boolean "is_external"
    t.boolean "is_subteam"
    t.boolean "is_usergroup"
    t.string "name"
    t.bigint "user_count"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["team_id"], name: "index_slack_usergroups_on_team_id"
  end

  create_table "slack_users", force: :cascade do |t|
    t.bigint "team_id"
    t.bigint "user_id"
    t.string "slack_user_id"
    t.string "email"
    t.string "name"
    t.string "real_name"
    t.string "display_name"
    t.datetime "updated"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "tz"
    t.string "tz_label"
    t.string "tz_offset"
    t.boolean "is_bot"
    t.string "image_1024"
    t.string "image_192"
    t.string "image_48"
    t.index ["team_id"], name: "index_slack_users_on_team_id"
    t.index ["user_id"], name: "index_slack_users_on_user_id"
  end

  create_table "survey_data", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "survey_field_id", null: false
    t.bigint "survey_id", null: false
    t.jsonb "content", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "survey_data_no_auths", force: :cascade do |t|
    t.bigint "igem_user_id"
    t.bigint "survey_field_id"
    t.bigint "survey_id"
    t.jsonb "content"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "survey_fields", force: :cascade do |t|
    t.bigint "survey_id", null: false
    t.string "category", null: false
    t.string "name", null: false
    t.boolean "required", default: false
    t.jsonb "content", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "surveys", force: :cascade do |t|
    t.bigint "team_id"
    t.string "name", null: false
    t.string "description", null: false
    t.interval "time"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "tasks", force: :cascade do |t|
    t.string "category"
    t.string "name"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "team_id"
  end

  create_table "teams", force: :cascade do |t|
    t.string "name", null: false
    t.integer "year", null: false
    t.integer "igem_team_id"
    t.string "country"
    t.boolean "registered", default: false
    t.string "url"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name", "year"], name: "name_year_uniqueness", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.bigint "igem_user_id"
    t.bigint "team_id"
    t.string "username"
    t.boolean "verified"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.float "latitude"
    t.float "longitude"
    t.string "status"
    t.text "settings"
    t.bigint "longest_streak"
    t.bigint "current_streak"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

end
