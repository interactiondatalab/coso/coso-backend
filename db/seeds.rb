# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


team = Team.find_by(igem_team_id: 301)
if team.nil?
  team = Team.create(name: 'IDLab', year: 2020, country: 'France', igem_team_id: 301)
else
  team.update_attributes(name: 'IDLab', year: 2020, country: 'France', igem_team_id: 301)
end
if 'Accepted' == 'Accepted'
  team.registered = true
else
  team.registered = false
end
team.save
